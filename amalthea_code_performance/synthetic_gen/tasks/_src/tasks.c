// This code is auto-generated

#include "tasks.h"
#include <pthread.h>
extern int Image[15625]; 
extern int ResultsA[15625]; 
extern int ResultsB[15625]; 

void Task1(){
	#pragma omp parallel 
	#pragma omp single
	{
	#pragma omp task depend(out:Image)
	run_read_image("puDef");
	#pragma omp task depend(inout:Image)
	{
		run_convert_image("puDef");
	}
	#pragma omp task depend(in:Image) depend(out:ResultsA) 
	{
		run_analysisA("puDef");
	}
	#pragma omp task depend(in:Image) depend(out:ResultsB) 
	{
		run_analysisB("puDef");
	}
	#pragma omp task depend(in:ResultsA,ResultsB)
	run_merge_results("puDef");
	}
}

void *Task1_entry(){
	Task1();
}
