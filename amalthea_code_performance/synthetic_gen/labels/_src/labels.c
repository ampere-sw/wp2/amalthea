// This code is auto-generated

#include "labels.h"

int Image[15625];	

static bool isIinitialized_Image = false;
void initialize_Image() {
	if (!isIinitialized_Image){
		for (int i=0; i < 15625; i++){
			Image[i] = i+1;
		}
		isIinitialized_Image = true;
	}
}


void read_Image(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	
	for (int repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Image) / 4;
		
		//printf("number of bytes:%d\n",arraysize);
		int leftOverElements=arraysize%10;
		
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Image[i];
			a = Image[i+1];
			a = Image[i+2];
			a = Image[i+3];
			a = Image[i+4];
			a = Image[i+5];
			a = Image[i+6];
			a = Image[i+7];
			a = Image[i+8];
			a = Image[i+9];
		}
		for(;i<arraysize;i++){
			a = Image[i];
		}
	}
}

void write_Image(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	for (int repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Image) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Image[i]   = 0xAFFE;
			Image[i+1] = 0xAFFE;
			Image[i+2] = 0xAFFE;
			Image[i+3] = 0xAFFE;
			Image[i+4] = 0xAFFE;
			Image[i+5] = 0xAFFE;
			Image[i+6] = 0xAFFE;
			Image[i+7] = 0xAFFE;
			Image[i+8] = 0xAFFE;
			Image[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Image[i]=0xAFFE;
		}
	}
}

int ResultsA[15625];	

static bool isIinitialized_ResultsA = false;
void initialize_ResultsA() {
	if (!isIinitialized_ResultsA){
		for (int i=0; i < 15625; i++){
			ResultsA[i] = i+1;
		}
		isIinitialized_ResultsA = true;
	}
}


void read_ResultsA(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	
	for (int repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(ResultsA) / 4;
		
		//printf("number of bytes:%d\n",arraysize);
		int leftOverElements=arraysize%10;
		
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = ResultsA[i];
			a = ResultsA[i+1];
			a = ResultsA[i+2];
			a = ResultsA[i+3];
			a = ResultsA[i+4];
			a = ResultsA[i+5];
			a = ResultsA[i+6];
			a = ResultsA[i+7];
			a = ResultsA[i+8];
			a = ResultsA[i+9];
		}
		for(;i<arraysize;i++){
			a = ResultsA[i];
		}
	}
}

void write_ResultsA(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	for (int repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(ResultsA) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			ResultsA[i]   = 0xAFFE;
			ResultsA[i+1] = 0xAFFE;
			ResultsA[i+2] = 0xAFFE;
			ResultsA[i+3] = 0xAFFE;
			ResultsA[i+4] = 0xAFFE;
			ResultsA[i+5] = 0xAFFE;
			ResultsA[i+6] = 0xAFFE;
			ResultsA[i+7] = 0xAFFE;
			ResultsA[i+8] = 0xAFFE;
			ResultsA[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				ResultsA[i]=0xAFFE;
		}
	}
}

int ResultsB[15625];	

static bool isIinitialized_ResultsB = false;
void initialize_ResultsB() {
	if (!isIinitialized_ResultsB){
		for (int i=0; i < 15625; i++){
			ResultsB[i] = i+1;
		}
		isIinitialized_ResultsB = true;
	}
}


void read_ResultsB(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	
	for (int repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(ResultsB) / 4;
		
		//printf("number of bytes:%d\n",arraysize);
		int leftOverElements=arraysize%10;
		
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = ResultsB[i];
			a = ResultsB[i+1];
			a = ResultsB[i+2];
			a = ResultsB[i+3];
			a = ResultsB[i+4];
			a = ResultsB[i+5];
			a = ResultsB[i+6];
			a = ResultsB[i+7];
			a = ResultsB[i+8];
			a = ResultsB[i+9];
		}
		for(;i<arraysize;i++){
			a = ResultsB[i];
		}
	}
}

void write_ResultsB(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	for (int repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(ResultsB) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			ResultsB[i]   = 0xAFFE;
			ResultsB[i+1] = 0xAFFE;
			ResultsB[i+2] = 0xAFFE;
			ResultsB[i+3] = 0xAFFE;
			ResultsB[i+4] = 0xAFFE;
			ResultsB[i+5] = 0xAFFE;
			ResultsB[i+6] = 0xAFFE;
			ResultsB[i+7] = 0xAFFE;
			ResultsB[i+8] = 0xAFFE;
			ResultsB[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				ResultsB[i]=0xAFFE;
		}
	}
}

