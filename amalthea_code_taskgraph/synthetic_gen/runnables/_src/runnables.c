// This code is auto-generated

#include "runnables.h"

//Runnable read_image----
void run_read_image(char* coreName){
	{
	executeTicks_DiscreteValueConstantImpl(400000000);
	}
	write_Image(1);
}


//Runnable convert_image----
void run_convert_image(char* coreName){
	read_Image(1);
	{
	executeTicks_DiscreteValueConstantImpl(700000000);
	}
	write_Image(1);
}


//Runnable analysisA----
void run_analysisA(char* coreName){
	read_Image(1);
	{
	executeTicks_DiscreteValueConstantImpl(1000000000);
	}
	write_ResultsA(1);
}


//Runnable analysisB----
void run_analysisB(char* coreName){
	read_Image(1);
	{
	executeTicks_DiscreteValueConstantImpl(1000000000);
	}
	write_ResultsB(1);
}


//Runnable merge_results----
void run_merge_results(char* coreName){
	read_ResultsA(1);
	read_ResultsB(1);
	{
	executeTicks_DiscreteValueConstantImpl(700000000);
	}
}

