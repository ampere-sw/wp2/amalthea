// This code is auto-generated

#pragma once

#include <string.h>
//#pragma omp declare target
//int strcmp ( const char * str1, const char * str2 );
//#pragma omp end declare target
#include "labels.h"

//Runnable read_image----
void run_read_image(char* coreName);
#include "labels.h"

//Runnable convert_image----
void run_convert_image(char* coreName);
#include "labels.h"

//Runnable analysisA----
void run_analysisA(char* coreName);
#include "labels.h"

//Runnable analysisB----
void run_analysisB(char* coreName);
#include "labels.h"

//Runnable merge_results----
void run_merge_results(char* coreName);
