// This code is auto-generated

#pragma once

#include <stdbool.h>

void initialize_Image();
void read_Image(int labelAccessStatistics);
void write_Image(int labelAccessStatistics);

void initialize_ResultsA();
void read_ResultsA(int labelAccessStatistics);
void write_ResultsA(int labelAccessStatistics);

void initialize_ResultsB();
void read_ResultsB(int labelAccessStatistics);
void write_ResultsB(int labelAccessStatistics);

