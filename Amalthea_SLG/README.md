# Code generator

This folder contains the code generator plugin source code and a simple example.

APP4MC 1.2 is required.

1. To install the code generator follow the steps 1 to 5 in "developers_guide.pdf".

2. Replace the load_generator/plugins folders with the ones provided in this repository

3. Redo step 5 of the "developers_guide.pdf".

After executing the code generator an output folder should be generated in the path "org.eclipse.app4mc.addon.transformation/load_generator/linux/releng/org.eclipse.app4mc.slg.linux.product/output".

This output folder will contain the C source code of the model.
