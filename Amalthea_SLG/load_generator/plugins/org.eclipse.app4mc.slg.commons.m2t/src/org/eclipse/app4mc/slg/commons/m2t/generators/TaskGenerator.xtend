/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.commons.m2t.generators

import java.util.List
import java.util.Set
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit

class TaskGenerator {

	public static val String LIB_NAME = "TASKS_LIB";

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def String toH(SLGTranslationUnit tu)
	'''
		
		//Header of Task «tu.moduleName»
		#include <stdint.h>
		
		class «tu.moduleName» {
		
		public:
		
		    //constructor
			«tu.moduleName»(void);
			
			// destructor
			virtual ~«tu.moduleName»() =default;
			
			
			void «initCall(tu.moduleName)»;
		
			void «stepCall(tu.moduleName)»;
		
		 };
	'''

	static def String toCpp(SLGTranslationUnit tu, String incFile, Set<String> includes, List<String> initCalls, List<String> stepCalls)
	'''
		#include "«incFile»"
		«FOR include : includes»
			#include "«include»"
		«ENDFOR»
		
		«tu.moduleName»::«tu.moduleName»(){
			//default constructor
		}
		
		
		void «tu.moduleName»::«initCall(tu.moduleName)» {
		       «FOR call : initCalls»
		       	«call»;
		       «ENDFOR»
		} 
		
		void «tu.moduleName»::«stepCall(tu.moduleName)» {
		       «FOR call : stepCalls»
		       	«call»;
		       «ENDFOR»
		}
	'''

	// ---------- names of generated 'C' functions ----------

	static def String initCall(String moduleName) '''initialize_task_«moduleName»()'''

	static def String stepCall(String moduleName) '''step_task_«moduleName»()'''

}
