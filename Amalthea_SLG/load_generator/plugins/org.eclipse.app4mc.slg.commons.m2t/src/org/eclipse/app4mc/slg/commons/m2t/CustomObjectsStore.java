/**
 * *******************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.commons.m2t;

import java.util.HashMap;

public class CustomObjectsStore {
	private HashMap<Object, Object> cls_instance_map = new HashMap<>();

	private HashMap<String, Object> data_map = new HashMap<>();

	public <T extends Object> T getInstance(final Object cls) {
		final Object value = this.cls_instance_map.get(cls);
		return ((T) value);
	}

	public <T extends Object> void injectMembers(final Object cls, final T instance) {
		this.cls_instance_map.put(cls, instance);
	}

	public <T extends Object> void indexData(final String key, final T value) {
		this.data_map.put(key, value);
	}

	public <T extends Object> T getData(final String key) {
		return ((T) this.data_map.get(key));
	}

	public void clearCache() {
		this.cls_instance_map.clear();
	}
}
