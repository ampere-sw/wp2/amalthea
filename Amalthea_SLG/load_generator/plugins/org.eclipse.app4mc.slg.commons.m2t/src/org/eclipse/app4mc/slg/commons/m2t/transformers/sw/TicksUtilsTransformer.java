/**
 ********************************************************************************
 * Copyright (c) 2021-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.commons.m2t.transformers.sw;

import static org.eclipse.app4mc.slg.commons.m2t.transformers.SLGBaseSettings.OTHER_TYPE;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.slg.commons.m2t.CustomObjectsStore;
import org.eclipse.app4mc.slg.commons.m2t.generators.TicksUtilsGenerator;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGBaseTransformer;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.PlatformArchitecture;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.ecore.EClass;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class TicksUtilsTransformer extends SLGBaseTransformer {

	@Inject private OutputBuffer outputBuffer;
	@Inject private CustomObjectsStore customObjsStore;

	public SLGTranslationUnit transform(final IDiscreteValueDeviation value) {
		if (value == null)
			return new SLGTranslationUnit("UNSPECIFIED TICKS");

		return transformClass(value.eClass()); // hash according to class not instance
	}

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transformClass(final EClass eClass) {
		final List<Object> key = List.of(eClass);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(eClass);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, eClass);
		}

		return tu;
	}

	// ---------------------------------------------------

	protected SLGTranslationUnit createTranslationUnit(final EClass eClass) {
		if (eClass == null) {
			return new SLGTranslationUnit("UNSPECIFIED TICKS");
		} else {
			String basePath = "synthetic_gen";
			String moduleName = "ticksUtils";
			String call = "burnTicks(<params>)"; // unused
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	protected void doTransform(final SLGTranslationUnit tu, final EClass eClass) {
		genFiles(tu, eClass);
	}

	protected void genFiles(SLGTranslationUnit tu, final EClass eClass) {
		if (isIncFileEmpty(tu)) {
			incAppend(tu, "#pragma once \n");
			toH(tu);
		}
		if (isSrcFileEmpty(tu)) {
			toCPP(tu);
		}
		srcAppend(tu, TicksUtilsGenerator.generateTicks(eClass));
		incAppend(tu, TicksUtilsGenerator.generateTicksDeclaration(eClass));
	}

	protected void toCPP(SLGTranslationUnit tu) {
		srcAppend(tu, "#include \"" + getIncFile(tu) + "\"\n");

		final ConfigModel configModel = customObjsStore.<ConfigModel>getInstance(ConfigModel.class);

		PlatformArchitecture platformArchitectureType = configModel.getPlatformArchitectureType();

		if (platformArchitectureType != null) {

			srcAppend(tu, "#define __" + platformArchitectureType.getName() + "__\n");
		}
		final String ticksCodeSnippet = configModel.getCustomTickImpl().getValue();
		final boolean ticksCodeEnabled = configModel.getCustomTickImpl().isEnable();

		final String burnTicksBody = ticksCodeEnabled ? ticksCodeSnippet : TicksUtilsGenerator.burnTicksDefault();

		srcAppend(tu, TicksUtilsGenerator.burnTicks(burnTicksBody));
		srcAppend(tu, TicksUtilsGenerator.burnTicksStatistics(configModel));
	}

	protected void toH(SLGTranslationUnit tu) {

		incAppend(tu, TicksUtilsGenerator.burnTicksDeclaration());
		incAppend(tu, TicksUtilsGenerator.burnTicksStatisticsDeclaration());

	}

	public void createCMake() {
		// Building rule: basePath + moduleName + "CMakeLists.txt"
		String makeFilePath = "synthetic_gen/ticksUtils/CMakeLists.txt";
		outputBuffer.appendTo(OTHER_TYPE, makeFilePath, TicksUtilsGenerator.toCMake(getSrcFiles()));
	}

}
