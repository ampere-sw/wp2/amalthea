/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.commons.m2t.generators

import java.util.List
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage
import org.eclipse.app4mc.slg.config.ConfigModel
import org.eclipse.app4mc.slg.config.TickType
import org.eclipse.emf.ecore.EClass

class TicksUtilsGenerator {

	public static val String LIB_NAME = "TICKS_UTILS";

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def String toCMake(List<String> srcFiles)
	'''
			# «LIB_NAME» ################################################################
			####
			add_library(«LIB_NAME»  STATIC
		«FOR srcFile : srcFiles»
			${CMAKE_CURRENT_LIST_DIR}/_src/«srcFile»
			«ENDFOR»
		)
		
				target_include_directories(«LIB_NAME»
				    PUBLIC ${CMAKE_CURRENT_LIST_DIR}/_inc/
				)	
	'''

	// ----- generate ticks -----

	static def String generateTicksDeclaration(EClass eClass) {
		switch eClass {
			case AmaltheaPackage.eINSTANCE.discreteValueConstant: '''
				void «execCall(eClass, "long long ticks")»;
				
			'''
			case AmaltheaPackage.eINSTANCE.discreteValueStatistics: '''
				void «execCall(eClass, "double average, long long lowerBound, long long upperBound")»;
				
			'''
			default: {
				""
			}
		}
	}

	static def String generateTicks(EClass eClass) {
		switch eClass {
			case AmaltheaPackage.eINSTANCE.discreteValueConstant: '''
				void «execCall(eClass, "long long ticks")» {
					burnTicks(ticks);
				}
				
			'''
			case AmaltheaPackage.eINSTANCE.discreteValueStatistics: '''
				void «execCall(eClass, "double average,long long lowerBound, long long upperBound")» {
					burnTicksStatistics(average, lowerBound, upperBound);
				}
				
			'''
			default: {
				""
			}
		}
	}

	static def String execCall(EClass eClass, String params) '''executeTicks_«eClass.name»(«params»)'''


	// ----- burn ticks -----

	static def String burnTicksDeclaration() '''
		void burnTicks(long long ticks);
		
	'''

	static def String burnTicks(String burnTicksBody)
	'''
		void burnTicks(long long ticks) {
			«burnTicksBody»
		}
		
	'''

	// ----- burn ticks statistics -----

	static def String burnTicksStatisticsDeclaration() '''
		void burnTicksStatistics(double average, long long lowerBound, long long upperBound);
		
	  '''
	
	static def String burnTicksStatistics(ConfigModel configModel)
	'''
		void burnTicksStatistics(double average, long long lowerBound, long long upperBound) {
			burnTicks(«chooseTicks(configModel)»);
		}
		
	'''

	// select statistics parameter according to configuration model
	// TODO: choose final names of configuration parameters
	private static def chooseTicks(ConfigModel configModel) {
		switch configModel.defaultTickType {
			case TickType.MINIMUM: "lowerBound"
			case TickType.MAXIMUM: "upperBound"
			case TickType.AVERAGE: "(long long)average"
			default: "(long long)average"
		}
	}

	// ----- Default implementations -----

	static def String burnTicksDefault()
	'''
			// default implementation of tick burning
			long long numLoops = ticks / 400; 
		#	if defined (__x86_64__)
		 		for (long long i = 0; i < numLoops; i++) {
		 			«FOR i : 1..400»
		 				__asm volatile("nop");
		 			«ENDFOR»
		 		}
		#	elif defined (__x86_32__) 		
		 		for (long long i = 0; i < numLoops; i++) {
		 			 «FOR i : 1..400»
		 			 	__asm volatile("mov r0, r0");
		 			 «ENDFOR» 
		 		}
		#	elif defined (__aarch64__) 		
			      asm volatile (
			        "mov x0, #0 \n"            // Initialize loop counter
			        "mov x1, %0 \n"            // Load loop count
			        "1: \n"                    // Global label
			        "add x0, x0, #1 \n"        // Increment loop counter
			        "cmp x0, x1 \n"            // Compare loop counter with loop count
			        "bne 1b \n"                // Branch to global label
			        :                         // No output operands
			        : "r" (ticks)        // Input operand (loop count)
			        : "x0", "x1", "cc"        // Clobbered registers (x0, x1) and condition codes (cc)
			    );
		#	endif
	'''

}
