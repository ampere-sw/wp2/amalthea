/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.commons.m2t.generators

import java.util.List

class RunnableGenerator {

	public static val String LIB_NAME = "RUNNABLES_LIB";

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def String toCMake(List<String> srcFiles) '''
			# «LIB_NAME» ################################################################
			####
			add_library(«LIB_NAME» STATIC
			«FOR srcFile : srcFiles»
			${CMAKE_CURRENT_LIST_DIR}/_src/«srcFile»
		«ENDFOR»
		)
		
				target_include_directories(«LIB_NAME»
				    PUBLIC  ${CMAKE_CURRENT_LIST_DIR}/_inc
				)	
				
				target_include_directories(«LIB_NAME»
				    PUBLIC ${CMAKE_CURRENT_LIST_DIR}/../ticksUtils/_inc
				    	        ${CMAKE_CURRENT_LIST_DIR}/../labels/_inc
				)	
				
				target_link_libraries(«LIB_NAME» 
				    PRIVATE LABELS_LIB TICKS_UTILS
				)
	'''

}
