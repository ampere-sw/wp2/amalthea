/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.commons.m2t.transformers;

import static org.eclipse.app4mc.slg.commons.m2t.transformers.SLGBaseSettings.INC_FOLDER;
import static org.eclipse.app4mc.slg.commons.m2t.transformers.SLGBaseSettings.INC_TYPE;
import static org.eclipse.app4mc.slg.commons.m2t.transformers.SLGBaseSettings.SRC_FOLDER;
import static org.eclipse.app4mc.slg.commons.m2t.transformers.SLGBaseSettings.SRC_TYPE;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.app4mc.transformation.util.OutputBuffer;

import com.google.inject.Inject;

public abstract class SLGBaseTransformer {

	@Inject private OutputBuffer outputBuffer;

	// Buffer handling

	public boolean isIncFileEmpty(SLGTranslationUnit tu) {
		return !outputBuffer.bufferExists(INC_TYPE, getIncModulePath(tu));
	}

	public boolean isSrcFileEmpty(SLGTranslationUnit tu) {
		return !outputBuffer.bufferExists(SRC_TYPE, getSrcModulePath(tu));
	}

	public boolean incAppend(SLGTranslationUnit tu, String str) {
		return outputBuffer.appendTo(INC_TYPE, getIncModulePath(tu), str);
	}

	public boolean srcAppend(SLGTranslationUnit tu, String str) {
		return outputBuffer.appendTo(SRC_TYPE, getSrcModulePath(tu), str);
	}

	// Attributes derived from translation unit 

	public String getIncModulePath(SLGTranslationUnit tu) { return tu.getModulePath() + INC_FOLDER + tu.getModuleName(); }
	public String getSrcModulePath(SLGTranslationUnit tu) { return tu.getModulePath() + SRC_FOLDER + tu.getModuleName(); }

	public String getIncPath(SLGTranslationUnit tu) { return tu.getModulePath() + INC_FOLDER + getIncFile(tu); }
	public String getSrcPath(SLGTranslationUnit tu) { return tu.getModulePath() + SRC_FOLDER + getSrcFile(tu); }

	// Attributes derived from translation unit and output buffer (!)

	public String getIncFile(SLGTranslationUnit tu) { return tu.getModuleName() + outputBuffer.getFileExtension(INC_TYPE); }
	public String getSrcFile(SLGTranslationUnit tu) { return tu.getModuleName() + outputBuffer.getFileExtension(SRC_TYPE); }

	// Generic cache handling

	public List<String> getSrcFiles() {

		return getCache().values().stream()
				.map(this::getSrcFile)
				.sorted()
				.distinct()
				.collect(Collectors.toList());
	}

	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return Collections.emptyMap();
	}

}
