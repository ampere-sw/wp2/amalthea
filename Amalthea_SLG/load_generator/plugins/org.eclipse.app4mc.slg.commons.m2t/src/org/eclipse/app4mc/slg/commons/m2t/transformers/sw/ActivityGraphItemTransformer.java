/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.commons.m2t.transformers.sw;

import java.util.Map;

import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.ChannelReceive;
import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGBaseTransformer;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class ActivityGraphItemTransformer extends SLGBaseTransformer {

	private static final String UNSUPPORTED_ELEMENT = "Unsupported element inside ActivityGraph : {0}";

	@Inject private SessionLogger logger;
	@Inject private RunnableTransformer runnableTransformer;
	@Inject private LabelAccessTransformer labelAccessTransformer;
	@Inject private TicksTransformer ticksTransformer;

	public SLGTranslationUnit transform(final ActivityGraphItem graphItem) {
		if (graphItem == null) {
			throw new IllegalArgumentException("Unhandled parameter type: null");
		}

		if (graphItem instanceof LabelAccess) {
			return labelAccessTransformer.transform((LabelAccess) graphItem);

		} else if (graphItem instanceof Ticks) {
			return ticksTransformer.transform((Ticks) graphItem);

		} else if (graphItem instanceof RunnableCall) {
			return runnableTransformer.transform(((RunnableCall) graphItem).getRunnable());

		} else if (graphItem instanceof ChannelReceive) {
			logger.error(UNSUPPORTED_ELEMENT, "ChannelReceive");
			return new SLGTranslationUnit("//ChannelReceive is not yet implemented");

		} else if (graphItem instanceof ChannelSend) {
			logger.error(UNSUPPORTED_ELEMENT, "ChannelSend");
			return new SLGTranslationUnit("//ChannelSend is not yet implemented");
		}

		logger.error(UNSUPPORTED_ELEMENT, graphItem.getClass().getName());
		return new SLGTranslationUnit(graphItem.eClass().getName() + " in not supported");
	}

	public Map<String, SLGTranslationUnit> transformAllItems(final Ticks ticks) {
		return ticksTransformer.transformAllItems(ticks);
	}

}
