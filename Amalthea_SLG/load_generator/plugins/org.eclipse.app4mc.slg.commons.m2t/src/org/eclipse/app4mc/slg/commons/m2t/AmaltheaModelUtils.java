/**
 * *******************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.commons.m2t;

import java.util.Set;

import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.BooleanObject;
import org.eclipse.app4mc.amalthea.model.DataSize;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.ModeSwitch;
import org.eclipse.app4mc.amalthea.model.ModeSwitchDefault;
import org.eclipse.app4mc.amalthea.model.ModeSwitchEntry;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

public class AmaltheaModelUtils {

	// Suppress default constructor
	private AmaltheaModelUtils() {
		throw new IllegalStateException("Utility class");
	}

	public static ModeSwitchEntry getModeSwitchEntryForCase(ActivityGraph activityGraph, String caseValue) {

		EList<ActivityGraphItem> items = activityGraph.getItems();

		for (ActivityGraphItem ActivityGraphItem : items) {
			if (ActivityGraphItem instanceof ModeSwitch) {
				EList<ModeSwitchEntry> entries = ((ModeSwitch) ActivityGraphItem).getEntries();

				for (ModeSwitchEntry modeSwitchEntry : entries) {

					if (modeSwitchEntry.getName().equals(caseValue)) {
						return modeSwitchEntry;
					}
				}
			}
		}

		return null;
	}

	/**
	 * This method is used to get all the ActivityGraphItems which are present in
	 * the default level and inside ModeSwitchEntry elements
	 * 
	 * @param activityGraph
	 * @return EList<ActivityGraphItem>
	 */
	public static EList<ActivityGraphItem> getAllActivityGraphItems(ActivityGraph activityGraph) {

		BasicEList<ActivityGraphItem> basicEList = new BasicEList<>();
		EList<ActivityGraphItem> items = activityGraph.getItems();

		basicEList.addAll(items);

		for (ActivityGraphItem ActivityGraphItem : items) {
			if (ActivityGraphItem instanceof ModeSwitch) {
				EList<ModeSwitchEntry> entries = ((ModeSwitch) ActivityGraphItem).getEntries();

				for (ModeSwitchEntry modeSwitchEntry : entries) {

					EList<ActivityGraphItem> modeSwitchEntriesActivityGraphItems = modeSwitchEntry.getItems();

					basicEList.addAll(modeSwitchEntriesActivityGraphItems);

					// TODO: handle the nested ModeSwitchEntry elements also the RunnableModeSwitch
					// elements etc.,
				}
			}
		}

		return basicEList;
	}

	public static long getDataSizeInBytes(LabelAccess labelAccess) {

		Label data = labelAccess.getData();

		if (data != null) {
			DataSize size = data.getSize();

			if (size != null) {
				return size.getNumberBytes();
			}
		}
		return 0;
	}

	public static long getLabelArraySize(Label label) {

		if (label != null) {
			DataSize size = label.getSize();
			if (size != null) {
				long numberBytes = size.getNumberBytes();
				return numberBytes / 4;
			}
		}
		return 0;
	}

	public static Set<Label> getAllLabelsUsedInTask(Task amltTask, Set<Label> labels) {

		ActivityGraph activityGraph = amltTask.getActivityGraph();

		if (activityGraph != null) {
			EList<ActivityGraphItem> items = activityGraph.getItems();

			populateLabels(labels, items);
		}

		return labels;
	}

	private static void populateLabels(Set<Label> labels, EList<ActivityGraphItem> items) {

		for (ActivityGraphItem ActivityGraphItem : items) {
			if (ActivityGraphItem instanceof LabelAccess) {
				Label data = ((LabelAccess) ActivityGraphItem).getData();
				if (data != null) {
					labels.add(data);
				}
			} else if (ActivityGraphItem instanceof ModeSwitch) {
				ModeSwitchDefault defaultEntry = ((ModeSwitch) ActivityGraphItem).getDefaultEntry();

				if (defaultEntry != null) {
					EList<ActivityGraphItem> items2 = defaultEntry.getItems();

					populateLabels(labels, items2);

				}

				EList<ModeSwitchEntry> entries = ((ModeSwitch) ActivityGraphItem).getEntries();

				for (ModeSwitchEntry modeSwitchEntry : entries) {

					EList<ActivityGraphItem> items2 = modeSwitchEntry.getItems();

					populateLabels(labels, items2);
				}
			} else if (ActivityGraphItem instanceof RunnableCall) {
				Runnable runnable = ((RunnableCall) ActivityGraphItem).getRunnable();
				if (runnable != null) {
					ActivityGraph activityGraph = runnable.getActivityGraph();

					if (activityGraph != null) {
						populateLabels(labels, activityGraph.getItems());
					}
				}

			}
		}
	}

	public static boolean isInitializePropertySet(RunnableCall runnableCall) {
		EMap<String, Value> customProperties = runnableCall.getCustomProperties();

		String key = "initialize";
		Value value = customProperties.get(key);

		if (value instanceof BooleanObject) {
			return ((BooleanObject) value).isValue();
		}

		return false;
	}
}
