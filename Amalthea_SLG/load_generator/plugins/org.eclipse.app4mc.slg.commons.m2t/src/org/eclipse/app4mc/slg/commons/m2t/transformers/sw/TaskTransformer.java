/**
 ********************************************************************************
 * Copyright (c) 2021-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.commons.m2t.transformers.sw;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.slg.commons.m2t.generators.LabelGenerator;
import org.eclipse.app4mc.slg.commons.m2t.generators.TaskGenerator;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGBaseTransformer;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class TaskTransformer extends SLGBaseTransformer {

	@Inject private RunnableTransformer runnableTransformer;
	@Inject private LabelTransformer labelTransformer;

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final Task task) {
		final List<Object> key = List.of(task);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(task);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, task);
		}

		return tu;
	}

	// ---------------------------------------------------

	protected SLGTranslationUnit createTranslationUnit(final Task task) {
		if ((task == null)) {
			return new SLGTranslationUnit("UNSPECIFIED TASK");
		} else {
			String basePath = "";
			String moduleName = task.getName();
			String call = "" + task.getName() + "()"; // unused
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	protected void doTransform(final SLGTranslationUnit tu, final Task task) {
		genFiles(tu, task);
	}

	protected void genFiles(SLGTranslationUnit tu, final Task task) {

		final LinkedHashSet<String> includes = new LinkedHashSet<>();
		final LinkedList<String> initCalls = new LinkedList<>();
		final LinkedList<String> stepCalls = new LinkedList<>();

		if (task != null && task.getActivityGraph() != null) {
			for (ActivityGraphItem item : task.getActivityGraph().getItems()) {

				if (item instanceof RunnableCall) {
					final Runnable runnable = ((RunnableCall) item).getRunnable();
					final SLGTranslationUnit runnableTU = runnableTransformer.transform(runnable);

					includes.add(getIncFile(runnableTU));

					List<Tag> tags = ((RunnableCall) item).getTags();
					if (tags.stream().anyMatch(tag -> "initialize".equals(tag.getName()))) {
						initCalls.add(runnableTU.getCall());
					} else {
						stepCalls.add(runnableTU.getCall());
					}
				}
			}
		}

		for (Entry<List<Object>, SLGTranslationUnit> entry : labelTransformer.getCache().entrySet()) {
			if (entry.getKey().get(0) instanceof Label) {	// Mc: use better way to get the object of interest
				Label label = (Label) entry.getKey();
				SLGTranslationUnit labelTU = entry.getValue();

				includes.add(getIncFile(labelTU));
				initCalls.add(LabelGenerator.initCall(label));
			}
		}

		incAppend(tu, TaskGenerator.toH(tu));
		srcAppend(tu, TaskGenerator.toCpp(tu, getIncFile(tu), includes, initCalls, stepCalls));
	}

}
