/**
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.commons.m2t.generators;

import java.util.List;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class RunnableGenerator {
  public static final String LIB_NAME = "RUNNABLES_LIB";
  
  private RunnableGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String toCMake(final List<String> srcFiles) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.append("# ");
    _builder.append(RunnableGenerator.LIB_NAME, "\t");
    _builder.append(" ################################################################");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("####");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("add_library(");
    _builder.append(RunnableGenerator.LIB_NAME, "\t");
    _builder.append(" STATIC");
    _builder.newLineIfNotEmpty();
    {
      for(final String srcFile : srcFiles) {
        _builder.append("\t");
        _builder.append("${CMAKE_CURRENT_LIST_DIR}/_src/");
        _builder.append(srcFile, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("target_include_directories(");
    _builder.append(RunnableGenerator.LIB_NAME, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t    ");
    _builder.append("PUBLIC  ${CMAKE_CURRENT_LIST_DIR}/_inc");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append(")\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("target_include_directories(");
    _builder.append(RunnableGenerator.LIB_NAME, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t    ");
    _builder.append("PUBLIC ${CMAKE_CURRENT_LIST_DIR}/../ticksUtils/_inc");
    _builder.newLine();
    _builder.append("\t\t    \t        ");
    _builder.append("${CMAKE_CURRENT_LIST_DIR}/../labels/_inc");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append(")\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("target_link_libraries(");
    _builder.append(RunnableGenerator.LIB_NAME, "\t\t");
    _builder.append(" ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t    ");
    _builder.append("PRIVATE LABELS_LIB TICKS_UTILS");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append(")");
    _builder.newLine();
    return _builder.toString();
  }
}
