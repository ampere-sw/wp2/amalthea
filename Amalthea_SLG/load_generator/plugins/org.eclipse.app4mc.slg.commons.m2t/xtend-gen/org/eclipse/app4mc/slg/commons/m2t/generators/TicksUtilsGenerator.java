/**
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.commons.m2t.generators;

import com.google.common.base.Objects;
import java.util.List;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.TickType;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.IntegerRange;

@SuppressWarnings("all")
public class TicksUtilsGenerator {
  public static final String LIB_NAME = "TICKS_UTILS";
  
  private TicksUtilsGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String toCMake(final List<String> srcFiles) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.append("# ");
    _builder.append(TicksUtilsGenerator.LIB_NAME, "\t");
    _builder.append(" ################################################################");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("####");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("add_library(");
    _builder.append(TicksUtilsGenerator.LIB_NAME, "\t");
    _builder.append("  STATIC");
    _builder.newLineIfNotEmpty();
    {
      for(final String srcFile : srcFiles) {
        _builder.append("${CMAKE_CURRENT_LIST_DIR}/_src/");
        _builder.append(srcFile);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("target_include_directories(");
    _builder.append(TicksUtilsGenerator.LIB_NAME, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t    ");
    _builder.append("PUBLIC ${CMAKE_CURRENT_LIST_DIR}/_inc/");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append(")\t");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String generateTicksDeclaration(final EClass eClass) {
    String _switchResult = null;
    boolean _matched = false;
    EClass _discreteValueConstant = AmaltheaPackage.eINSTANCE.getDiscreteValueConstant();
    if (Objects.equal(eClass, _discreteValueConstant)) {
      _matched=true;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("void ");
      String _execCall = TicksUtilsGenerator.execCall(eClass, "long long ticks");
      _builder.append(_execCall);
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _switchResult = _builder.toString();
    }
    if (!_matched) {
      EClass _discreteValueStatistics = AmaltheaPackage.eINSTANCE.getDiscreteValueStatistics();
      if (Objects.equal(eClass, _discreteValueStatistics)) {
        _matched=true;
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("void ");
        String _execCall_1 = TicksUtilsGenerator.execCall(eClass, "double average, long long lowerBound, long long upperBound");
        _builder_1.append(_execCall_1);
        _builder_1.append(";");
        _builder_1.newLineIfNotEmpty();
        _builder_1.newLine();
        _switchResult = _builder_1.toString();
      }
    }
    if (!_matched) {
      _switchResult = "";
    }
    return _switchResult;
  }
  
  public static String generateTicks(final EClass eClass) {
    String _switchResult = null;
    boolean _matched = false;
    EClass _discreteValueConstant = AmaltheaPackage.eINSTANCE.getDiscreteValueConstant();
    if (Objects.equal(eClass, _discreteValueConstant)) {
      _matched=true;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("void ");
      String _execCall = TicksUtilsGenerator.execCall(eClass, "long long ticks");
      _builder.append(_execCall);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("burnTicks(ticks);");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _switchResult = _builder.toString();
    }
    if (!_matched) {
      EClass _discreteValueStatistics = AmaltheaPackage.eINSTANCE.getDiscreteValueStatistics();
      if (Objects.equal(eClass, _discreteValueStatistics)) {
        _matched=true;
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("void ");
        String _execCall_1 = TicksUtilsGenerator.execCall(eClass, "double average,long long lowerBound, long long upperBound");
        _builder_1.append(_execCall_1);
        _builder_1.append(" {");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("burnTicksStatistics(average, lowerBound, upperBound);");
        _builder_1.newLine();
        _builder_1.append("}");
        _builder_1.newLine();
        _builder_1.newLine();
        _switchResult = _builder_1.toString();
      }
    }
    if (!_matched) {
      _switchResult = "";
    }
    return _switchResult;
  }
  
  public static String execCall(final EClass eClass, final String params) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("executeTicks_");
    String _name = eClass.getName();
    _builder.append(_name);
    _builder.append("(");
    _builder.append(params);
    _builder.append(")");
    return _builder.toString();
  }
  
  public static String burnTicksDeclaration() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void burnTicks(long long ticks);");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String burnTicks(final String burnTicksBody) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void burnTicks(long long ticks) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append(burnTicksBody, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String burnTicksStatisticsDeclaration() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void burnTicksStatistics(double average, long long lowerBound, long long upperBound);");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String burnTicksStatistics(final ConfigModel configModel) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void burnTicksStatistics(double average, long long lowerBound, long long upperBound) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("burnTicks(");
    String _chooseTicks = TicksUtilsGenerator.chooseTicks(configModel);
    _builder.append(_chooseTicks, "\t");
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  private static String chooseTicks(final ConfigModel configModel) {
    String _switchResult = null;
    TickType _defaultTickType = configModel.getDefaultTickType();
    if (_defaultTickType != null) {
      switch (_defaultTickType) {
        case MINIMUM:
          _switchResult = "lowerBound";
          break;
        case MAXIMUM:
          _switchResult = "upperBound";
          break;
        case AVERAGE:
          _switchResult = "(long long)average";
          break;
        default:
          _switchResult = "(long long)average";
          break;
      }
    } else {
      _switchResult = "(long long)average";
    }
    return _switchResult;
  }
  
  public static String burnTicksDefault() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.append("// default implementation of tick burning");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("long long numLoops = ticks / 400; ");
    _builder.newLine();
    _builder.append("#\tif defined (__x86_64__)");
    _builder.newLine();
    _builder.append(" \t\t");
    _builder.append("for (long long i = 0; i < numLoops; i++) {");
    _builder.newLine();
    {
      IntegerRange _upTo = new IntegerRange(1, 400);
      for(final Integer i : _upTo) {
        _builder.append(" \t\t\t");
        _builder.append("__asm volatile(\"nop\");");
        _builder.newLine();
      }
    }
    _builder.append(" \t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("#\telif defined (__x86_32__) \t\t");
    _builder.newLine();
    _builder.append(" \t\t");
    _builder.append("for (long long i = 0; i < numLoops; i++) {");
    _builder.newLine();
    {
      IntegerRange _upTo_1 = new IntegerRange(1, 400);
      for(final Integer i_1 : _upTo_1) {
        _builder.append(" \t\t\t ");
        _builder.append("__asm volatile(\"mov r0, r0\");");
        _builder.newLine();
      }
    }
    _builder.append(" \t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("#\telif defined (__aarch64__) \t\t");
    _builder.newLine();
    _builder.append("\t      ");
    _builder.append("asm volatile (");
    _builder.newLine();
    _builder.append("\t        ");
    _builder.append("\"mov x0, #0 \\n\"            // Initialize loop counter");
    _builder.newLine();
    _builder.append("\t        ");
    _builder.append("\"mov x1, %0 \\n\"            // Load loop count");
    _builder.newLine();
    _builder.append("\t        ");
    _builder.append("\"1: \\n\"                    // Global label");
    _builder.newLine();
    _builder.append("\t        ");
    _builder.append("\"add x0, x0, #1 \\n\"        // Increment loop counter");
    _builder.newLine();
    _builder.append("\t        ");
    _builder.append("\"cmp x0, x1 \\n\"            // Compare loop counter with loop count");
    _builder.newLine();
    _builder.append("\t        ");
    _builder.append("\"bne 1b \\n\"                // Branch to global label");
    _builder.newLine();
    _builder.append("\t        ");
    _builder.append(":                         // No output operands");
    _builder.newLine();
    _builder.append("\t        ");
    _builder.append(": \"r\" (ticks)        // Input operand (loop count)");
    _builder.newLine();
    _builder.append("\t        ");
    _builder.append(": \"x0\", \"x1\", \"cc\"        // Clobbered registers (x0, x1) and condition codes (cc)");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.append(");");
    _builder.newLine();
    _builder.append("#\tendif");
    _builder.newLine();
    return _builder.toString();
  }
}
