/**
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.commons.m2t.generators;

import java.util.List;
import java.util.Set;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class TaskGenerator {
  public static final String LIB_NAME = "TASKS_LIB";
  
  private TaskGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String toH(final SLGTranslationUnit tu) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("//Header of Task ");
    String _moduleName = tu.getModuleName();
    _builder.append(_moduleName);
    _builder.newLineIfNotEmpty();
    _builder.append("#include <stdint.h>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("class ");
    String _moduleName_1 = tu.getModuleName();
    _builder.append(_moduleName_1);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("public:");
    _builder.newLine();
    _builder.newLine();
    _builder.append("    ");
    _builder.append("//constructor");
    _builder.newLine();
    _builder.append("\t");
    String _moduleName_2 = tu.getModuleName();
    _builder.append(_moduleName_2, "\t");
    _builder.append("(void);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// destructor");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("virtual ~");
    String _moduleName_3 = tu.getModuleName();
    _builder.append(_moduleName_3, "\t");
    _builder.append("() =default;");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("void ");
    String _initCall = TaskGenerator.initCall(tu.getModuleName());
    _builder.append(_initCall, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("void ");
    String _stepCall = TaskGenerator.stepCall(tu.getModuleName());
    _builder.append(_stepCall, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append(" ");
    _builder.append("};");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toCpp(final SLGTranslationUnit tu, final String incFile, final Set<String> includes, final List<String> initCalls, final List<String> stepCalls) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    _builder.append(incFile);
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    {
      for(final String include : includes) {
        _builder.append("#include \"");
        _builder.append(include);
        _builder.append("\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    String _moduleName = tu.getModuleName();
    _builder.append(_moduleName);
    _builder.append("::");
    String _moduleName_1 = tu.getModuleName();
    _builder.append(_moduleName_1);
    _builder.append("(){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("//default constructor");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append("void ");
    String _moduleName_2 = tu.getModuleName();
    _builder.append(_moduleName_2);
    _builder.append("::");
    String _initCall = TaskGenerator.initCall(tu.getModuleName());
    _builder.append(_initCall);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    {
      for(final String call : initCalls) {
        _builder.append("       ");
        _builder.append(call, "       ");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("} ");
    _builder.newLine();
    _builder.newLine();
    _builder.append("void ");
    String _moduleName_3 = tu.getModuleName();
    _builder.append(_moduleName_3);
    _builder.append("::");
    String _stepCall = TaskGenerator.stepCall(tu.getModuleName());
    _builder.append(_stepCall);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    {
      for(final String call_1 : stepCalls) {
        _builder.append("       ");
        _builder.append(call_1, "       ");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String initCall(final String moduleName) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("initialize_task_");
    _builder.append(moduleName);
    _builder.append("()");
    return _builder.toString();
  }
  
  public static String stepCall(final String moduleName) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("step_task_");
    _builder.append(moduleName);
    _builder.append("()");
    return _builder.toString();
  }
}
