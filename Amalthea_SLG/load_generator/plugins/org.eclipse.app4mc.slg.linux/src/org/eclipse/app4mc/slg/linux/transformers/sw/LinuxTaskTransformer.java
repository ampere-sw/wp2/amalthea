/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.linux.transformers.sw;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.ASILType;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Group;
import org.eclipse.app4mc.amalthea.model.ILocalModeValueSource;
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.LocalModeLabelAssignment;
import org.eclipse.app4mc.amalthea.model.MapObject;
import org.eclipse.app4mc.amalthea.model.ModeLiteralConst;
import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.StringObject;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amalthea.model.impl.CustomPropertyImpl;
import org.eclipse.app4mc.amalthea.model.util.DeploymentUtil;
import org.eclipse.app4mc.slg.commons.m2t.AmaltheaModelUtils;
import org.eclipse.app4mc.slg.commons.m2t.CustomObjectsStore;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.config.CodehookType;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.util.ConfigModelUtils;
import org.eclipse.app4mc.slg.linux.generators.LinuxTaskGenerator;
import org.eclipse.app4mc.slg.linux.transformers.LinuxBaseTransformer;
import org.eclipse.app4mc.slg.linux.transformers.LinuxModel2TextTransformer;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class LinuxTaskTransformer extends LinuxBaseTransformer {

	private static final String KEY_CODEHOOK = "codehook";
	private static final String KEY_CODEHOOK_OVERWRITE = "codehook_overwrite";

	@Inject private Properties properties;
	@Inject private CustomObjectsStore customObjsStore;
	@Inject private SessionLogger logger;
	
	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final Task task) {
		final List<Object> key = List.of(task);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(task);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, task);
		}

		return tu;
	}

	// ---------------------------------------------------

	private SLGTranslationUnit createTranslationUnit(final Task task) {
		if ((task == null)) {
			return new SLGTranslationUnit("UNSPECIFIED TASK");
		} else {
			String basePath = "synthetic_gen";
			String moduleName = "tasks";
			String call = task.getName() + "()";
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	private void doTransform(final SLGTranslationUnit tu, final Task task) {
		genFiles(tu, task);
	}
	
	private class TaskDeps {
		public List<Label> inputLabels;
		public List<Label> outputLabels;

		public TaskDeps() {
			inputLabels = new ArrayList<Label>();
			outputLabels = new ArrayList<Label>();
		}
	};

	private class SchedulingParameters {
		public int period;
		public int deadline;
		public int runtime;

		public SchedulingParameters(int period, int deadline, int runtime) {
			this.period = period;
			this.deadline = deadline;
			this.runtime = runtime;
		}
	};

	private void addRunnable(Runnable runnable, RunnableCall runnableCall, List<TaskDeps> TaskDepsList) {
		TaskDeps NewTask = new TaskDeps();
		for (ActivityGraphItem runitem : runnable.getActivityGraph().getItems()) {
			if (runitem instanceof LabelAccess) {
				LabelAccess la = (LabelAccess) runitem;
				Label label = la.getData();

				if (la.getAccess() == LabelAccessEnum.READ) {
					NewTask.inputLabels.add(label);
				}
				if (la.getAccess() == LabelAccessEnum.WRITE) {
					NewTask.outputLabels.add(label);
				}
			}
		}
		TaskDepsList.add(NewTask);

	}

	private boolean isValidInput(Label label, int TaskID, List<TaskDeps> TaskDepsList) {

		for (int i = TaskID - 1; i >= 0; i--) {
			for (int j = 0; j < TaskDepsList.get(i).outputLabels.size(); j++) {
				if (TaskDepsList.get(i).outputLabels.get(j).equals(label))
					return true;
			}

		}
		for (int i = TaskID + 1; i < TaskDepsList.size(); i++) {
			for (int j = 0; j < TaskDepsList.get(i).outputLabels.size(); j++) {
				if (TaskDepsList.get(i).outputLabels.get(j).equals(label))
					return true;
			}

		}
		return false;
	}

	private boolean isValidOutput(Label label, int TaskID, List<TaskDeps> TaskDepsList) {
		for (int i = 0; i < TaskDepsList.size(); i++) {
			if (i == TaskID)
				continue;
			for (int j = 0; j < TaskDepsList.get(i).inputLabels.size(); j++) {
				if (TaskDepsList.get(i).inputLabels.get(j).equals(label))
					return true;
			}
			for (int j = 0; j < TaskDepsList.get(i).outputLabels.size(); j++) {
				if (TaskDepsList.get(i).outputLabels.get(j).equals(label))
					return true;
			}
		}
		return false;
	}
		
	private boolean processRunnable(Runnable runnable, RunnableCall runnableCall, List<String> statements,
			List<Label> externLabels, StringBuilder puDefinition, int id, List<TaskDeps> TaskDepsList,
			boolean hasOpenMP) {
		boolean hasParallelRunnables = false;
		String localMode = null;
		EList<LocalModeLabelAssignment> context = (runnableCall).getContext();

		for (LocalModeLabelAssignment localModeLabelAssignment : context) {

			ILocalModeValueSource valueSource = localModeLabelAssignment.getValueSource();

			if (valueSource != null && valueSource instanceof ModeLiteralConst) {

				localMode = ((ModeLiteralConst) valueSource).getValue().getName();
			}
		}
		int numReplications = 0;
		EList<Tag> tags = runnable.getTags();
		for (Tag runnableTag : tags) {
			if (runnableTag.getName().equals("SIL4")) {
				hasOpenMP = true;
				localMode = "OpenMP";
				numReplications = 2;
				break;
			}
		}

		if (localMode != null || hasOpenMP == true) {
			ArrayList<Label> LabelsOut = new ArrayList<Label>();
			ArrayList<Label> LabelsIn = new ArrayList<Label>();
			ArrayList<Label> LabelsInOut = new ArrayList<Label>();
			ArrayList<Label> ToRemove = new ArrayList<Label>();
			for (ActivityGraphItem runitem : runnable.getActivityGraph().getItems()) {
				if (runitem instanceof LabelAccess) {
					LabelAccess la = (LabelAccess) runitem;
					Label label = la.getData();


					if (la.getAccess() == LabelAccessEnum.READ && isValidInput(label, id, TaskDepsList)) {
						if (!externLabels.contains(label))
							externLabels.add(label);
						if (!LabelsOut.contains(label) && !LabelsIn.contains(label))
							LabelsIn.add(label);
					}
					if (la.getAccess() == LabelAccessEnum.WRITE && isValidOutput(label, id, TaskDepsList)) {
						if (!externLabels.contains(label))
							externLabels.add(label);
						if (!LabelsOut.contains(label))
							LabelsOut.add(label);
					}
				}
			}

			for (Label inLabel : LabelsIn) {
				if (LabelsOut.contains(inLabel)) {
					ToRemove.add(inLabel);
					LabelsInOut.add(inLabel);
				}
			}
			LabelsIn.removeAll(ToRemove);
			LabelsOut.removeAll(ToRemove);

			String isOpenMP = "\n#if defined(_OPENMP)";
			String isOMPSS = "\n#elif defined(_OMPSS)";

			String OpenMPPragma = "\n #pragma omp task";
			String OmpssPragma = "\n #pragma oss task";

			boolean isTarget = false;
			if (localMode != null && (localMode.equals("GPU") || localMode.equals("FPGA")))
				isTarget = true;

			String depends = "";
			String maps = "";
			if (LabelsIn.size() != 0) {
				depends += " depend(in:";
				if (isTarget)
					maps += " map(to:";
				for (Label inLabel : LabelsIn) {
					depends += inLabel.getName() + ",";
					if (isTarget)
						maps += inLabel.getName() + "[0:" + AmaltheaModelUtils.getLabelArraySize(inLabel) + "],";
				}
				if (isTarget) {
					maps = maps.substring(0, maps.length() - 1);
					maps += ")";
				}
				depends = depends.substring(0, depends.length() - 1);
				depends += ")";
			}
			if (LabelsOut.size() != 0) {
				depends += " depend(out:";
				if (isTarget)
					maps += " map(from:";
				for (Label outLabel : LabelsOut) {
					depends += outLabel.getName() + ",";
					if (isTarget)
						maps += outLabel.getName() + "[0:" + AmaltheaModelUtils.getLabelArraySize(outLabel) + "],";
				}
				if (isTarget) {
					maps = maps.substring(0, maps.length() - 1);
					maps += ")";
				}
				depends = depends.substring(0, depends.length() - 1);
				depends += ")";

			}
			if (LabelsInOut.size() != 0) {
				depends += " depend(inout:";
				if (isTarget)
					maps += " map(tofrom:";
				for (Label inoutLabel : LabelsInOut) {
					depends += inoutLabel.getName() + ",";
					if (isTarget)
						maps += inoutLabel.getName() + "[0:" + AmaltheaModelUtils.getLabelArraySize(inoutLabel) + "],";
				}
				if (isTarget) {
					maps = maps.substring(0, maps.length() - 1);
					maps += ")";
				}
				depends = depends.substring(0, depends.length() - 1);
				depends += ")";
			}

			if (numReplications > 0)
				depends += " replicated(" + numReplications + ", dummy, check)";

			OpenMPPragma += depends;
			if(isTarget)
				OpenMPPragma += "\n #pragma omp target "+ maps;

			OmpssPragma += depends;

			if (isTarget)
				OmpssPragma += maps + " copy_deps";

			statements.add(isOpenMP + OpenMPPragma + isOMPSS + OmpssPragma + "\n#endif\n");

			hasParallelRunnables = true;

		}
		return hasParallelRunnables;
	}

	private void genFiles(SLGTranslationUnit tu, final Task task) {

		boolean enableInstrumentation = Boolean.parseBoolean(properties.getProperty("enableInstrumentation_Runnables")); // runnable instrumentation
		boolean enableExtCode = Boolean.parseBoolean(properties.getProperty("enableExternalCode"));

		boolean extOverwrite = false; // enabling codehook overwriting

		final Set<ProcessingUnit> assignedCores = DeploymentUtil.getAssignedCoreForProcess(task);

		final StringBuilder puDefinition = new StringBuilder("default");

		if (assignedCores != null && !assignedCores.isEmpty()) {
			puDefinition.delete(0, puDefinition.length());

			ProcessingUnit pu = assignedCores.iterator().next();
			ProcessingUnitDefinition puDef = pu.getDefinition();
			String puDefName = (puDef == null) ? null : puDef.getName();

			puDefinition.append(puDefName);
		}

		final List<String> statements = new ArrayList<>();
		final List<Process> processedTasks = new ArrayList<>();
		final List<String> statementsOverwrite = new ArrayList<>();
		final List<Label> externLabels = new ArrayList<Label>();
		final List<TaskDeps> TaskDepsList = new ArrayList<TaskDeps>();
		boolean hasParallelRunnables = false;
		boolean printOMPHeader = false;
		boolean hasOpenMP = false;

		if (task.getCustomProperties().containsKey("OpenMP")) {
			Value V = task.getCustomProperties().get("OpenMP");
			EStructuralFeature feature = V.eClass().getEStructuralFeatures().get(0);
			hasOpenMP = (boolean) V.eGet(feature);
		}

		ArrayList<SchedulingParameters> threadParams =  new ArrayList<SchedulingParameters>();
		if (task.getCustomProperties().containsKey("SchedulingParameters")) {
			Value V = task.getCustomProperties().get("SchedulingParameters");
			EStructuralFeature feature = V.eClass().getEStructuralFeatures().get(0);
			EObjectContainmentEList schedulingList = (EObjectContainmentEList) V.eGet(feature);
			for (Object threadMapping : schedulingList) {
				int period = 0;
				int deadline = 0;
				int runtime = 0;
				MapObject map = (MapObject) threadMapping;
				for(Map.Entry<String,Value> entry: map.getEntries()) {
					EStructuralFeature timeFeature= entry.getValue().eClass().getEStructuralFeatures().get(0);
					int time = (int) entry.getValue().eGet(timeFeature);
					if(entry.getKey().equals("period")) {
						period = time;
					}
					else if(entry.getKey().equals("deadline")) {
						deadline = time;
					}
					else if(entry.getKey().equals("runtime")) {
						runtime= time;
					}
				}
				threadParams.add(new SchedulingParameters(period,deadline,runtime));
			}
		}
		String schedString = "";
		for (int j = 0 ; j < threadParams.size() ; j++)
			schedString += "thread_sched("+ j +"," + threadParams.get(j).period+"," + threadParams.get(j).deadline + "," + threadParams.get(j).runtime + ") ";

		String Parallel = String.join("\n", "#if defined(_OPENMP)", " #pragma omp parallel " + schedString, " #pragma omp single",
				" #pragma omp taskgraph tdg_type(static)", "#endif", "{");

		if (task != null && task.getActivityGraph() != null) {
			for (ActivityGraphItem item : task.getActivityGraph().getItems()) {

				if (item instanceof Group) {
					for (ActivityGraphItem item2 : ((Group) item).getItems()) {
						if ((item2 instanceof RunnableCall)) {
							final Runnable runnable = ((RunnableCall) item2).getRunnable();
							if ((runnable != null)) {
								addRunnable(runnable, (RunnableCall) item2, TaskDepsList);
							}
						}
					}
				} else if (item instanceof RunnableCall) {
					final Runnable runnable = ((RunnableCall) item).getRunnable();
					if ((runnable != null)) {
						addRunnable(runnable, (RunnableCall) item, TaskDepsList);
					}
				}
			}
		}

		int TaskId = 0;
		if (task != null && task.getActivityGraph() != null) {
			for (final EObject item : task.getActivityGraph().eContents()) {

				if (item instanceof CustomPropertyImpl) {
					CustomPropertyImpl customProp = (CustomPropertyImpl) item;

					if (enableExtCode) {

						if (customProp.getKey().equals(KEY_CODEHOOK)) {
							Value value = customProp.getValue();
							if (value instanceof StringObject) {
								statements.add(((StringObject) value).getValue() + ";");
							}

						} else if (customProp.getKey().equals(KEY_CODEHOOK_OVERWRITE)) {
							extOverwrite = true;
							Value value1 = customProp.getValue();
							if (value1 instanceof StringObject) {
								statementsOverwrite.add(((StringObject) value1).getValue());
							}
						}
					}
				}

				if (item instanceof Group) {
					for (ActivityGraphItem item2 : ((Group) item).getItems()) {

						if ((item2 instanceof RunnableCall)) {
							final Runnable runnable = ((RunnableCall) item2).getRunnable();
							hasParallelRunnables = processRunnable(runnable, (RunnableCall) item, statements,
									externLabels, puDefinition, TaskId, TaskDepsList, hasOpenMP);
							if (hasParallelRunnables)
								printOMPHeader = true;
							if (enableInstrumentation) {
								statements.add("instr_start();");
								statements.add("run_" + runnable.getName() + "(\"" + puDefinition + "\");");
								statements.add("instr_stop();");
							} else {
								statements.add("run_" + runnable.getName() + "(\"" + puDefinition + "\");");
							}
							TaskId++;
						} else if ((item2 instanceof InterProcessTrigger)) {
							InterProcessStimulus stimulus = ((InterProcessTrigger) item2).getStimulus();
							LinuxTaskGenerator.handleInterProcessTrigger(statements, processedTasks, stimulus);
						}
					}

				} else if (item instanceof RunnableCall) {
					final Runnable runnable = ((RunnableCall) item).getRunnable();
					hasParallelRunnables = processRunnable(runnable, (RunnableCall) item, statements, externLabels,
							puDefinition, TaskId, TaskDepsList, hasOpenMP);
					if (hasParallelRunnables)
						printOMPHeader = true;
					if ((runnable != null)) {

						List<String> contextParameters = new ArrayList<>();
						List<LocalModeLabelAssignment> context = ((RunnableCall) item).getContext();

						for (LocalModeLabelAssignment localModeLabelAssignment : context) {

							ILocalModeValueSource valueSource = localModeLabelAssignment.getValueSource();

							/*
							 * if(valueSource !=null && valueSource instanceof ModeLiteralConst) {
							 * 
							 * contextParameters.add("\""+((ModeLiteralConst)valueSource).getValue().getName
							 * ()+"\""); }
							 */
						}

						if (enableInstrumentation) {
							statements.add("instr_start();");
							/*
							 * if(contextParameters.size()>0) {
							 * 
							 * statements.add("run_" + runnable.getName()+"_Context" + "(\"" +
							 * puDefinition.toString() + "\" , "+String.join(",", contextParameters)+");");
							 * }else {
							 */
							statements.add("run_" + runnable.getName() + "(\"" + puDefinition.toString() + "\");");

							// }
							statements.add("instr_stop();");
						} else {
							/*
							 * if(contextParameters.size()>0) {
							 * 
							 * statements.add("run_" + runnable.getName()+"_Context" + "(\"" +
							 * puDefinition.toString() + "\" , "+String.join(",", contextParameters)+");");
							 * }else {
							 */
							statements.add("run_" + runnable.getName() + "(\"" + puDefinition.toString() + "\");");

							// }
						}
						TaskId++;
					}

				} else if (item instanceof InterProcessTrigger) {
					InterProcessStimulus stimulus = ((InterProcessTrigger) item).getStimulus();
					LinuxTaskGenerator.handleInterProcessTrigger(statements, processedTasks, stimulus);
				}
			}
		}
		if (printOMPHeader) {
			this.properties.setProperty("OpenMP", "true");
			statements.add(0, Parallel);
			statements.add("}");
		}
		if (isIncFileEmpty(tu)) {
			// ----------------------------------/* fetch headers
			// names*/--------------------------------------
			// to fetch the headers names we use the getHeaderFilesDirectories function and
			// we go through every directory with the for loop
			final ConfigModel configModel = customObjsStore.<ConfigModel>getInstance(ConfigModel.class);

			if (enableExtCode) // input property to test if we need to include the external code headerfiles
			{
				String workingDirectory = customObjsStore.getData(TransformationConstants.WORKING_DIRECTORY);

				for (String hDir : ConfigModelUtils.getHeaderFilesDirectories(configModel, CodehookType.TASK, workingDirectory, logger)) {
					final File folder = new File(hDir.trim()); // fetching all the names in the headerfile directory.
					String names = ConfigModelUtils.getHeaderFilesIncludeMultiString(folder, logger);
					incAppend(tu, names);
				}
			}
			// ------------------------------------------------------------------------

			incAppend(tu, LinuxTaskGenerator.snippetIncStart());

		}
		if (isSrcFileEmpty(tu)) {
			srcAppend(tu, LinuxTaskGenerator.snippetSrcStart(enableInstrumentation));
			String DummyAndCheckFunc = "int dummy; \nvoid check(int *original, int *replicated){}\n";
			srcAppend(tu, DummyAndCheckFunc);
		}

		for (Label externLa : externLabels) {
			long num = AmaltheaModelUtils.getLabelArraySize(externLa);
			if (num == 0)
				num = 1;
			srcAppend(tu, "extern int " + externLa.getName() + "[" + num + "]; \n");
		}
		// ------------------------ write body with overwrite codehook function

		if (extOverwrite) {
			// This code is
			String statement_overwrite = "void " + task.getName() + "(){ \n\n";
			srcAppend(tu, statement_overwrite);

			for (String statement : statementsOverwrite) {
				srcAppend(tu, statement + ";" + "\n");
			}
			srcAppend(tu, "\n" + "}" + "\n");

			srcAppend(tu, "\n void *" + task.getName() + "_entry(){ \n \n");
			srcAppend(tu, task.getName() + "(); \n");
			srcAppend(tu, "\n" + "}" + "\n");

		} else {
			srcAppend(tu, LinuxTaskGenerator.toCpp(task, statements));
		}
		incAppend(tu, LinuxTaskGenerator.toH(task));
	}

}
