/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.linux.generators

import org.eclipse.app4mc.amalthea.model.Label
import org.eclipse.app4mc.slg.commons.m2t.AmaltheaModelUtils

class LinuxLabelGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	// ---------- names of generated 'C' functions ----------

	static def String initCall(Label label) 			'''initialize_«label.name»()'''	
	static def String readCall(Label label, String param) 	'''read_«label.name»(«param»)'''	
	static def String writeCall(Label label, String param) '''write_«label.name»(«param»)'''	

	// ---------- generate file contents ----------
	
	static def String toH(Label label) '''
	void «initCall(label)»;
	void «readCall(label, "int labelAccessStatistics")»;
	void «writeCall(label, "int labelAccessStatistics")»;
	
	'''

	static def String toCpp(Label label) {
		val name = if (label?.name.isNullOrEmpty) "<undefined label>" else label.name
		val numberOfBytes = if (label?.size === null) 0 else label.size.numberBytes

		'''
		int «name»[«AmaltheaModelUtils.getLabelArraySize(label)»];

		static bool isIinitialized_«name» = false;
		void «initCall(label)» {
			if (!isIinitialized_«name»){
				int i;
				for (i=0; i < «AmaltheaModelUtils.getLabelArraySize(label)»; i++){
					«name»[i] = i+1;
				}
				isIinitialized_«name» = true;
			}
		}

		void «readCall(label, "int labelAccessStatistics")» {
			int numberOfBytes = «numberOfBytes»;
			int repeat;
			for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
				if(numberOfBytes < 4){
					numberOfBytes = 4;
				}
				int arraysize = sizeof(«name») / 4;
				
				//printf("number of bytes:»d\n",arraysize);
				int leftOverElements=arraysize»10;
				
				int arraySizeWith10Multiples=arraysize-leftOverElements;
				int i = 0;
				int a = 0;
				for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
					a = «name»[i];
					a = «name»[i+1];
					a = «name»[i+2];
					a = «name»[i+3];
					a = «name»[i+4];
					a = «name»[i+5];
					a = «name»[i+6];
					a = «name»[i+7];
					a = «name»[i+8];
					a = «name»[i+9];
				}
				for(;i<arraysize;i++){
					a = «name»[i];
				}
			}
		}

		void «writeCall(label, "int labelAccessStatistics")» {
			int numberOfBytes = «numberOfBytes»;
			int repeat;
			for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
				if(numberOfBytes < 4){
					numberOfBytes = 4;
				}
				int arraysize = sizeof(«name») / 4;
				int leftOverElements=arraysize»10;
				int arraySizeWith10Multiples=arraysize-leftOverElements;
				
				int i = 0;
				for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
					«name»[i]   = 0xAFFE;
					«name»[i+1] = 0xAFFE;
					«name»[i+2] = 0xAFFE;
					«name»[i+3] = 0xAFFE;
					«name»[i+4] = 0xAFFE;
					«name»[i+5] = 0xAFFE;
					«name»[i+6] = 0xAFFE;
					«name»[i+7] = 0xAFFE;
					«name»[i+8] = 0xAFFE;
					«name»[i+9] = 0xAFFE;
				}
				for(;i<arraysize;i++){
						«name»[i]=0xAFFE;
				}
			}
		}
		
		'''
	}
}
