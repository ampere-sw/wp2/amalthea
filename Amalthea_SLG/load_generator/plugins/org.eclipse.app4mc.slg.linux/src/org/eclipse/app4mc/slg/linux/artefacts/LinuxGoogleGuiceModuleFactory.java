/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.linux.artefacts;

import java.util.Properties;

import org.eclipse.app4mc.transformation.ServiceConstants;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;

@Component(
	configurationPid = ServiceConstants.SESSION_CONFIGURATION_PID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	service = LinuxGoogleGuiceModuleFactory.class
)
public class LinuxGoogleGuiceModuleFactory {

	public LinuxGoogleGuiceModule getModule(SessionLogger logger, Properties properties) {
		return new LinuxGoogleGuiceModule(logger, properties);
	}

}
