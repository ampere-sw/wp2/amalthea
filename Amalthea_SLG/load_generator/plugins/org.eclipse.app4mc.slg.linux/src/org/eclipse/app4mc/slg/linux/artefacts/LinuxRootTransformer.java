/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.linux.artefacts;

import java.io.File;
import java.util.Map;
import java.util.Properties;

import org.eclipse.app4mc.slg.linux.transformers.LinuxModel2TextTransformer;
import org.eclipse.app4mc.transformation.ServiceConstants;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.transformation.transformers.Model2TextRootTransformer;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.google.inject.Guice;
import com.google.inject.Injector;

@Component(
	configurationPid = ServiceConstants.SESSION_CONFIGURATION_PID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	property = { ServiceConstants.TRANSFORMATION_PROPERTY + "=LINUX_SLG" },
	service = Model2TextRootTransformer.class
)
public class LinuxRootTransformer extends Model2TextRootTransformer {

	@Reference
	private LinuxGoogleGuiceModuleFactory moduleFactory;

	Properties properties;

	@Activate
	@Override
	protected void activate(Map<String, ?> properties) {
		super.activate(properties);
		this.properties = new Properties();
		this.properties.putAll(properties);
		
		// set the output directory to the local properties
		this.properties.put(TransformationConstants.TRANSFORMATION_OUTPUT_FOLDER, getOutputFolder());
	}

	@Override
	public void m2tTransformation() {

		Injector injector = Guice.createInjector(moduleFactory.getModule(logger, properties));

		LinuxModel2TextTransformer instance = injector.getInstance(LinuxModel2TextTransformer.class);

		harmonizeSLGParams(properties);

		instance.m2tTransformation(getInputResourceSet(getInputFolder(), logger));
	}

	private void harmonizeSLGParams(Properties props) {

		if (!props.containsKey("configurationFile")) {
			logger.error("'configurationFile' property not set in the input properties file");
			throw new IllegalArgumentException("'configurationFile' property not set in the input properties file");

		}

		harmonizeSLGParam(props, TransformationConstants.WORKING_DIRECTORY, "configurationFile");
	}

	private void harmonizeSLGParam(Properties props, String workingDirectoryKey, String param) {

		String workingDirectory = (String) props.get(workingDirectoryKey);
		String paramValue = (String) props.get(param);

		if (paramValue != null && workingDirectory != null) {

			File file = null;

			file = new File(paramValue);

			if (!file.exists()) {

				file = new File(workingDirectory, paramValue);

				if (file.exists()) {
					props.put(param, file.getAbsolutePath());
				}
			}
		}
	}

}
