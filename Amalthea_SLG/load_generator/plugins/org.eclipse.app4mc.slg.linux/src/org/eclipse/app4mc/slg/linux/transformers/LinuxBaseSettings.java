/**
********************************************************************************
* Copyright (c) 2021 Robert Bosch GmbH.
* 
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
* 
* SPDX-License-Identifier: EPL-2.0
* 
* Contributors:
*     Robert Bosch GmbH - initial API and implementation
********************************************************************************
*/

package org.eclipse.app4mc.slg.linux.transformers;

import org.eclipse.app4mc.transformation.util.OutputBuffer;

public class LinuxBaseSettings {

	// Suppress default constructor
	private LinuxBaseSettings() {
		throw new IllegalStateException("Utility class");
	}

	public static final String SRC_TYPE = "SRC";
	public static final String SRC_EXT = ".c";
	public static final String SRC_FOLDER = "/_src/";

	public static final String INC_TYPE = "INC";
	public static final String INC_EXT = ".h";
	public static final String INC_FOLDER = "/_inc/";

	public static final String OTHER_TYPE = "OTHER";

	public static void initializeOutputBuffer(OutputBuffer buffer, String outputFolder) {
		buffer.initialize(outputFolder);
		buffer.configureFiletype(SRC_TYPE, SRC_EXT, "// This code is auto-generated\n\n", null);
		buffer.configureFiletype(INC_TYPE, INC_EXT, "// This code is auto-generated\n\n#pragma once\n\n", null);
		buffer.configureFiletype(OTHER_TYPE, "", "", "");
	}

}
