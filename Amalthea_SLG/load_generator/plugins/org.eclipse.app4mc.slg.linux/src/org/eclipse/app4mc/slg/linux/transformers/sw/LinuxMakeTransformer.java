/**
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.linux.transformers.sw;

import com.google.inject.Inject;

import org.eclipse.app4mc.slg.commons.m2t.CustomObjectsStore;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.linux.generators.LinuxMakeGenerator;
import org.eclipse.app4mc.slg.linux.transformers.LinuxBaseTransformer;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.transformation.util.OutputBuffer;

import java.util.Properties;


public class LinuxMakeTransformer extends LinuxBaseTransformer {
    @Inject
    private OutputBuffer outputBuffer;
    @Inject
    private Properties properties;
    @Inject
    private CustomObjectsStore customObjsStore;

    public void transform() {
        final boolean experimentalCodeMatching = Boolean.parseBoolean(this.properties.getProperty(
                    "experimentalCodeSnippetMatching", "false"));
        final boolean instrumentation = Boolean.parseBoolean(this.properties.getProperty(
                    "enableInstrumentation_Tasks", "false"));
        final boolean instrumentation_R = Boolean.parseBoolean(this.properties.getProperty(
                    "enableInstrumentation_Runnables", "false"));

        final boolean externalCode = Boolean.parseBoolean(this.properties.getProperty(
                    "enableExternalCode", "false"));
        final boolean hasOpenMP = Boolean.parseBoolean(this.properties.getProperty(
                "OpenMP", "false"));

        final ConfigModel configModel = customObjsStore.<ConfigModel>getInstance(ConfigModel.class);
        final String workingDirectory = customObjsStore.getData(TransformationConstants.WORKING_DIRECTORY);

        this.outputBuffer.appendTo("OTHER", "Makefile",
            LinuxMakeGenerator.getContent(experimentalCodeMatching,
                instrumentation, instrumentation_R, externalCode, hasOpenMP, configModel,
                workingDirectory));
    }
}
