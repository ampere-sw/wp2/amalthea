/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.linux.generators

import java.util.ArrayList
import java.util.List

class LinuxRealisiticSyntheticGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static val codeSnippets = new ArrayList<CodeSnippet>

	static def List<CodeSnippet> getCodeSnippets() {
		if (codeSnippets.isEmpty) {
			initCodeSnippets()
		}
		return codeSnippets.immutableCopy
	}

	static private def initCodeSnippets() {
		val String bubblesortCode = '''
			int arraylength = 400;
			int array1[400] = {952, 954, 790, 167, 904, 692, 259, 20, 85, 381, 494, 732, 192, 991, 865, 481, 288, 428, 574, 454, 303, 618,
				573, 7, 143, 918, 625, 765, 839, 87, 53, 143, 393, 195, 663, 297, 239, 274, 317, 325, 655, 163, 57, 848, 154, 922, 329, 794,
				702, 256, 248, 5, 226, 821, 365, 370, 740, 990, 135, 931, 429, 188, 74, 822, 383, 737, 471, 623, 12, 140, 300, 667, 303,
				357, 515, 809, 631, 197, 603, 685, 453, 851, 43, 679, 24, 408, 49, 764, 398, 184, 695, 179, 724, 122, 353, 108, 859, 824,
				83, 223, 964, 735, 243, 619, 92, 758, 428, 75, 955, 31, 760, 760, 234, 803, 440, 258, 563, 841, 23, 313, 378, 70, 492, 102,
				544, 845, 562, 756, 21, 645, 979, 985, 380, 222, 956, 824, 333, 384, 251, 640, 767, 364, 401, 1, 519, 193, 260, 83, 34, 635,
				396, 412, 57, 241, 515, 602, 438, 429, 358, 460, 427, 337, 797, 159, 912, 754, 336, 597, 490, 587, 237, 258, 951, 990, 259,
				471, 183, 871, 906, 570, 858, 302, 334, 916, 543, 201, 870, 334, 631, 228, 794, 58, 917, 943, 217, 181, 49, 553, 778, 540,
				493, 16, 798, 796, 358, 409, 267, 542, 633, 173, 464, 491, 828, 798, 759, 723, 0, 629, 57, 631, 209, 203, 41, 479, 147, 258,
				660, 196, 164, 439, 736, 9, 807, 886, 805, 165, 648, 73, 59, 281, 598, 523, 124, 778, 322, 236, 502, 674, 217, 911, 305, 427,
				115, 698, 258, 262, 308, 918, 458, 472, 709, 547, 481, 868, 785, 287, 386, 433, 712, 445, 66, 662, 321, 543, 441, 995, 779,
				295, 669, 996, 206, 326, 775, 321, 24, 33, 935, 332, 304, 746, 157, 365, 293, 638, 234, 78, 277, 620, 864, 341, 417, 930, 4,
				738, 473, 797, 733, 604, 444, 402, 953, 2, 728, 728, 676, 104, 114, 611, 437, 418, 357, 594, 783, 2, 584, 369, 81, 214, 989,
				297, 555, 407, 227, 911, 497, 53, 60, 231, 657, 504, 633, 610, 507, 714, 691, 183, 818, 157, 146, 607, 927, 504, 201, 62, 858,
				138, 432, 291, 352, 773, 588, 907, 532, 816, 171, 30, 869, 231, 261, 878, 736, 246, 841, 595, 312, 884, 130, 131, 41, 276, 90,
				968, 132, 292, 30, 991, 430, 462, 282, 782, 588, 223, 41, 120, 39, 212, 150, 260, 444, 411};
			
			int tmp, i, j;
			
			for (i = 1; i < arraylength ; i++) 
			{
				for (j = 0; j < arraylength - i ; j++) 
				{
				   	if (array1[j] > array1[j + 1]) 
				   	   {
				   	   	tmp = array1[j];
				   	   	   array1[j] = array1[j + 1];
				   	   	   array1[j + 1] = tmp;
				   	   }
				   }
			}
		'''
		val bubblesort = new CodeSnippet(3523325, 925649, 202672, 276, 1600, bubblesortCode, "bubblesort", 0)
		codeSnippets.add(bubblesort)

		val String euklidcode = '''
			long long num1 = 72984718;
			long long num2 = 204;
			if(num1 == 0){
			}
			else{
			    while(num2 != 0){
					if(num1 > num2){
						num1 = num1 - num2;
					}
					else{
						num2 = num2 - num1;
					}
				}
			}
		'''
		val euklid = new CodeSnippet(5009893, 1788917, 357792, 5, 329, euklidcode, "euklid", 0)
		codeSnippets.add(euklid)

		val String maxSucheCode = '''
			int length = 400;
			int array[400] = {999,204,362,149,165,551,604,336,463,19,924,355,4,268,450,560,563,61,793,401,347,707,516,314,502,438,818,
				974,923,892,895,923,448,257,72,613,160,28,301,624,47,226,979,51,494,781,612,57,842,757,810,542,816,678,208,319,469,26,645,392,270,
				540,667,718,149,91,332,310,119,633,934,166,211,265,570,57,399,182,467,593,939,629,135,755,308,344,426,777,722,71,521,993,963,189,
				711,465,280,395,775,752,381,61,918,592,678,488,2,77,22,469,671,961,98,158,69,758,502,495,887,225,919,409,570,882,598,281,347,230,
				677,474,982,58,535,253,2,566,741,356,643,116,825,666,77,276,825,498,34,679,346,922,256,265,683,826,147,633,108,847,863,137,321,198,
				547,209,451,549,775,544,906,770,660,83,437,90,711,614,588,746,293,934,20,550,551,55,376,51,688,836,898,551,973,219,101,520,780,552,
				422,907,97,680,678,109,115,467,199,827,433,788,925,726,74,297,628,978,352,357,29,40,545,927,943,519,498,45,391,631,949,165,538,398,
				845,568,508,961,387,707,788,820,847,713,899,274,362,527,252,714,236,281,106,782,560,49,653,410,446,44,41,748,210,580,146,407,500,
				654,368,888,714,508,708,561,573,607,835,935,487,87,649,723,720,755,505,632,157,158,43,955,555,84,703,117,16,850,524,517,856,893,405,
				570,753,465,132,327,73,319,262,560,759,264,283,831,371,141,464,880,651,859,836,206,295,891,675,312,93,200,181,950,445,938,872,198,
				403,356,877,828,676,492,740,787,756,24,618,479,517,82,360,520,941,548,79,589,439,754,253,885,954,434,187,399,724,59,950,479,416,827,
				308,444,319,48,231,427,424,849,907,293,284,619,814,577,167,893,166,958,647,419,843,954,205,382,705,281,442,655,761,210,835,421,654,
				154,821,237};
			if(length>0){
				int max = array[0];
				int i;
				for(i = 0; i < length; i ++){
					if(array[i] > max){
						max = array[i];
					}
				}
			}
		'''
		val maxsuche = new CodeSnippet(12154, 2668, 508, 300, 1390, maxSucheCode, "maxSuche", 0)
		codeSnippets.add(maxsuche)

		val String fakArrayCode = '''
			int length = 10;
			int i, j;
			int array[10] = {7,2,7,2,12,1,11,10,9,8};
			int fak[10] = {1,1,1,1,1,1,1,1,1,1};
			for(i = 0; i < length; i ++){
				for(j = 1; j <= array[i]; j++){
					fak[i] *= j;
				}
			}
		'''
		val fakArray = new CodeSnippet(3584, 636, 176, 5, 604, fakArrayCode, "fakArray", 0)
		codeSnippets.add(fakArray)

		val String hanoiCode = '''
			void hanoi(int hoehe, int stab1, int stab2, int stab3){
				if(hoehe==1){
				       return;
				   }
				   hanoi(hoehe-1,stab1,stab3,stab2);
				   hanoi(1,stab1,stab2,stab3);
				   hanoi(hoehe-1,stab2,stab1,stab3);
			}
			hanoi(12,1,2,3);
		'''
		val hanoi = new CodeSnippet(136909, 34815, 30720, 5, 345, hanoiCode, "hanoi", 0)
		codeSnippets.add(hanoi)

		var String collatzCode = '''
			int n = 20837982;
			    while (n!=1) {
			     if (n%2==0) {
			         n=n/2;
			     }
			     else {
			         n=3*n+1;
			     }
			    }
		'''
		val collatz = new CodeSnippet(4997, 772, 263, 5, 686, collatzCode, "collatz", 0)
		codeSnippets.add(collatz)

		val String duplikateCode = '''
			int len = 400;
			int array2[400] = {54, 497, 683, 885, 642, 588, 236, 123, 726, 371, 663, 810, 57, 706, 658, 294, 112, 59, 737,
			167, 111, 58, 68, 596, 723, 757, 634, 974, 979, 741, 918, 34, 590, 601, 271, 232, 541, 508, 356, 267, 231, 371,
			77, 640, 77, 87, 934, 189, 147, 671, 356, 610, 82, 777, 206, 805, 886, 192, 780, 866, 285, 50, 252, 875, 4, 875,
			459, 545, 383, 815, 165, 615, 538, 594, 255, 615, 682, 542, 805, 829, 565, 513, 439, 647, 642, 997, 805, 529,
			189, 585, 747, 826, 987, 351, 701, 991, 226, 160, 889, 610, 328, 54, 577, 218, 648, 832, 834, 330, 726, 991,
			511, 292, 504, 302, 291, 147, 651, 96, 28, 192, 33, 775, 18, 21, 126, 71, 364, 352, 584, 253, 314, 264, 307, 891,
			482, 308, 76, 316, 990, 802, 659, 854, 446, 164, 156, 738, 663, 160, 186, 691, 352, 572, 466,
			371, 593, 944, 794, 309, 296, 378, 563, 611, 642, 222, 502, 125, 530, 930, 793, 873, 733, 453,
			727, 531, 969, 883, 621, 984, 43, 808, 675, 748, 380, 493, 119, 325, 437, 913, 634, 85, 644,
			549, 48, 286, 772, 903, 763, 654, 833, 557, 527, 918, 362, 254, 450, 331, 490, 71, 315, 885,
			231, 342, 633, 963, 187, 104, 640, 624, 370, 627, 61, 14, 176, 110, 652, 300, 365, 416, 307,
			198, 325, 834, 469, 687, 441, 919, 370, 283, 342, 37, 168, 926, 731, 154, 889, 918, 610, 530,
			894, 980, 157, 955, 346, 685, 417, 351, 986, 782, 767, 293, 333, 444, 479, 802, 483, 272, 
			73, 205, 555, 415, 242, 76, 341, 973, 582, 583, 891, 192, 113, 785, 525, 622, 92, 223, 307,
			862, 574, 645, 996, 693, 290, 329, 137, 122, 483, 620, 746, 556, 825, 654, 324, 419, 730, 17, 
			392, 664, 600, 635, 208, 65, 772, 733, 39, 865, 957, 699, 79, 883, 344, 75, 577, 987, 757, 714,
			461, 240, 687, 207, 149, 864, 861, 825, 284, 591, 842, 28, 607, 795, 664, 816, 212, 788, 901,
			252, 5, 210, 951, 84, 94, 647, 160, 671, 986, 917, 737, 447, 509, 424, 655, 658, 289, 516,
			835, 573, 460, 30, 953, 67, 825, 617, 235, 37, 406, 137, 641, 763, 347, 944, 848, 441, 944,
			360, 464, 930, 277, 202, 730, 786, 978, 385, 797, 267, 253, 984, 192, 65, 14, 146, 485, 191};
			int i, j;
			for(i=0;i<len;i++)
				for(j=i+1;j<len;j++)
				   	if(array2[i]==array2[j]){
				   	}
		'''
		val duplikate = new CodeSnippet(653507, 242066, 80707, 163, 81932, duplikateCode, "duplikate", 0)
		codeSnippets.add(duplikate)

		val String dameCode = '''
			int dame_in_gefahr(int *schachbrett) {
				int x,y;
				for(x=0; x<7; x++)
					if(schachbrett[x])
						for(y=x+1; y<=7; y++)
							if(schachbrett[y])  {
								if(schachbrett[x]==schachbrett[y])
									return 1;
								if(abs(x-y)==abs(schachbrett[x]-schachbrett[y]))
									return 2;
							}
				return 0;
			}
			int dame_(int *schachbrett, int position) {
			   int x = 1, i;
			   static int counter = 1;
			   while(x <= 8) {
			    schachbrett[position]=x;
			    if(!dame_in_gefahr(schachbrett)) {
			      if(position) {
			      if(dame_(schachbrett,position-1))
			       return 1;
			   }
			   else {
			   	
			   }
					}
					x++;
				}
				schachbrett[position] = 0;
				return 0;
			}
			int schachbrett[8], x;
			for(x=0; x < 8; x++)
				schachbrett[x] = 0;
			dame_(schachbrett,7);
		'''
		val dame = new CodeSnippet(5514300, 1417457, 191554, 236, 136737, dameCode, "dame", 0)
		codeSnippets.add(dame)

		val String kmpCode = '''
			int next[4096];
			   void init_next(char *muster, int m) {
					int i, j;
			
					i = 0;
					j = next[0] = -1;
					while (i < m) {
						while (j > -1 && muster[i] != muster[j])
							j = next[j];
						i++;
						j++;
						(muster[i] == muster[j]) ? (next[i]=next[j]) : (next[i]=j);
					}
				}
				  char *muster = "alalasla";
				  char *text = "lu lalalala pipapo 2 1 alas ala lasla lulalas lule alalasl alal ala lulaalasla lulalalasla";
				  int i=0, j=0;
				  int m=strlen(muster);
				  int n=strlen(text);
			
			   init_next(muster, m);
			   while (i<n) {
			      while (j>=0 && text[i]!=muster[j])j=next[j];
			      i++; j++;
			      if (j==m) {
			         j = next[j];
			      }
			   }
		'''
		val kmp = new CodeSnippet(10652, 1899, 359, 173, 2040, kmpCode, "kmp", 60)
		codeSnippets.add(kmp)

		val String selectionsortCode = '''
			int i;
			   int array5[] = { 10, 9, 20, 18, 99, 52, 1, 37, 22, 10, 52, 59, 97, 12, 5, 0, 1, 17, 29, 1, 21, 77, 92, 91, 90, 81, 2, 1, 2, 93 };
			   int N = sizeof(array5)/sizeof(int);
			   int elemente = N-1;
			   
			   int index,index_klein,
			       wert, wert_klein;
			   for(index = 0; index < elemente; index++) {
			      wert=index;
			      for(index_klein = index+1; index_klein <= elemente;
			        index_klein++) { /* Ein kleineres Element gefunden? */
			         if(array5[index_klein] < array5[wert])
			            wert=index_klein;
			      }
			      if(wert != index) {
			         wert_klein=array5[wert];
			         array5[wert]=array5[index];
			         array5[index]=wert_klein;
			      }
			   }
		'''
		val selectionsort = new CodeSnippet(16694, 3750, 735, 7, 1357, selectionsortCode, "selectionsort", 0)
		codeSnippets.add(selectionsort)

		val String insertionsortCode = '''
			int array7[] = { 67, 42, 80, 51, 4, 19, 8, 31, 62, 27, 18, 17, 82, 68, 73, 41, 98, 39, 55, 32, 49, 54, 27, 16, 34, 57, 53, 48, 41, 37, 17, 42, 21 };
			   int N = sizeof(array7)/sizeof(int);
			   int elemente = N-1;
			   int index,index_klein,wert_klein;
			   for(index=1; index<=elemente; index++) {
			      wert_klein=array7[index];
			      for( index_klein=index;
			           array7[index_klein-1] > wert_klein&&index_klein > 0;
			           index_klein-- )
			         array7[index_klein] = array7[index_klein-1];
			      array7[index_klein]=wert_klein;
			   }
		'''
		val insertionsort = new CodeSnippet(14695, 2832, 778, 10, 1298, insertionsortCode, "insertionsort", 0)
		codeSnippets.add(insertionsort)

		val String shellsortCode = '''
			int array9[] = { 77, 32, 90, 2, 44, 1, 98, 3, 12, 17, 98, 1, 92, 88, 93, 21, 48, 19, 85, 82, 19, 94, 57, 1, 3, 5, 5, 28, 4, 87, 37, 92, 2 };
			   int N = sizeof(array9)/sizeof(int);
			   int elemente = N-1;
			   int index,index_klein,wert_klein;
			   for(index=1; index<=elemente; index++) {
			      wert_klein=array9[index];
			      for( index_klein=index;
			           array9[index_klein-1] > wert_klein&&index_klein > 0;
			           index_klein-- )
			         array9[index_klein] = array9[index_klein-1];
			      array9[index_klein]=wert_klein;
			   }
		'''
		val shellsort = new CodeSnippet(14873, 2784, 766, 7, 1332, shellsortCode, "shellsort", 40)
		codeSnippets.add(shellsort)

		val String primeCode = '''
			int n = 99829, i, flag = 0;
			for (i = 2; i <= n / 2; ++i) {
				if (n % i == 0) {
					flag = 1;
					      break;
					  }
			}
			if (n == 1) {
				return;
			}
			else {
				if (flag == 0)
					return;
				else
				       return;
			}
		'''
		val prime = new CodeSnippet(3027342, 299497, 99839, 5, 374, primeCode, "prime", 0)
		codeSnippets.add(prime)

		val String triangleCode = '''
			int rows = 17, coef = 1, space, i, j;
			    for (i = 0; i < rows; i++) {
			        for (space = 1; space <= rows - i; space++)
			            for (j = 0; j <= i; j++) {
			                if (j == 0 || i == 0)
			                    coef = 1;
			                else
			                    coef = coef * (i - j + 1) / j;
			            }
			    }
		'''
		val triangle = new CodeSnippet(50643, 8992, 2291, 5, 1330, triangleCode, "triangle", 0)
		codeSnippets.add(triangle)

		val String multiplymatricesCode = '''
			int matrix_a[5][5] = {72, 31, 43, 2, 91, 3, 17, 2, 93, 77, 91, 10, 31, 7, 14, 5, 22, 10, 96, 19, 17, 14, 12, 7, 2};
			int matrix_b[5][5] = {56, 2, 77, 32, 40, 1, 99, 15, 36, 21, 59, 3, 79, 81, 83, 52, 18, 48, 98, 32, 57, 12, 3, 5, 45};
			int matrix_r[5][5] = {0};
			int i, j, k;
			for (i = 0 ; i < 5 ; i++) {
				for (j = 0 ; j < 5 ; j++) {
				   	float sum = 0.0 ;
				   	   for (k = 0 ; k < 5 ; k++) {
				   	   	sum = sum + matrix_a[i][k] * matrix_b[k][j] ;
				   	   } 
				   	   matrix_r[i][j] = sum ;
				   }
			}
		'''
		val multiplymatrices = new CodeSnippet(22071, 3636, 994, 12, 1431, multiplymatricesCode, "multiplymatrices", 0)
		codeSnippets.add(multiplymatrices)
	}

	static def String toCPP() '''
		#include "codesnippets.h"
		«FOR codesnippet : codeSnippets»
		void «codesnippet.name»(){
		«codesnippet.code»
		}
		«ENDFOR»
	'''
	
	static def String toH() '''
		#include<string.h>
		#include<stdlib.h>
		«FOR codesnippet : codeSnippets»
			void «codesnippet.name»();
		«ENDFOR»
	'''
	
}
