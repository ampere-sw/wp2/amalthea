/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.customization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.ConditionDisjunction;
import org.eclipse.app4mc.amalthea.model.ConditionDisjunctionEntry;
import org.eclipse.app4mc.amalthea.model.ILocalModeValueSource;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.LocalModeCondition;
import org.eclipse.app4mc.amalthea.model.LocalModeLabel;
import org.eclipse.app4mc.amalthea.model.ModeLiteral;
import org.eclipse.app4mc.amalthea.model.ModeLiteralConst;
import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.Switch;
import org.eclipse.app4mc.amalthea.model.SwitchEntry;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.slg.commons.m2t.CustomObjectsStore;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.commons.m2t.transformers.sw.ActivityGraphItemTransformer;
import org.eclipse.app4mc.slg.commons.m2t.transformers.sw.RunnableTransformer;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class CustomRunnableTransformer extends RunnableTransformer {

	@Inject private ActivityGraphItemTransformer activityGraphItemTransformer;
	@Inject private CustomObjectsStore customObjsStore;
	@Inject private Properties properties;

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	@Override
	public SLGTranslationUnit transform(final Runnable runnable) {
		final List<Object> key = List.of(runnable);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(runnable);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, runnable);
		}

		return tu;
	}

	// ---------------------------------------------------

	@Override
	protected SLGTranslationUnit createTranslationUnit(final Runnable runnable) {
		if ((runnable == null)) {
			return new SLGTranslationUnit("UNSPECIFIED RUNNABLE");
		} else {
			String basePath = "synthetic_gen";
			String moduleName = "runnables";
			String call = "run_" + runnable.getName();

			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	@Override
	protected void doTransform(final SLGTranslationUnit tu, final Runnable runnable) {
		genFiles(tu, runnable);
	}

	@Override
	protected void genFiles(final SLGTranslationUnit tu, final Runnable runnable) {
		boolean extOverwriteAtActivityGraph = false; // enabling codehook overwriting
		if (isSrcFileEmpty(tu)) {
			srcAppend(tu, "#include \"" + getIncFile(tu) + "\"\n");
			srcAppend(tu, "#include \"ticksUtils.h\"\n");
		}

		customObjsStore.indexData(tu.getCall(), runnable);

		final HashSet<String> includes = new LinkedHashSet<>();
		final List<String> calls = new ArrayList<>();
		final List<String> callsOverwrite = new ArrayList<>(); // overwrite codehook fct

		final Map<SwitchEntry, List<String>> switchBasedCalls = new LinkedHashMap<>();

		final Map<SwitchEntry, List<String>> switchBasedCallsOverwrite = new LinkedHashMap<>(); // overwrite codehook fct

		final List<SwitchEntry> switchEntries = new ArrayList<>();

		final Map<SwitchEntry, Boolean> switchEntryOverwriteActivityGraph = new LinkedHashMap<>();

		final List<Process> processedTasks = new ArrayList<>();

		final List<ActivityGraphItem> defaultActivityGraphItems = new ArrayList<>();

		if (runnable != null && runnable.getActivityGraph() != null) {

			EList<ActivityGraphItem> activityGraphItems = runnable.getActivityGraph().getItems();

			for (ActivityGraphItem activityGraphItem : activityGraphItems) {

				if (activityGraphItem instanceof Switch) {

					for (SwitchEntry entry : ((Switch) activityGraphItem).getEntries()) {
						switchBasedCalls.put(entry, new ArrayList<>());
						switchBasedCallsOverwrite.put(entry, new ArrayList<>());
						switchEntries.add(entry);
					}
				} else {
					defaultActivityGraphItems.add(activityGraphItem);
				}

			}

			extOverwriteAtActivityGraph = extracted(tu, new BasicEList<>(defaultActivityGraphItems),
					extOverwriteAtActivityGraph, includes, calls, callsOverwrite, processedTasks);

			for (SwitchEntry switchEntry : switchEntries) {

				Boolean extOverwriteActivityGraph_switch = extracted(tu, switchEntry.getItems(),
						extOverwriteAtActivityGraph, includes, switchBasedCalls.get(switchEntry),
						switchBasedCallsOverwrite.get(switchEntry), processedTasks);

				switchEntryOverwriteActivityGraph.put(switchEntry, extOverwriteActivityGraph_switch);
			}
		}

		String runnableName = runnable.getName();

		// write header
		customToH(tu, runnableName, includes, switchEntries);
		this.toH(tu, runnableName, includes);
		// write body
		srcAppend(tu, "\n// Runnable " + runnableName + " ----\n");

		// ------------------------ write body with overwrite codehook function

		if (extOverwriteAtActivityGraph) {
			String call_overwrite = "run_" + runnable.getName();

			srcAppend(tu, "void " + call_overwrite + "(char* coreName) {\n");

			for (String call : callsOverwrite) {
				srcAppend(tu, "\n\t" + call + ";");
			}
			srcAppend(tu, "\n" + "}" + "\n");

			if (!runnable.getLocalLabels().isEmpty()) {

				// additional method for switch
				srcAppend(tu, "void " + call_overwrite + "_Context" + "(char* coreName,"
						+ getParamNames(runnable.getLocalLabels()) + ") {\n" + "\n");

				for (SwitchEntry switchEntry : switchEntries) {

					if (switchEntryOverwriteActivityGraph.get(switchEntry)) {

						srcAppend(tu, getConditionString(switchEntry) + "\n");

						for (String call : switchBasedCallsOverwrite.get(switchEntry)) {
							srcAppend(tu, call + ";" + "\n");
						}

					} else {
						for (String call : switchBasedCalls.get(switchEntry)) {
							srcAppend(tu, call + ";" + "\n");
						}
					}
				}
				srcAppend(tu, "\n" + "}" + "\n");
			}
		}

		// ------------------------

		else {
			toCustomCpp(tu, calls, switchEntries, switchBasedCalls, runnable);	// write body without overwriting the codehook function
		}
	}

	private String getConditionString(SwitchEntry switchEntry) {

		ConditionDisjunction condition = switchEntry.getCondition();

		if (condition != null) {

			EList<ConditionDisjunctionEntry> entries = condition.getEntries();

			for (ConditionDisjunctionEntry conditionDisjunctionEntry : entries) {

				if (conditionDisjunctionEntry instanceof LocalModeCondition) {
					LocalModeLabel localLabel = ((LocalModeCondition) conditionDisjunctionEntry).getLabel();

					if (localLabel != null) {

						String localVariableName = localLabel.getName();

						ILocalModeValueSource valueSource = ((LocalModeCondition) conditionDisjunctionEntry).getValueSource();

						if (valueSource instanceof ModeLiteralConst) {
							ModeLiteral value = ((ModeLiteralConst) valueSource).getValue();

							String caseName = value.getName();

							return "strcmp(" + localVariableName + ",\"" + caseName + "\") == 0";
						}
					}
				}
			}
		}

		return "false";
	}

	private String getRawConditionString(SwitchEntry switchEntry) {

		ConditionDisjunction condition = switchEntry.getCondition();

		if (condition != null) {

			EList<ConditionDisjunctionEntry> entries = condition.getEntries();

			for (ConditionDisjunctionEntry conditionDisjunctionEntry : entries) {

				if (conditionDisjunctionEntry instanceof LocalModeCondition) {
					LocalModeLabel localLabel = ((LocalModeCondition) conditionDisjunctionEntry).getLabel();

					if (localLabel != null) {

						String localVariableName = localLabel.getName();

						ILocalModeValueSource valueSource = ((LocalModeCondition) conditionDisjunctionEntry)
								.getValueSource();

						if (valueSource instanceof ModeLiteralConst) {
							ModeLiteral value = ((ModeLiteralConst) valueSource).getValue();

							String caseName = value.getName();
							return caseName;
						}
					}
				}
			}
		}

		return "false";
	}
	
	private String getParamNames(EList<LocalModeLabel> localLabels) {

		List<String> ls = new ArrayList<>();

		for (LocalModeLabel localModeLabel : localLabels) {
			ls.add("char* " + localModeLabel.getName());
		}

		return String.join(", ", ls);
	}

	private boolean extracted(final SLGTranslationUnit tu, final EList<ActivityGraphItem> activityGraphItems,
			boolean extOverwrite, final HashSet<String> includes, final List<String> calls,
			final List<String> callsOverwrite, final List<Process> processedTasks) {

		if (activityGraphItems.isEmpty()) {
			return false;
		}

		extOverwrite = processCustomProperties(extOverwrite, calls, callsOverwrite,
				activityGraphItems.get(0).eContainer());

		// ************ Looping through all the elements inside ActivityGraph object
		for (final ActivityGraphItem item : activityGraphItems) {
			if( item instanceof RunnableCall) {
				RunnableCall call = (RunnableCall) item;
				calls.add("run_"+call.getRunnable().getName() + "(\"\");\n");
			}
			else if (item instanceof Ticks) {
				Ticks ticks = (Ticks) item;
				final Map<String, SLGTranslationUnit> translationUnits = activityGraphItemTransformer
						.transformAllItems(ticks); // Mc: move method to TicksTransformer ?

				SLGTranslationUnit defaultTicksTU = null;
				boolean ticksAssociatedToPUs = false;

				for (final Entry<String, SLGTranslationUnit> entry : translationUnits.entrySet()) {
					String puName = entry.getKey();
					SLGTranslationUnit tmpTU = entry.getValue();

					if (puName.equals("default")) {
						defaultTicksTU = tmpTU;
					} else {
						final String tmpIncFile = getIncFile(tmpTU);
						if (tmpIncFile != null && !tmpIncFile.isEmpty() && !getIncFile(tu).equals(tmpIncFile)) {
							includes.add(tmpIncFile);
						}
						final String call = tmpTU.getCall();
						if (call != null && !call.isEmpty()) {
							calls.add(ticksAssociatedToPUs
									? "else if(strcmp(coreName,\"" + puName + "\") == 0) {"
									: " if(strcmp(coreName,\"" + puName + "\") == 0) {");
							calls.add("\t" + call);
							calls.add("}");
							ticksAssociatedToPUs = true;
						}
					}
				}

				if ((defaultTicksTU != null)) {
					if (ticksAssociatedToPUs) {
						calls.add("else ");
						calls.add("{");
					}

					if (defaultTicksTU.getCall() != null && !defaultTicksTU.getCall().isEmpty()) {
						calls.add(defaultTicksTU.getCall());
					}

					if (ticksAssociatedToPUs) {
						calls.add("}");
					}

				}
			} else if (item instanceof ActivityGraphItem) {
				if ((item instanceof InterProcessTrigger)) {
					InterProcessTrigger trigger = (InterProcessTrigger) item;
					// final ConfigModel configModel =
					// customObjsStore.<ConfigModel>getInstance(ConfigModel.class);
					final String value = CustomRunnableGenerator.handleInterProcessTrigger(trigger.getStimulus(),
							processedTasks);
					if (value != null && !value.trim().isEmpty()) {
						calls.add(value);
					}
				} else {
					final SLGTranslationUnit tmpTU = activityGraphItemTransformer.transform((ActivityGraphItem) item);

					final String tmpIncFile = getIncFile(tmpTU);
					if (tmpIncFile != null && !tmpIncFile.isEmpty() && !getIncFile(tu).equals(tmpIncFile)) {
						includes.add(tmpIncFile);
					}
					final String call = tmpTU.getCall();
					if (call != null && !call.isEmpty()) {
						calls.add(call);
					}
				}
			}
		}
		return extOverwrite;
	}

	@Override
	protected void toCpp(final SLGTranslationUnit tu, final List<String> calls) {

		srcAppend(tu, "void " + tu.getCall() + "(char* coreName){\n");

		for (String call : calls) {
			srcAppend(tu, "\t" + call + (call.endsWith(")") ? ";" : "") + "\n");
		}

		srcAppend(tu, "}\n\n");

	}

	protected void toCustomCpp(final SLGTranslationUnit tu, final List<String> calls, List<SwitchEntry> switchEntries,
			Map<SwitchEntry, List<String>> switchBasedCalls, Runnable runnable) {
		if (!runnable.getLocalLabels().isEmpty()) {
			// additional method for switch
			for (SwitchEntry switchEntry : switchEntries) {
				String label = getRawConditionString(switchEntry);

				if (label.equals("ARM") || label.equals("host_seq") || label.equals("CPU"))
					srcAppend(tu, "void " + tu.getCall() + "(char* coreName){\n" + "\n");
				else
					srcAppend(tu, "void " + tu.getCall() + "_" + label + "(char* coreName){\n" + "\n");
				{
					for (String call : switchBasedCalls.get(switchEntry)) {
						srcAppend(tu, call + ";" + "\n");
					}
					srcAppend(tu, "\n" + "}" + "\n");
				}
			}
		} else {
			toCpp(tu, calls);
		}
	}

	protected void customToH(SLGTranslationUnit tu, String runnableName, HashSet<String> includes,
			List<SwitchEntry> switchEntries) {
		if (isIncFileEmpty(tu)) {
			incAppend(tu, "#include <pthread.h> \n");
			incAppend(tu, "#include \"tasks.h\" \n");
			incAppend(tu, "#include \"labels.h\" \n");
			incAppend(tu, "#include <string.h> \n\n");
		}

		Object data = customObjsStore.getData(tu.getCall());
		EList<LocalModeLabel> localLabels = ((Runnable) data).getLocalLabels();
		if (!localLabels.isEmpty()) {
			String arch, construct, type;
			arch = construct = type = "";
			boolean validName = true;
			String VariantDecl = "";

			for (SwitchEntry switchEntry : switchEntries) {
				String label = getRawConditionString(switchEntry);
				switch (label) {
				case "ARM":
				case "host_seq":
				case "CPU":
					continue;
				case "OpenMP":
				case "host_omp":
					arch = "cpu";
					construct = "parallel";
					type = "host_omp";
					break;
				case "DeviceOpenMP":
					arch = "gpu";
					construct = "target";
					type = "device_omp";
					break;
				case "FPGA":
				case "GPU":
					arch = "gpu";
					construct = "target";
					type = "device_cuda";
					break;
				default:
					validName = false;
					// System.out.println("Variant " + label +" not recognized \n");
				}
				if (validName) {
					incAppend(tu, "void " + tu.getCall() + "_" + label + "(char* coreName);\n");
					VariantDecl += "#pragma omp declare variant(" + tu.getCall() + "_" + label + ") match(construct={"
							+ construct + "}) \n";
				}
			}
			incAppend(tu, VariantDecl);
		}
	}
	@Override
	protected void toH(SLGTranslationUnit tu, String runnableName, HashSet<String> includes) {
		super.toH(tu, runnableName, includes);
		/*
		Object data = customObjsStore.getData(tu.getCall());
		
		EList<LocalModeLabel> localLabels = ((Runnable) data).getLocalLabels();
		if (!localLabels.isEmpty()) {
			incAppend(tu, "void " + tu.getCall() + "_Context" + "(char* coreName" + " , " + getParamNames(localLabels) + " );\n");
		}
		*/
	}
}
