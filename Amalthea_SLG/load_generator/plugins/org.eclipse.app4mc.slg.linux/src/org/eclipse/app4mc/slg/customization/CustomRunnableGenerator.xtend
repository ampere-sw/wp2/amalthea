/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.customization

import java.util.List
import org.eclipse.app4mc.amalthea.model.Process
import org.eclipse.app4mc.amalthea.model.Stimulus
import org.eclipse.app4mc.amalthea.model.Task

class CustomRunnableGenerator {
	
	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def handleInterProcessTrigger(Stimulus stimulus, List<Process> processedTasks) {	
		
		val StringBuilder builder = new StringBuilder

		if (stimulus !== null) {
			val processes = stimulus.affectedProcesses

			for (process : processes) {

				if (process instanceof Task) {
					var codeSnippet = '''

						//interprocess trigger
							pthread_t «process.name»_;
							«{processedTasks.add(process);""}»
							
							pthread_create(&«process.name»_, NULL, *«process.name»_entry, NULL);
							//pthread_join(«process.name»_, NULL);
							
					''';

					builder.append(codeSnippet.toString)
				}

			}
		}
		return builder.toString
	}

}
