/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.linux.generators

import org.eclipse.app4mc.slg.config.ConfigModel
import org.eclipse.app4mc.slg.config.util.ConfigModelUtils
import org.eclipse.app4mc.slg.config.CodehookType
import java.util.ArrayList

class LinuxMakeGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

private static def String getHeaderFilesDirectory(ConfigModel configModel,String workingDirectory){
	
	val StringBuffer buffer=new StringBuffer
	
	val runnableHeaders=ConfigModelUtils.getHeaderFilesDirectories(configModel,CodehookType.RUNNABLE,workingDirectory,null);
	val tasksHeader=ConfigModelUtils.getHeaderFilesDirectories(configModel,CodehookType.TASK,workingDirectory,null);
	
	val allHeaderDirs=new ArrayList
	
	allHeaderDirs.addAll(runnableHeaders)
	allHeaderDirs.addAll(tasksHeader)
	
	for(hfileDir : allHeaderDirs){
			buffer.append("-I"+hfileDir+ " ")
	}
	return buffer.toString.trim;
}
	static def String getContent(boolean experimentalCodeMatching, boolean instrumentation, boolean instrumentation_R, boolean externalCode, boolean hasOpenMP, ConfigModel configModel,String workingDirectory) '''
		synthetic: main.o tasks.o runnables.o «IF experimentalCodeMatching»codesnippets.o«ENDIF» «IF instrumentation»instrument.o «ELSEIF instrumentation_R»instrument.o«ENDIF» «IF !experimentalCodeMatching» labels.o ticksUtils.o«ENDIF»
			clang -g«IF hasOpenMP» -fopenmp«ENDIF» -o synthetic main.o tasks.o runnables.o«IF hasOpenMP» tasks_tdg.cpp«ENDIF»«IF !experimentalCodeMatching» labels.o ticksUtils.o«ENDIF» «IF experimentalCodeMatching»codesnippets.o«ENDIF» «IF instrumentation_R»instrument.o «ELSEIF instrumentation»instrument.o«ENDIF» -pthread «IF externalCode»«FOR LibPath : ConfigModelUtils.getLinkedLibraryInstructions(configModel,workingDirectory)» «LibPath» «ENDFOR»«ENDIF»
		
		main.o: Executable/main/_src/main.c
			clang -g -c «IF !experimentalCodeMatching»-Isynthetic_gen/labels/_inc -Isynthetic_gen/ticksUtils/_inc«ENDIF»  -Isynthetic_gen/tasks/_inc -Isynthetic_gen/runnables/_inc -Isynthetic_gen «IF externalCode»«getHeaderFilesDirectory(configModel,workingDirectory)»«ENDIF» «IF experimentalCodeMatching»-Isynthetic_gen/codesnippets/_inc «ENDIF»Executable/main/_src/main.c
		
		tasks.o: synthetic_gen/tasks/_src/tasks.c
			clang -g«IF hasOpenMP» -fopenmp-taskgraph -fopenmp -static-tdg«ENDIF» -c -Isynthetic_gen «IF !experimentalCodeMatching»-Isynthetic_gen/labels/_inc -Isynthetic_gen/ticksUtils/_inc«ENDIF»  -Isynthetic_gen/tasks/_inc -Isynthetic_gen/runnables/_inc «IF externalCode»«getHeaderFilesDirectory(configModel,workingDirectory)»«ENDIF» «IF experimentalCodeMatching»-Isynthetic_gen/codesnippets/_inc «ENDIF»synthetic_gen/tasks/_src/tasks.c
			
		runnables.o: synthetic_gen/runnables/_src/runnables.c
			clang -g«IF hasOpenMP» -fopenmp«ENDIF» -c «IF !experimentalCodeMatching»-Isynthetic_gen/labels/_inc -Isynthetic_gen/tasks/_inc -Isynthetic_gen/ticksUtils/_inc«ENDIF»  -Isynthetic_gen/runnables/_inc «IF externalCode»«getHeaderFilesDirectory(configModel,workingDirectory)»«ENDIF» «IF experimentalCodeMatching»-Isynthetic_gen/codesnippets/_inc «ENDIF»synthetic_gen/runnables/_src/runnables.c -O2
		
		
		«IF !experimentalCodeMatching»
			labels.o: synthetic_gen/labels/_src/labels.c
				clang -c -Isynthetic_gen/labels/_inc  synthetic_gen/labels/_src/labels.c -O2
			
			ticksUtils.o: synthetic_gen/ticksUtils/_src/ticksUtils.c
				clang -c -Isynthetic_gen/ticksUtils/_inc synthetic_gen/ticksUtils/_src/ticksUtils.c -O2
			
		«ELSE»
			codesnippets.o: synthetic_gen/codesnippets/_src/codesnippets.c
				clang -c -Isynthetic_gen/codesnippets/_inc synthetic_gen/codesnippets/_src/codesnippets.c
		«ENDIF»
		
		«IF instrumentation || instrumentation_R »	
		 
		instrument.o: synthetic_gen/instrument.c
				gcc -c -Isynthetic_gen synthetic_gen/instrument.c
		«ENDIF»
		
	'''

}
