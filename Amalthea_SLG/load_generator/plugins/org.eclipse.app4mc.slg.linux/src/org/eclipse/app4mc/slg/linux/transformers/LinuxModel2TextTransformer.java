/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.slg.linux.transformers;

import static org.eclipse.app4mc.slg.linux.transformers.LinuxBaseSettings.INC_TYPE;
import static org.eclipse.app4mc.slg.linux.transformers.LinuxBaseSettings.SRC_TYPE;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.StimuliModel;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.slg.commons.m2t.CustomObjectsStore;
import org.eclipse.app4mc.slg.commons.m2t.transformers.AmaltheaModel2TextTransformer;
import org.eclipse.app4mc.slg.commons.m2t.transformers.sw.InstrumentationTransformer;
import org.eclipse.app4mc.slg.commons.m2t.transformers.sw.RunnableTransformer;
import org.eclipse.app4mc.slg.linux.transformers.stimuli.LinuxStimulusTransformer;
import org.eclipse.app4mc.slg.linux.transformers.sw.LinuxMakeTransformer;
import org.eclipse.app4mc.slg.linux.transformers.sw.LinuxRealisticCodeTransformer;
import org.eclipse.app4mc.slg.linux.transformers.sw.LinuxRunnableTransformer;
import org.eclipse.app4mc.slg.linux.transformers.sw.LinuxTaskTransformer;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;

import com.google.inject.Inject;


public class LinuxModel2TextTransformer extends AmaltheaModel2TextTransformer {

	@Inject private SessionLogger logger;
	@Inject private OutputBuffer outputBuffer;
	@Inject private LinuxMakeTransformer linuxMakeTransformer;
	@Inject private LinuxRealisticCodeTransformer realisticCodeBasedOnCodeSnippet;
	@Inject private LinuxStimulusTransformer stimuliTransformer;
	@Inject private LinuxRunnableTransformer linuxRunnableTransformer;
	@Inject private RunnableTransformer defaultRunnableTransformer;
	@Inject private LinuxTaskTransformer taskTransformer;
	@Inject private InstrumentationTransformer instrumentationTransformer;
	@Inject private CustomObjectsStore customObjsStore;

    @Override
    public void transform(final Amalthea model, final String outputFolder) {
        final boolean experimentalCodeSnippetMatching = Boolean.parseBoolean(getProperty("experimentalCodeSnippetMatching"));
        final boolean enableInstrumentation = Boolean.parseBoolean(getProperty("enableInstrumentation_Tasks"));
        final boolean enableInstrumentation_R = Boolean.parseBoolean(getProperty("enableInstrumentation_Runnables"));

        customObjsStore.indexData(TransformationConstants.WORKING_DIRECTORY,
            getProperty(TransformationConstants.WORKING_DIRECTORY));

        final StimuliModel stimuliModel = ModelUtil.getOrCreateStimuliModel(model);
        final SWModel swModel = ModelUtil.getOrCreateSwModel(model);

        logger.info("Starting Linux SLG code generation");

        LinuxBaseSettings.initializeOutputBuffer(outputBuffer, outputFolder);

        if (experimentalCodeSnippetMatching) {
            realisticCodeBasedOnCodeSnippet.transform();
        }

        this.stimuliTransformer.transform(stimuliModel.getStimuli());

        for (Runnable runnable : swModel.getRunnables()) {
            if (experimentalCodeSnippetMatching) {
                linuxRunnableTransformer.transform(runnable);
            } else {
                defaultRunnableTransformer.transform(runnable);
            }
        }

        for (Task task : swModel.getTasks()) {
            taskTransformer.transform(task);
        }

        if (enableInstrumentation || enableInstrumentation_R) {
            instrumentationTransformer.transform("synthetic_gen/instrument");
        }

        linuxMakeTransformer.transform();

        // ensure that the following files will be created
        if (experimentalCodeSnippetMatching) {
            outputBuffer.appendTo(SRC_TYPE, "synthetic_gen/labels/_src/labels",
                "");
            outputBuffer.appendTo(INC_TYPE, "synthetic_gen/labels/_inc/labels",
                "");
            outputBuffer.appendTo(SRC_TYPE,
                "synthetic_gen/codesnippets/_src/codesnippets", "");
            outputBuffer.appendTo(INC_TYPE,
                "synthetic_gen/codesnippets/_inc/codesnippets", "");
        }

        outputBuffer.appendTo(SRC_TYPE,
            "synthetic_gen/ticksUtils/_src/ticksUtils", "");
        outputBuffer.appendTo(INC_TYPE,
            "synthetic_gen/ticksUtils/_inc/ticksUtils", "");
        outputBuffer.appendTo(SRC_TYPE,
            "synthetic_gen/runnables/_src/runnables", "");
        outputBuffer.appendTo(INC_TYPE,
            "synthetic_gen/runnables/_inc/runnables", "");

        outputBuffer.finish();

        logger.info("Finished Linux SLG code generation");
    }

}
