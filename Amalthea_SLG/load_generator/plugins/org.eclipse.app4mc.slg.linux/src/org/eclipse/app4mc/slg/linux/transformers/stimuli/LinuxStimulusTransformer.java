/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.linux.transformers.stimuli;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.linux.generators.LinuxStimulusGenerator;
import org.eclipse.app4mc.slg.linux.transformers.LinuxBaseTransformer;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class LinuxStimulusTransformer extends LinuxBaseTransformer {

	@Inject private Properties properties;

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final List<Stimulus> stimuli) {
		final List<Object> key = List.of(stimuli);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(stimuli);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, stimuli);
		}

		return tu;
	}

	// ---------------------------------------------------

	private SLGTranslationUnit createTranslationUnit(final List<Stimulus> stimuli) {
		if ((stimuli == null || stimuli.isEmpty())) {
			return new SLGTranslationUnit("UNSPECIFIED STIMULI");
		} else {
			String basePath = "Executable";
			String moduleName = "main";
			String call = "trigger<Stimuli>"; // unused
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	private void doTransform(final SLGTranslationUnit tu, final List<Stimulus> stimuli) {
		List<Stimulus> filteredStimuli = new ArrayList<>();

		for (Stimulus stimulus : stimuli) {
			if ((stimulus instanceof PeriodicStimulus)) {
				filteredStimuli.add(stimulus);
			}
		}

		genFiles(tu, filteredStimuli);
	}

	private void genFiles(SLGTranslationUnit tu, final List<Stimulus> stimuli) {
		Stimulus lastStimulus = stimuli.get(stimuli.size() - 1);
		boolean enableInstrumentation = Boolean.parseBoolean(properties.getProperty("enableInstrumentation_Tasks"));

		srcAppend(tu, LinuxStimulusGenerator.toSrc(stimuli, lastStimulus, enableInstrumentation));
	}

}
