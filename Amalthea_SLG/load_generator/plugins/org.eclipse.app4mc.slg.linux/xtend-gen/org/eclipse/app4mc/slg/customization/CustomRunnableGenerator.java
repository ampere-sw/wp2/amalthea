/**
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.customization;

import java.util.List;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class CustomRunnableGenerator {
  private CustomRunnableGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String handleInterProcessTrigger(final Stimulus stimulus, final List<org.eclipse.app4mc.amalthea.model.Process> processedTasks) {
    final StringBuilder builder = new StringBuilder();
    if ((stimulus != null)) {
      final EList<org.eclipse.app4mc.amalthea.model.Process> processes = stimulus.getAffectedProcesses();
      for (final org.eclipse.app4mc.amalthea.model.Process process : processes) {
        if ((process instanceof Task)) {
          StringConcatenation _builder = new StringConcatenation();
          _builder.newLine();
          _builder.append("//interprocess trigger");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("pthread_t ");
          String _name = ((Task)process).getName();
          _builder.append(_name, "\t");
          _builder.append("_;");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          String _xblockexpression = null;
          {
            processedTasks.add(process);
            _xblockexpression = "";
          }
          _builder.append(_xblockexpression, "\t");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("pthread_create(&");
          String _name_1 = ((Task)process).getName();
          _builder.append(_name_1, "\t");
          _builder.append("_, NULL, *");
          String _name_2 = ((Task)process).getName();
          _builder.append(_name_2, "\t");
          _builder.append("_entry, NULL);");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("//pthread_join(");
          String _name_3 = ((Task)process).getName();
          _builder.append(_name_3, "\t");
          _builder.append("_, NULL);");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.newLine();
          String codeSnippet = _builder.toString();
          builder.append(codeSnippet.toString());
        }
      }
    }
    return builder.toString();
  }
}
