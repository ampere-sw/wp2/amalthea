/**
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.linux.generators;

import com.google.common.base.Objects;
import java.util.List;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class LinuxStimulusGenerator {
  private LinuxStimulusGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String toSrc(final List<Stimulus> stimuli, final Stimulus lastStimulus, final boolean enableInstrumentation) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"tasks.h\"");
    _builder.newLine();
    _builder.append("#include <pthread.h>");
    _builder.newLine();
    _builder.append("#include <unistd.h>");
    _builder.newLine();
    {
      if (enableInstrumentation) {
        _builder.append("#include \"instrument.h\"");
        _builder.newLine();
        _builder.newLine();
        _builder.append("int counter=0;");
        _builder.newLine();
      }
    }
    {
      for(final Stimulus stimulus : stimuli) {
        {
          if ((stimulus instanceof PeriodicStimulus)) {
            _builder.append("void *");
            String _name = ((PeriodicStimulus)stimulus).getName();
            _builder.append(_name);
            _builder.append("Entry(){");
            _builder.newLineIfNotEmpty();
            {
              EList<org.eclipse.app4mc.amalthea.model.Process> _affectedProcesses = ((PeriodicStimulus)stimulus).getAffectedProcesses();
              for(final org.eclipse.app4mc.amalthea.model.Process task : _affectedProcesses) {
                {
                  if (enableInstrumentation) {
                    _builder.append("instr_start();");
                  }
                }
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                String _name_1 = task.getName();
                _builder.append(_name_1, "\t");
                _builder.append("();");
                _builder.newLineIfNotEmpty();
                {
                  if (enableInstrumentation) {
                    _builder.append("instr_stop();");
                  }
                }
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.append("}");
            _builder.newLine();
          }
        }
      }
    }
    {
      for(final Stimulus stimulus_1 : stimuli) {
        {
          if ((stimulus_1 instanceof PeriodicStimulus)) {
            _builder.append("void *");
            String _name_2 = ((PeriodicStimulus)stimulus_1).getName();
            _builder.append(_name_2);
            _builder.append("Loop(){");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("pthread_t ");
            String _name_3 = ((PeriodicStimulus)stimulus_1).getName();
            _builder.append(_name_3, "\t");
            _builder.append(";");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("for(;;){");
            _builder.newLine();
            _builder.append("\t\t");
            _builder.append("pthread_create(&");
            String _name_4 = ((PeriodicStimulus)stimulus_1).getName();
            _builder.append(_name_4, "\t\t");
            _builder.append(", NULL, ");
            String _name_5 = ((PeriodicStimulus)stimulus_1).getName();
            _builder.append(_name_5, "\t\t");
            _builder.append("Entry, NULL);");
            _builder.newLineIfNotEmpty();
            _builder.append("\t\t");
            _builder.newLine();
            {
              Time _recurrence = ((PeriodicStimulus)stimulus_1).getRecurrence();
              boolean _tripleNotEquals = (_recurrence != null);
              if (_tripleNotEquals) {
                {
                  String _get = ((PeriodicStimulus)stimulus_1).getRecurrence().toString().split(" ")[1];
                  boolean _equals = Objects.equal(_get, "us");
                  if (_equals) {
                    _builder.append("\t\t");
                    _builder.append("usleep(");
                    String _get_1 = ((PeriodicStimulus)stimulus_1).getRecurrence().toString().split(" ")[0];
                    _builder.append(_get_1, "\t\t");
                    _builder.append(");");
                    _builder.newLineIfNotEmpty();
                  }
                }
                {
                  String _get_2 = ((PeriodicStimulus)stimulus_1).getRecurrence().toString().split(" ")[1];
                  boolean _equals_1 = Objects.equal(_get_2, "ms");
                  if (_equals_1) {
                    _builder.append("\t\t");
                    _builder.append("usleep(");
                    String _get_3 = ((PeriodicStimulus)stimulus_1).getRecurrence().toString().split(" ")[0];
                    _builder.append(_get_3, "\t\t");
                    _builder.append("000);");
                    _builder.newLineIfNotEmpty();
                  }
                }
              }
            }
            _builder.append("\t\t\t");
            _builder.append("}");
            _builder.newLine();
            _builder.append("\t\t");
            _builder.append("}");
            _builder.newLine();
          }
        }
      }
    }
    _builder.append("\t");
    _builder.append("int main(int argc, char **argv){");
    _builder.newLine();
    {
      for(final Stimulus stimulus_2 : stimuli) {
        {
          if ((stimulus_2 instanceof PeriodicStimulus)) {
            _builder.append("\t\t");
            _builder.append("pthread_t ");
            String _name_6 = ((PeriodicStimulus)stimulus_2).getName();
            _builder.append(_name_6, "\t\t");
            _builder.append("_;");
            _builder.newLineIfNotEmpty();
            _builder.append("\t\t");
            _builder.append("pthread_create(&");
            String _name_7 = ((PeriodicStimulus)stimulus_2).getName();
            _builder.append(_name_7, "\t\t");
            _builder.append("_, NULL, ");
            String _name_8 = ((PeriodicStimulus)stimulus_2).getName();
            _builder.append(_name_8, "\t\t");
            _builder.append("Loop, NULL);");
            _builder.newLineIfNotEmpty();
            _builder.append("\t\t");
            _builder.append("\t");
            _builder.newLine();
          }
        }
      }
    }
    _builder.append("\t\t");
    {
      if ((lastStimulus != null)) {
        _builder.append("pthread_join(");
        String _name_9 = lastStimulus.getName();
        _builder.append(_name_9, "\t\t");
        _builder.append("_, NULL);");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
}
