/**
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.linux.generators;

import org.eclipse.app4mc.amalthea.model.DataSize;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.slg.commons.m2t.AmaltheaModelUtils;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class LinuxLabelGenerator {
  private LinuxLabelGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String initCall(final Label label) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("initialize_");
    String _name = label.getName();
    _builder.append(_name);
    _builder.append("()");
    return _builder.toString();
  }
  
  public static String readCall(final Label label, final String param) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("read_");
    String _name = label.getName();
    _builder.append(_name);
    _builder.append("(");
    _builder.append(param);
    _builder.append(")");
    return _builder.toString();
  }
  
  public static String writeCall(final Label label, final String param) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("write_");
    String _name = label.getName();
    _builder.append(_name);
    _builder.append("(");
    _builder.append(param);
    _builder.append(")");
    return _builder.toString();
  }
  
  public static String toH(final Label label) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void ");
    String _initCall = LinuxLabelGenerator.initCall(label);
    _builder.append(_initCall);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("void ");
    String _readCall = LinuxLabelGenerator.readCall(label, "int labelAccessStatistics");
    _builder.append(_readCall);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("void ");
    String _writeCall = LinuxLabelGenerator.writeCall(label, "int labelAccessStatistics");
    _builder.append(_writeCall);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toCpp(final Label label) {
    String _xblockexpression = null;
    {
      String _xifexpression = null;
      String _name = null;
      if (label!=null) {
        _name=label.getName();
      }
      boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(_name);
      if (_isNullOrEmpty) {
        _xifexpression = "<undefined label>";
      } else {
        _xifexpression = label.getName();
      }
      final String name = _xifexpression;
      long _xifexpression_1 = (long) 0;
      DataSize _size = null;
      if (label!=null) {
        _size=label.getSize();
      }
      boolean _tripleEquals = (_size == null);
      if (_tripleEquals) {
        _xifexpression_1 = 0;
      } else {
        _xifexpression_1 = label.getSize().getNumberBytes();
      }
      final long numberOfBytes = _xifexpression_1;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("int ");
      _builder.append(name);
      _builder.append("[");
      long _labelArraySize = AmaltheaModelUtils.getLabelArraySize(label);
      _builder.append(_labelArraySize);
      _builder.append("];");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("static bool isIinitialized_");
      _builder.append(name);
      _builder.append(" = false;");
      _builder.newLineIfNotEmpty();
      _builder.append("void ");
      String _initCall = LinuxLabelGenerator.initCall(label);
      _builder.append(_initCall);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("if (!isIinitialized_");
      _builder.append(name, "\t");
      _builder.append("){");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.append("int i;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("for (i=0; i < ");
      long _labelArraySize_1 = AmaltheaModelUtils.getLabelArraySize(label);
      _builder.append(_labelArraySize_1, "\t\t");
      _builder.append("; i++){");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append(name, "\t\t\t");
      _builder.append("[i] = i+1;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("isIinitialized_");
      _builder.append(name, "\t\t");
      _builder.append(" = true;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("void ");
      String _readCall = LinuxLabelGenerator.readCall(label, "int labelAccessStatistics");
      _builder.append(_readCall);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("int numberOfBytes = ");
      _builder.append(numberOfBytes, "\t");
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("int repeat;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("if(numberOfBytes < 4){");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("numberOfBytes = 4;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("int arraysize = sizeof(");
      _builder.append(name, "\t\t");
      _builder.append(") / 4;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("//printf(\"number of bytes:»d\\n\",arraysize);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("int leftOverElements=arraysize»10;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("int arraySizeWith10Multiples=arraysize-leftOverElements;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("int i = 0;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("int a = 0;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("a = ");
      _builder.append(name, "\t\t\t");
      _builder.append("[i];");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append("a = ");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+1];");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append("a = ");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+2];");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append("a = ");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+3];");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append("a = ");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+4];");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append("a = ");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+5];");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append("a = ");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+6];");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append("a = ");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+7];");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append("a = ");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+8];");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append("a = ");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+9];");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("for(;i<arraysize;i++){");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("a = ");
      _builder.append(name, "\t\t\t");
      _builder.append("[i];");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("void ");
      String _writeCall = LinuxLabelGenerator.writeCall(label, "int labelAccessStatistics");
      _builder.append(_writeCall);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("int numberOfBytes = ");
      _builder.append(numberOfBytes, "\t");
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("int repeat;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("if(numberOfBytes < 4){");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("numberOfBytes = 4;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("int arraysize = sizeof(");
      _builder.append(name, "\t\t");
      _builder.append(") / 4;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.append("int leftOverElements=arraysize»10;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("int arraySizeWith10Multiples=arraysize-leftOverElements;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("int i = 0;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append(name, "\t\t\t");
      _builder.append("[i]   = 0xAFFE;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+1] = 0xAFFE;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+2] = 0xAFFE;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+3] = 0xAFFE;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+4] = 0xAFFE;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+5] = 0xAFFE;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+6] = 0xAFFE;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+7] = 0xAFFE;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+8] = 0xAFFE;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append(name, "\t\t\t");
      _builder.append("[i+9] = 0xAFFE;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("for(;i<arraysize;i++){");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append(name, "\t\t\t\t");
      _builder.append("[i]=0xAFFE;");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
}
