/**
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.linux.generators;

import java.util.Arrays;
import org.eclipse.app4mc.amalthea.model.DiscreteValueBetaDistribution;
import org.eclipse.app4mc.amalthea.model.DiscreteValueBoundaries;
import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant;
import org.eclipse.app4mc.amalthea.model.DiscreteValueGaussDistribution;
import org.eclipse.app4mc.amalthea.model.DiscreteValueHistogram;
import org.eclipse.app4mc.amalthea.model.DiscreteValueStatistics;
import org.eclipse.app4mc.amalthea.model.DiscreteValueUniformDistribution;
import org.eclipse.app4mc.amalthea.model.DiscreteValueWeibullEstimatorsDistribution;
import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class LinuxTicksGenerator {
  private LinuxTicksGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  protected static String _getParameters(final DiscreteValueConstant value) {
    StringConcatenation _builder = new StringConcatenation();
    long _value = value.getValue();
    _builder.append(_value);
    return _builder.toString();
  }
  
  protected static String _getParameters(final DiscreteValueStatistics value) {
    StringConcatenation _builder = new StringConcatenation();
    Double _average = value.getAverage();
    _builder.append(_average);
    _builder.append(", ");
    Long _lowerBound = value.getLowerBound();
    _builder.append(_lowerBound);
    _builder.append(", ");
    Long _upperBound = value.getUpperBound();
    _builder.append(_upperBound);
    return _builder.toString();
  }
  
  protected static String _getParameters(final DiscreteValueWeibullEstimatorsDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    Double _average = value.getAverage();
    _builder.append(_average);
    return _builder.toString();
  }
  
  protected static String _getParameters(final IDiscreteValueDeviation value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED\t");
    _builder.newLine();
    return _builder.toString();
  }
  
  protected static String _getParameters(final DiscreteValueBetaDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    _builder.newLine();
    return _builder.toString();
  }
  
  protected static String _getParameters(final DiscreteValueBoundaries value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    _builder.newLine();
    return _builder.toString();
  }
  
  protected static String _getParameters(final DiscreteValueGaussDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    _builder.newLine();
    return _builder.toString();
  }
  
  protected static String _getParameters(final DiscreteValueHistogram value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    _builder.newLine();
    return _builder.toString();
  }
  
  protected static String _getParameters(final DiscreteValueUniformDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String getParameters(final IDiscreteValueDeviation value) {
    if (value instanceof DiscreteValueBetaDistribution) {
      return _getParameters((DiscreteValueBetaDistribution)value);
    } else if (value instanceof DiscreteValueBoundaries) {
      return _getParameters((DiscreteValueBoundaries)value);
    } else if (value instanceof DiscreteValueGaussDistribution) {
      return _getParameters((DiscreteValueGaussDistribution)value);
    } else if (value instanceof DiscreteValueStatistics) {
      return _getParameters((DiscreteValueStatistics)value);
    } else if (value instanceof DiscreteValueUniformDistribution) {
      return _getParameters((DiscreteValueUniformDistribution)value);
    } else if (value instanceof DiscreteValueWeibullEstimatorsDistribution) {
      return _getParameters((DiscreteValueWeibullEstimatorsDistribution)value);
    } else if (value instanceof DiscreteValueConstant) {
      return _getParameters((DiscreteValueConstant)value);
    } else if (value instanceof DiscreteValueHistogram) {
      return _getParameters((DiscreteValueHistogram)value);
    } else if (value != null) {
      return _getParameters(value);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(value).toString());
    }
  }
}
