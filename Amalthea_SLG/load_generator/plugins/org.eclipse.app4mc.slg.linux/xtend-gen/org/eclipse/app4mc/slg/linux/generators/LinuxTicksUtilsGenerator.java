/**
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.linux.generators;

import com.google.common.base.Objects;
import org.eclipse.app4mc.amalthea.model.impl.DiscreteValueConstantImpl;
import org.eclipse.app4mc.amalthea.model.impl.DiscreteValueStatisticsImpl;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.TickType;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.IntegerRange;

@SuppressWarnings("all")
public class LinuxTicksUtilsGenerator {
  private LinuxTicksUtilsGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String getExecCall(final String params) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("burnTicks(");
    _builder.append(params);
    _builder.append(")");
    return _builder.toString();
  }
  
  /**
   * '''executeTicks_«valueClass.toString.split("\\.").last»(«params»)'''
   */
  public static String generateTicks(final Object valueClass) {
    String _switchResult = null;
    boolean _matched = false;
    if (Objects.equal(valueClass, DiscreteValueConstantImpl.class)) {
      _matched=true;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("void ");
      String _execCall = LinuxTicksUtilsGenerator.getExecCall("int ticks");
      _builder.append(_execCall);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("burnTicks(ticks);");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _switchResult = _builder.toString();
    }
    if (!_matched) {
      if (Objects.equal(valueClass, DiscreteValueStatisticsImpl.class)) {
        _matched=true;
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("void ");
        String _execCall_1 = LinuxTicksUtilsGenerator.getExecCall("double average, int lowerBound, int upperBound");
        _builder_1.append(_execCall_1);
        _builder_1.append(" {");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("burnTicksStatistics(average, lowerBound, upperBound);");
        _builder_1.newLine();
        _builder_1.append("}");
        _builder_1.newLine();
        _builder_1.newLine();
        _switchResult = _builder_1.toString();
      }
    }
    if (!_matched) {
      StringConcatenation _builder_2 = new StringConcatenation();
      _switchResult = _builder_2.toString();
    }
    return _switchResult;
  }
  
  public static String burnTicks(final String burnTicksBody) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void burnTicks(int ticks) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append(burnTicksBody, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String chooseTicks(final ConfigModel configModel) {
    String _switchResult = null;
    TickType _defaultTickType = configModel.getDefaultTickType();
    if (_defaultTickType != null) {
      switch (_defaultTickType) {
        case MINIMUM:
          _switchResult = "lowerBound";
          break;
        case MAXIMUM:
          _switchResult = "upperBound";
          break;
        case AVERAGE:
          _switchResult = "(int)average";
          break;
        default:
          _switchResult = "(int)average";
          break;
      }
    } else {
      _switchResult = "(int)average";
    }
    return _switchResult;
  }
  
  public static String burnTicksStatistics(final ConfigModel configModel) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void burnTicksStatistics(double average, int lowerBound, int upperBound) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("burnTicks(");
    String _chooseTicks = LinuxTicksUtilsGenerator.chooseTicks(configModel);
    _builder.append(_chooseTicks, "\t");
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String burnTicksDefault() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.append("// default implementation of tick burning");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("int numLoops = ticks / 400; ");
    _builder.newLine();
    _builder.append("#\tif defined (__x86_64__)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("int i;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("for (i = 0; i < numLoops; i++) {");
    _builder.newLine();
    {
      IntegerRange _upTo = new IntegerRange(1, 400);
      for(final Integer i : _upTo) {
        _builder.append("\t\t\t");
        _builder.append("__asm volatile(\"nop\");");
        _builder.newLine();
      }
    }
    _builder.append("\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("#\telif defined (__x86_32__) \t\t");
    _builder.newLine();
    _builder.append(" \t\t");
    _builder.append("for (i = 0; i < numLoops; i++) {");
    _builder.newLine();
    {
      IntegerRange _upTo_1 = new IntegerRange(1, 400);
      for(final Integer i_1 : _upTo_1) {
        _builder.append(" \t\t\t \t");
        _builder.append("__asm volatile(\"mov r0, r0\");");
        _builder.newLine();
      }
    }
    _builder.append(" \t\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("#\telif defined (__aarch64__) \t\t");
    _builder.newLine();
    _builder.append(" \t");
    _builder.append("for (i = 0; i < numLoops; i++) {");
    _builder.newLine();
    {
      IntegerRange _upTo_2 = new IntegerRange(1, 400);
      for(final Integer i_2 : _upTo_2) {
        _builder.append(" \t");
        _builder.append("__asm volatile(\"mov x0, x0\");");
        _builder.newLine();
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("#\tendif");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String burnTicksDeclaration() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void burnTicks(int ticks);");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String burnTicksStatisticsDecleration() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void burnTicksStatistics(double average, int lowerBound, int upperBound);");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String generateTicksDeclaration(final Object valueClass) {
    String _switchResult = null;
    boolean _matched = false;
    if (Objects.equal(valueClass, DiscreteValueConstantImpl.class)) {
      _matched=true;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("void ");
      String _execCall = LinuxTicksUtilsGenerator.getExecCall("int ticks");
      _builder.append(_execCall);
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _switchResult = _builder.toString();
    }
    if (!_matched) {
      if (Objects.equal(valueClass, DiscreteValueStatisticsImpl.class)) {
        _matched=true;
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("void ");
        String _execCall_1 = LinuxTicksUtilsGenerator.getExecCall("double average, int lowerBound, int upperBound");
        _builder_1.append(_execCall_1);
        _builder_1.append(";");
        _builder_1.newLineIfNotEmpty();
        _builder_1.newLine();
        _switchResult = _builder_1.toString();
      }
    }
    if (!_matched) {
      StringConcatenation _builder_2 = new StringConcatenation();
      _switchResult = _builder_2.toString();
    }
    return _switchResult;
  }
}
