/**
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.linux.generators;

import com.google.common.base.Objects;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessStatistic;
import org.eclipse.app4mc.amalthea.model.MinAvgMaxStatistic;
import org.eclipse.app4mc.amalthea.model.NumericStatistic;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amalthea.model.SingleValueStatistic;
import org.eclipse.app4mc.amalthea.model.StringObject;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amalthea.model.impl.CustomPropertyImpl;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.PlatformArchitecture;
import org.eclipse.app4mc.slg.linux.generators.CodeSnippet;
import org.eclipse.app4mc.slg.linux.generators.LinuxRealisiticSyntheticGenerator;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.IntegerRange;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class LinuxRunnableGenerator {
  public static class Calculation {
    public int calculatedTicksSum = 0;
    
    public int readsSum = 0;
    
    public int writesSum = 0;
    
    public String matchingCodeSnippetIdsBuffer = "";
    
    public final HashMap<String, Integer> ticksSumMap = new HashMap<String, Integer>();
    
    public int resetCalculatedTicksSum() {
      return this.calculatedTicksSum = 0;
    }
  }
  
  private LinuxRunnableGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String snippetSrcStart(final ConfigModel configModel) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"runnables.h\"");
    _builder.newLine();
    _builder.append("#include \"codesnippets.h\"");
    _builder.newLine();
    _builder.append("#include \"tasks.h\"");
    _builder.newLine();
    _builder.append("#include <pthread.h>");
    _builder.newLine();
    _builder.newLine();
    final PlatformArchitecture platformArchitectureType = configModel.getPlatformArchitectureType();
    _builder.newLineIfNotEmpty();
    {
      if ((platformArchitectureType != null)) {
        _builder.append(" #define __");
        String _name = platformArchitectureType.getName();
        _builder.append(_name);
        _builder.append("__");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("void ticks(int numberTicks){");
    _builder.newLine();
    _builder.append("#\tif defined (__x86_64__)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("for(int i = 0; i < numberTicks; i++){");
    _builder.newLine();
    {
      IntegerRange _upTo = new IntegerRange(1, 400);
      for(final Integer k : _upTo) {
        _builder.append("\t\t");
        _builder.append("__asm volatile(\"nop\");");
        _builder.newLine();
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("#\telif defined (__x86_32__) ");
    _builder.newLine();
    _builder.append("for(int i = 0; i < numberTicks; i++){");
    _builder.newLine();
    {
      IntegerRange _upTo_1 = new IntegerRange(1, 400);
      for(final Integer k_1 : _upTo_1) {
        _builder.append("\t\t");
        _builder.append("__asm volatile(\"mov r0, r0\");");
        _builder.newLine();
      }
    }
    _builder.append("\t\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("#\telif defined (__aarch64__) \t");
    _builder.newLine();
    _builder.append("for(int i = 0; i < numberTicks; i++){");
    _builder.newLine();
    {
      IntegerRange _upTo_2 = new IntegerRange(1, 400);
      for(final Integer k_2 : _upTo_2) {
        _builder.append("\t\t");
        _builder.append("__asm volatile(\"mov x0, x0\");");
        _builder.newLine();
      }
    }
    _builder.append("\t\t\t\t\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("#endif");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String snippetSrcBody(final org.eclipse.app4mc.amalthea.model.Runnable runnable, final String codeString, final boolean enableExtCode) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// Runnable ");
    String _name = null;
    if (runnable!=null) {
      _name=runnable.getName();
    }
    _builder.append(_name);
    _builder.newLineIfNotEmpty();
    _builder.append("void run_");
    String _name_1 = null;
    if (runnable!=null) {
      _name_1=runnable.getName();
    }
    _builder.append(_name_1);
    _builder.append("(char* coreName){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append(codeString, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String snippetIncStart() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"codesnippets.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("void ticks(int numberTicks);");
    _builder.newLine();
    return _builder.toString();
  }
  
  /**
   * This method is used to insert Synthetic code by considering the label-access's, Ticks (default and the PU specific i.e. extended ones)
   */
  public static String syntheticLoad(final org.eclipse.app4mc.amalthea.model.Runnable runnable, final boolean experimental, final Properties properties, final boolean enableExtCode) {
    final LinuxRunnableGenerator.Calculation calc = new LinuxRunnableGenerator.Calculation();
    final StringBuffer codeHookFunctionsBuffer = new StringBuffer();
    final StringBuffer codeHookFunctionsOverwriteBuffer = new StringBuffer();
    LinuxRunnableGenerator.updateContent(runnable, calc, properties, codeHookFunctionsBuffer, codeHookFunctionsOverwriteBuffer);
    StringConcatenation _builder = new StringConcatenation();
    {
      if (enableExtCode) {
        {
          int _length = codeHookFunctionsOverwriteBuffer.length();
          boolean _equals = (_length == 0);
          if (_equals) {
            _builder.append(codeHookFunctionsBuffer);
            _builder.newLineIfNotEmpty();
          } else {
            _builder.append(codeHookFunctionsOverwriteBuffer);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    {
      if (((!enableExtCode) || (codeHookFunctionsOverwriteBuffer.length() == 0))) {
        boolean extendedFound = false;
        _builder.newLineIfNotEmpty();
        {
          List<String> _sort = IterableExtensions.<String>sort(calc.ticksSumMap.keySet());
          for(final String puName : _sort) {
            {
              boolean _equals_1 = puName.equals("default");
              boolean _not = (!_equals_1);
              if (_not) {
                _builder.append("\t\t");
                {
                  if (extendedFound) {
                    _builder.append("else");
                  }
                }
                _builder.append(" if(strcmp(coreName,\"");
                _builder.append(puName, "\t\t");
                _builder.append("\") == 0) {");
                _builder.newLineIfNotEmpty();
                _builder.append("\t\t");
                _builder.append("\t");
                String _syntheticLoadContentForEachPU = LinuxRunnableGenerator.syntheticLoadContentForEachPU(experimental, calc, puName);
                _builder.append(_syntheticLoadContentForEachPU, "\t\t\t");
                _builder.newLineIfNotEmpty();
                _builder.append("\t\t");
                _builder.append("}");
                _builder.newLine();
                _builder.append("\t\t");
                String _xblockexpression = null;
                {
                  extendedFound = true;
                  _xblockexpression = "";
                }
                _builder.append(_xblockexpression, "\t\t");
                _builder.newLineIfNotEmpty();
              }
            }
          }
        }
        {
          boolean _containsKey = calc.ticksSumMap.containsKey("default");
          if (_containsKey) {
            _builder.append("\t\t");
            _builder.newLine();
            _builder.append("\t\t");
            final String value = LinuxRunnableGenerator.syntheticLoadContentForEachPU(experimental, calc, "default");
            _builder.newLineIfNotEmpty();
            {
              if (((value != null) && (!value.isEmpty()))) {
                _builder.append("\t\t");
                {
                  if (extendedFound) {
                    _builder.append("else");
                  }
                }
                _builder.newLineIfNotEmpty();
                _builder.append("\t\t");
                _builder.append("{");
                _builder.newLine();
                _builder.append("\t\t");
                _builder.append("\t");
                _builder.append(value, "\t\t\t");
                _builder.newLineIfNotEmpty();
                _builder.append("\t\t");
                _builder.append("}");
                _builder.newLine();
                _builder.append("\t\t");
                _builder.newLine();
              }
            }
          }
        }
      }
    }
    final String buffer = _builder.toString();
    return buffer;
  }
  
  public static void updateContent(final org.eclipse.app4mc.amalthea.model.Runnable runnable, final LinuxRunnableGenerator.Calculation calculation, final Properties properties, final StringBuffer codehookFct, final StringBuffer codehookFctOverwrite) {
    calculation.ticksSumMap.put("default", Integer.valueOf(0));
    ActivityGraph _activityGraph = null;
    if (runnable!=null) {
      _activityGraph=runnable.getActivityGraph();
    }
    EList<EObject> _eContents = null;
    if (_activityGraph!=null) {
      _eContents=_activityGraph.eContents();
    }
    if (_eContents!=null) {
      final Consumer<EObject> _function = (EObject item) -> {
        if ((item instanceof Ticks)) {
          Integer _get = calculation.ticksSumMap.get("default");
          IDiscreteValueDeviation _default = null;
          if (((Ticks)item)!=null) {
            _default=((Ticks)item).getDefault();
          }
          Double _average = null;
          if (_default!=null) {
            _average=_default.getAverage();
          }
          int _intValue = 0;
          if (_average!=null) {
            _intValue=_average.intValue();
          }
          int _plus = ((_get).intValue() + _intValue);
          calculation.ticksSumMap.put("default", Integer.valueOf(_plus));
          EMap<ProcessingUnitDefinition, IDiscreteValueDeviation> _extended = null;
          if (((Ticks)item)!=null) {
            _extended=((Ticks)item).getExtended();
          }
          Set<ProcessingUnitDefinition> _keySet = null;
          if (_extended!=null) {
            _keySet=_extended.keySet();
          }
          final Set<ProcessingUnitDefinition> puDefinitions = _keySet;
          for (final ProcessingUnitDefinition puDefinition : puDefinitions) {
            if ((puDefinition != null)) {
              boolean _containsKey = calculation.ticksSumMap.containsKey(puDefinition.getName());
              boolean _not = (!_containsKey);
              if (_not) {
                calculation.ticksSumMap.put(puDefinition.getName(), Integer.valueOf(0));
              }
              String _name = puDefinition.getName();
              Integer _get_1 = calculation.ticksSumMap.get(puDefinition.getName());
              EMap<ProcessingUnitDefinition, IDiscreteValueDeviation> _extended_1 = null;
              if (((Ticks)item)!=null) {
                _extended_1=((Ticks)item).getExtended();
              }
              IDiscreteValueDeviation _get_2 = null;
              if (_extended_1!=null) {
                _get_2=_extended_1.get(puDefinition);
              }
              Double _average_1 = null;
              if (_get_2!=null) {
                _average_1=_get_2.getAverage();
              }
              int _intValue_1 = 0;
              if (_average_1!=null) {
                _intValue_1=_average_1.intValue();
              }
              int _plus_1 = ((_get_1).intValue() + _intValue_1);
              calculation.ticksSumMap.put(_name, Integer.valueOf(_plus_1));
            }
          }
        } else {
          if ((item instanceof LabelAccess)) {
            Float value = Float.valueOf(Float.parseFloat(
              properties.getOrDefault("labelAccessStatisticValueDefault", "1.0F").toString()));
            final LabelAccessStatistic labelStatistic = ((LabelAccess)item).getStatistic();
            if ((labelStatistic != null)) {
              NumericStatistic labelStatisticValue = labelStatistic.getValue();
              if ((labelStatisticValue instanceof SingleValueStatistic)) {
                value = Float.valueOf(((SingleValueStatistic)labelStatisticValue).getValue());
              } else {
                if ((labelStatisticValue instanceof MinAvgMaxStatistic)) {
                  value = Float.valueOf(((MinAvgMaxStatistic)labelStatisticValue).getAvg());
                }
              }
            }
            String _string = ((LabelAccess)item).getAccess().toString();
            boolean _equals = Objects.equal(_string, "read");
            if (_equals) {
              int _readsSum = calculation.readsSum;
              int _intValue_2 = value.intValue();
              calculation.readsSum = (_readsSum + _intValue_2);
            } else {
              String _string_1 = ((LabelAccess)item).getAccess().toString();
              boolean _equals_1 = Objects.equal(_string_1, "write");
              if (_equals_1) {
                int _writesSum = calculation.writesSum;
                int _intValue_3 = value.intValue();
                calculation.writesSum = (_writesSum + _intValue_3);
              }
            }
          } else {
            if ((item instanceof CustomPropertyImpl)) {
              boolean _equals_2 = ((CustomPropertyImpl)item).getKey().equals("codehook_overwrite");
              if (_equals_2) {
                final Value value_1 = ((CustomPropertyImpl)item).getValue();
                if ((value_1 instanceof StringObject)) {
                  String _value = ((StringObject)value_1).getValue();
                  String _plus_2 = (_value + ";");
                  codehookFctOverwrite.append(_plus_2);
                  codehookFctOverwrite.append(System.getProperty("line.separator"));
                  codehookFctOverwrite.append(System.getProperty("line.separator"));
                }
              } else {
                boolean _equals_3 = ((CustomPropertyImpl)item).getKey().equals("codehook");
                if (_equals_3) {
                  final Value value_2 = ((CustomPropertyImpl)item).getValue();
                  if ((value_2 instanceof StringObject)) {
                    String _value_1 = ((StringObject)value_2).getValue();
                    String _plus_3 = (_value_1 + ";");
                    codehookFct.append(_plus_3);
                    codehookFct.append(System.getProperty("line.separator"));
                    codehookFct.append(System.getProperty("line.separator"));
                  }
                }
              }
            }
          }
        }
      };
      _eContents.forEach(_function);
    }
  }
  
  private static String syntheticLoadContentForEachPU(final boolean experimental, final LinuxRunnableGenerator.Calculation calc, final String puName) {
    calc.resetCalculatedTicksSum();
    calc.calculatedTicksSum = (calc.ticksSumMap.get(puName)).intValue();
    if (((calc.readsSum != 0) && (calc.readsSum < 10))) {
      calc.readsSum = 10;
    }
    if (((calc.writesSum != 0) && (calc.writesSum < 10))) {
      calc.writesSum = 10;
    }
    calc.matchingCodeSnippetIdsBuffer = "";
    LinuxRunnableGenerator.performCodeSnippetMatching(experimental, calc);
    int _calculatedTicksSum = calc.calculatedTicksSum;
    int _intValue = Double.valueOf((calc.writesSum * 1.0171)).intValue();
    calc.calculatedTicksSum = (_calculatedTicksSum - _intValue);
    int _calculatedTicksSum_1 = calc.calculatedTicksSum;
    int _intValue_1 = Double.valueOf((calc.readsSum * 1.0067)).intValue();
    calc.calculatedTicksSum = (_calculatedTicksSum_1 - _intValue_1);
    int _calculatedTicksSum_2 = calc.calculatedTicksSum;
    calc.calculatedTicksSum = (_calculatedTicksSum_2 / 400);
    int _readsSum = calc.readsSum;
    calc.readsSum = (_readsSum / 10);
    int _writesSum = calc.writesSum;
    calc.writesSum = (_writesSum / 10);
    if (((calc.calculatedTicksSum > 0) && (calc.matchingCodeSnippetIdsBuffer.length() == 0))) {
      LinuxRunnableGenerator.caseWithOnlyTicks(calc);
      return calc.matchingCodeSnippetIdsBuffer;
    } else {
      if ((((calc.calculatedTicksSum <= 0) && ((calc.writesSum > 0) || (calc.readsSum > 0))) && 
        (calc.matchingCodeSnippetIdsBuffer.length() == 0))) {
        LinuxRunnableGenerator.caseWithOutTicksButWithLabelAccesses(calc);
        return calc.matchingCodeSnippetIdsBuffer;
      }
    }
    if (((calc.calculatedTicksSum > 0) && (calc.matchingCodeSnippetIdsBuffer.length() != 0))) {
      LinuxRunnableGenerator.caseWithTicksAndMatchingCodeSnippets(calc);
      return calc.matchingCodeSnippetIdsBuffer;
    }
    return null;
  }
  
  /**
   * This method is used to check if the load specified at the Runnable is matching with any of the pre-defined code-snippet
   */
  private static void performCodeSnippetMatching(final boolean experimental, final LinuxRunnableGenerator.Calculation calc) {
    int i = 0;
    boolean added = false;
    List<CodeSnippet> codesnippets = LinuxRealisiticSyntheticGenerator.getCodeSnippets();
    if (experimental) {
      for (i = 0; (i < ((Object[])Conversions.unwrapArray(codesnippets, Object.class)).length); i++) {
        {
          if ((i == 0)) {
            added = false;
          }
          int ticks_net = ((codesnippets.get(i).ticks - codesnippets.get(i).stallsload) - 
            codesnippets.get(i).stallsstore);
          if ((((ticks_net < calc.calculatedTicksSum) && 
            (codesnippets.get(i).reads < calc.readsSum)) && 
            (codesnippets.get(i).writes < calc.writesSum))) {
            String _matchingCodeSnippetIdsBuffer = calc.matchingCodeSnippetIdsBuffer;
            String _property = System.getProperty("line.separator");
            String _plus = ((codesnippets.get(i).name + "();/\n") + _property);
            calc.matchingCodeSnippetIdsBuffer = (_matchingCodeSnippetIdsBuffer + _plus);
            int _calculatedTicksSum = calc.calculatedTicksSum;
            calc.calculatedTicksSum = (_calculatedTicksSum - ticks_net);
            int _readsSum = calc.readsSum;
            calc.readsSum = (_readsSum - codesnippets.get(i).reads);
            int _writesSum = calc.writesSum;
            calc.writesSum = (_writesSum - codesnippets.get(i).writes);
            added = true;
          }
          if ((added && (i == (((Object[])Conversions.unwrapArray(codesnippets, Object.class)).length - 1)))) {
            i = (-1);
          }
        }
      }
    }
  }
  
  /**
   * This method is used to generate the synthetic code for Runnable in the case when Ticks are present, and the 'synthetic code snippets are matching'
   */
  private static void caseWithTicksAndMatchingCodeSnippets(final LinuxRunnableGenerator.Calculation calc) {
    String[] matchingCodeSnippetsIdsArray = calc.matchingCodeSnippetIdsBuffer.split("/");
    int numberOfCodeSnippets = matchingCodeSnippetsIdsArray.length;
    int _writesSum = calc.writesSum;
    calc.writesSum = (_writesSum / numberOfCodeSnippets);
    int _readsSum = calc.readsSum;
    calc.readsSum = (_readsSum / numberOfCodeSnippets);
    int _calculatedTicksSum = calc.calculatedTicksSum;
    calc.calculatedTicksSum = (_calculatedTicksSum / numberOfCodeSnippets);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("volatile int arraymb[250000];");
    _builder.newLine();
    _builder.append("int i;");
    _builder.newLine();
    calc.matchingCodeSnippetIdsBuffer = _builder.toString();
    final String[] _converted_matchingCodeSnippetsIdsArray = (String[])matchingCodeSnippetsIdsArray;
    final Consumer<String> _function = (String codeSumPart) -> {
      String _matchingCodeSnippetIdsBuffer = calc.matchingCodeSnippetIdsBuffer;
      calc.matchingCodeSnippetIdsBuffer = (_matchingCodeSnippetIdsBuffer + codeSumPart);
      double _random = Math.random();
      int randomNumber1 = Double.valueOf((_random * 200000)).intValue();
      double _random_1 = Math.random();
      int randomNumber2 = Double.valueOf((_random_1 * 200000)).intValue();
      String _matchingCodeSnippetIdsBuffer_1 = calc.matchingCodeSnippetIdsBuffer;
      StringConcatenation _builder_1 = new StringConcatenation();
      {
        if (((calc.writesSum > 0) || (calc.readsSum > 0))) {
          {
            if ((calc.writesSum > 0)) {
              _builder_1.append("for (i = 0; i < ");
              _builder_1.append(calc.writesSum);
              _builder_1.append("; i++){");
              _builder_1.newLineIfNotEmpty();
              {
                IntegerRange _upTo = new IntegerRange(0, 9);
                for(final Integer k : _upTo) {
                  _builder_1.append("\t");
                  _builder_1.append("arraymb[");
                  double _random_2 = Math.random();
                  int _intValue = Double.valueOf((_random_2 * 5000)).intValue();
                  _builder_1.append(_intValue, "\t");
                  _builder_1.append("] = ");
                  _builder_1.append(randomNumber1, "\t");
                  _builder_1.append(";");
                  _builder_1.newLineIfNotEmpty();
                }
              }
              _builder_1.append("}");
              _builder_1.newLine();
            }
          }
          {
            if ((calc.readsSum > 0)) {
              _builder_1.append("for (i = 0; i < ");
              _builder_1.append(calc.readsSum);
              _builder_1.append("; i++){");
              _builder_1.newLineIfNotEmpty();
              {
                IntegerRange _upTo_1 = new IntegerRange(0, 9);
                for(final Integer k_1 : _upTo_1) {
                  _builder_1.append("\t");
                  _builder_1.append("arraymb[");
                  double _random_3 = Math.random();
                  int _intValue_1 = Double.valueOf((_random_3 * 5000)).intValue();
                  _builder_1.append(_intValue_1, "\t");
                  _builder_1.append("] == ");
                  _builder_1.append(randomNumber2, "\t");
                  _builder_1.append(";");
                  _builder_1.newLineIfNotEmpty();
                }
              }
              _builder_1.append("}");
              _builder_1.newLine();
            }
          }
        }
      }
      _builder_1.append("ticks(");
      _builder_1.append(calc.calculatedTicksSum);
      _builder_1.append(");");
      _builder_1.newLineIfNotEmpty();
      calc.matchingCodeSnippetIdsBuffer = (_matchingCodeSnippetIdsBuffer_1 + _builder_1);
    };
    ((List<String>)Conversions.doWrapArray(_converted_matchingCodeSnippetsIdsArray)).forEach(_function);
  }
  
  /**
   * This method is used to generate the synthetic code for Runnable in the case when Ticks and LabelAccess's are present
   */
  private static String caseWithOutTicksButWithLabelAccesses(final LinuxRunnableGenerator.Calculation calc) {
    String _xblockexpression = null;
    {
      double _random = Math.random();
      int randomNumber1 = Double.valueOf((_random * 200000)).intValue();
      double _random_1 = Math.random();
      int randomNumber2 = Double.valueOf((_random_1 * 200000)).intValue();
      StringConcatenation _builder = new StringConcatenation();
      {
        if (((calc.writesSum > 0) || (calc.readsSum > 0))) {
          _builder.append("volatile int arraymb[250000];");
          _builder.newLine();
          _builder.append("int i;");
          _builder.newLine();
          {
            if ((calc.writesSum > 0)) {
              _builder.append("for (i = 0; i < ");
              _builder.append(calc.writesSum);
              _builder.append("; i++){");
              _builder.newLineIfNotEmpty();
              {
                IntegerRange _upTo = new IntegerRange(0, 9);
                for(final Integer k : _upTo) {
                  _builder.append("\t");
                  _builder.append("arraymb[");
                  double _random_2 = Math.random();
                  int _intValue = Double.valueOf((_random_2 * 5000)).intValue();
                  _builder.append(_intValue, "\t");
                  _builder.append("] = ");
                  _builder.append(randomNumber1, "\t");
                  _builder.append(";");
                  _builder.newLineIfNotEmpty();
                }
              }
              _builder.append("}");
              _builder.newLine();
            }
          }
          {
            if ((calc.readsSum > 0)) {
              _builder.append("for (i = 0; i < ");
              _builder.append(calc.readsSum);
              _builder.append("; i++){");
              _builder.newLineIfNotEmpty();
              {
                IntegerRange _upTo_1 = new IntegerRange(0, 9);
                for(final Integer k_1 : _upTo_1) {
                  _builder.append("\t");
                  _builder.append("arraymb[");
                  double _random_3 = Math.random();
                  int _intValue_1 = Double.valueOf((_random_3 * 5000)).intValue();
                  _builder.append(_intValue_1, "\t");
                  _builder.append("] == ");
                  _builder.append(randomNumber2, "\t");
                  _builder.append(";");
                  _builder.newLineIfNotEmpty();
                }
              }
              _builder.append("}");
              _builder.newLine();
            }
          }
        }
      }
      _xblockexpression = calc.matchingCodeSnippetIdsBuffer = _builder.toString();
    }
    return _xblockexpression;
  }
  
  /**
   * This method is used to generate the synthetic code for Runnable in the case when only Ticks are present
   */
  private static String caseWithOnlyTicks(final LinuxRunnableGenerator.Calculation calc) {
    String _xblockexpression = null;
    {
      double _random = Math.random();
      int randomNumber1 = Double.valueOf((_random * 200000)).intValue();
      double _random_1 = Math.random();
      int randomNumber2 = Double.valueOf((_random_1 * 200000)).intValue();
      StringConcatenation _builder = new StringConcatenation();
      {
        if (((calc.writesSum > 0) || (calc.readsSum > 0))) {
          _builder.append("volatile int arraymb[250000];");
          _builder.newLine();
          _builder.append("int i;");
          _builder.newLine();
          {
            if ((calc.writesSum > 0)) {
              _builder.append("for (i = 0; i < ");
              _builder.append(calc.writesSum);
              _builder.append("; i++){");
              _builder.newLineIfNotEmpty();
              {
                IntegerRange _upTo = new IntegerRange(0, 9);
                for(final Integer k : _upTo) {
                  _builder.append("\t");
                  _builder.append("arraymb[");
                  double _random_2 = Math.random();
                  int _intValue = Double.valueOf((_random_2 * 5000)).intValue();
                  _builder.append(_intValue, "\t");
                  _builder.append("] = ");
                  _builder.append(randomNumber1, "\t");
                  _builder.append(";");
                  _builder.newLineIfNotEmpty();
                }
              }
              _builder.append("}");
              _builder.newLine();
            }
          }
          {
            if ((calc.readsSum > 0)) {
              _builder.append("for (i = 0; i < ");
              _builder.append(calc.readsSum);
              _builder.append("; i++){");
              _builder.newLineIfNotEmpty();
              {
                IntegerRange _upTo_1 = new IntegerRange(0, 9);
                for(final Integer k_1 : _upTo_1) {
                  _builder.append("\t");
                  _builder.append("arraymb[");
                  double _random_3 = Math.random();
                  int _intValue_1 = Double.valueOf((_random_3 * 5000)).intValue();
                  _builder.append(_intValue_1, "\t");
                  _builder.append("] == ");
                  _builder.append(randomNumber2, "\t");
                  _builder.append(";");
                  _builder.newLineIfNotEmpty();
                }
              }
              _builder.append("}");
              _builder.newLine();
            }
          }
        }
      }
      _builder.append("\t");
      _builder.append("ticks(");
      _builder.append(calc.calculatedTicksSum, "\t");
      _builder.append(");");
      _builder.newLineIfNotEmpty();
      _xblockexpression = calc.matchingCodeSnippetIdsBuffer = _builder.toString();
    }
    return _xblockexpression;
  }
}
