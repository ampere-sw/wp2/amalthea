/**
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.linux.generators;

import java.util.List;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class LinuxTaskGenerator {
  private LinuxTaskGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static void handleInterProcessTrigger(final List<String> statements, final List<org.eclipse.app4mc.amalthea.model.Process> processedTasks, final Stimulus stimulus) {
    statements.add("//interprocess trigger");
    if ((stimulus != null)) {
      final EList<org.eclipse.app4mc.amalthea.model.Process> processes = stimulus.getAffectedProcesses();
      for (final org.eclipse.app4mc.amalthea.model.Process process : processes) {
        if ((process instanceof Task)) {
          StringConcatenation _builder = new StringConcatenation();
          {
            boolean _contains = processedTasks.contains(process);
            boolean _equals = (_contains == false);
            if (_equals) {
              _builder.append("pthread_t ");
              String _name = ((Task)process).getName();
              _builder.append(_name);
              _builder.append("_;");
              _builder.newLineIfNotEmpty();
              String _xblockexpression = null;
              {
                processedTasks.add(process);
                _xblockexpression = "";
              }
              _builder.append(_xblockexpression);
              _builder.newLineIfNotEmpty();
            }
          }
          _builder.append("\t");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("pthread_create(&");
          String _name_1 = ((Task)process).getName();
          _builder.append(_name_1, "\t");
          _builder.append("_, NULL, *");
          String _name_2 = ((Task)process).getName();
          _builder.append(_name_2, "\t");
          _builder.append("_entry, NULL);");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("//pthread_join(");
          String _name_3 = ((Task)process).getName();
          _builder.append(_name_3, "\t");
          _builder.append("_, NULL);");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.newLine();
          final String codeSnippet = _builder.toString();
          statements.add(codeSnippet);
        }
      }
    }
  }
  
  public static String snippetIncStart() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"runnables.h\"");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String snippetSrcStart(final boolean enableInstrumentation_R) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"tasks.h\"");
    _builder.newLine();
    _builder.append("#include <pthread.h>");
    _builder.newLine();
    {
      if (enableInstrumentation_R) {
        _builder.append("#include \"instrument.h\"");
        _builder.newLine();
        _builder.newLine();
      }
    }
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toH(final Task task) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("void ");
    String _name = task.getName();
    _builder.append(_name, "\t");
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("void *");
    String _name_1 = task.getName();
    _builder.append(_name_1, "\t");
    _builder.append("_entry();");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }
  
  public static String toCpp(final Task task, final List<String> statements) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void ");
    String _name = task.getName();
    _builder.append(_name);
    _builder.append("(){");
    _builder.newLineIfNotEmpty();
    {
      for(final String statement : statements) {
        _builder.append("\t");
        _builder.append(statement, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("void *");
    String _name_1 = task.getName();
    _builder.append(_name_1);
    _builder.append("_entry(){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _name_2 = task.getName();
    _builder.append(_name_2, "\t");
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
}
