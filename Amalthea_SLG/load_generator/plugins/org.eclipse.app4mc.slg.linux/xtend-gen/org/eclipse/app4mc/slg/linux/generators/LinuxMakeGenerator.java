/**
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.linux.generators;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.app4mc.slg.config.CodehookType;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.util.ConfigModelUtils;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class LinuxMakeGenerator {
  private LinuxMakeGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  private static String getHeaderFilesDirectory(final ConfigModel configModel, final String workingDirectory) {
    final StringBuffer buffer = new StringBuffer();
    final List<String> runnableHeaders = ConfigModelUtils.getHeaderFilesDirectories(configModel, CodehookType.RUNNABLE, workingDirectory, null);
    final List<String> tasksHeader = ConfigModelUtils.getHeaderFilesDirectories(configModel, CodehookType.TASK, workingDirectory, null);
    final ArrayList<String> allHeaderDirs = new ArrayList<String>();
    allHeaderDirs.addAll(runnableHeaders);
    allHeaderDirs.addAll(tasksHeader);
    for (final String hfileDir : allHeaderDirs) {
      buffer.append((("-I" + hfileDir) + " "));
    }
    return buffer.toString().trim();
  }
  
  public static String getContent(final boolean experimentalCodeMatching, final boolean instrumentation, final boolean instrumentation_R, final boolean externalCode, final boolean hasOpenMP, final ConfigModel configModel, final String workingDirectory) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("synthetic: main.o tasks.o runnables.o ");
    {
      if (experimentalCodeMatching) {
        _builder.append("codesnippets.o");
      }
    }
    _builder.append(" ");
    {
      if (instrumentation) {
        _builder.append("instrument.o ");
      } else {
        if (instrumentation_R) {
          _builder.append("instrument.o");
        }
      }
    }
    _builder.append(" ");
    {
      if ((!experimentalCodeMatching)) {
        _builder.append(" labels.o ticksUtils.o");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("clang -g");
    {
      if (hasOpenMP) {
        _builder.append(" -fopenmp");
      }
    }
    _builder.append(" -o synthetic main.o tasks.o runnables.o");
    {
      if (hasOpenMP) {
        _builder.append(" tasks_tdg.cpp");
      }
    }
    {
      if ((!experimentalCodeMatching)) {
        _builder.append(" labels.o ticksUtils.o");
      }
    }
    _builder.append(" ");
    {
      if (experimentalCodeMatching) {
        _builder.append("codesnippets.o");
      }
    }
    _builder.append(" ");
    {
      if (instrumentation_R) {
        _builder.append("instrument.o ");
      } else {
        if (instrumentation) {
          _builder.append("instrument.o");
        }
      }
    }
    _builder.append(" -pthread ");
    {
      if (externalCode) {
        {
          List<String> _linkedLibraryInstructions = ConfigModelUtils.getLinkedLibraryInstructions(configModel, workingDirectory);
          for(final String LibPath : _linkedLibraryInstructions) {
            _builder.append(" ");
            _builder.append(LibPath, "\t");
            _builder.append(" ");
          }
        }
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("main.o: Executable/main/_src/main.c");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("clang -g -c ");
    {
      if ((!experimentalCodeMatching)) {
        _builder.append("-Isynthetic_gen/labels/_inc -Isynthetic_gen/ticksUtils/_inc");
      }
    }
    _builder.append("  -Isynthetic_gen/tasks/_inc -Isynthetic_gen/runnables/_inc -Isynthetic_gen ");
    {
      if (externalCode) {
        String _headerFilesDirectory = LinuxMakeGenerator.getHeaderFilesDirectory(configModel, workingDirectory);
        _builder.append(_headerFilesDirectory, "\t");
      }
    }
    _builder.append(" ");
    {
      if (experimentalCodeMatching) {
        _builder.append("-Isynthetic_gen/codesnippets/_inc ");
      }
    }
    _builder.append("Executable/main/_src/main.c");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("tasks.o: synthetic_gen/tasks/_src/tasks.c");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("clang -g");
    {
      if (hasOpenMP) {
        _builder.append(" -fopenmp-taskgraph -fopenmp -static-tdg");
      }
    }
    _builder.append(" -c -Isynthetic_gen ");
    {
      if ((!experimentalCodeMatching)) {
        _builder.append("-Isynthetic_gen/labels/_inc -Isynthetic_gen/ticksUtils/_inc");
      }
    }
    _builder.append("  -Isynthetic_gen/tasks/_inc -Isynthetic_gen/runnables/_inc ");
    {
      if (externalCode) {
        String _headerFilesDirectory_1 = LinuxMakeGenerator.getHeaderFilesDirectory(configModel, workingDirectory);
        _builder.append(_headerFilesDirectory_1, "\t");
      }
    }
    _builder.append(" ");
    {
      if (experimentalCodeMatching) {
        _builder.append("-Isynthetic_gen/codesnippets/_inc ");
      }
    }
    _builder.append("synthetic_gen/tasks/_src/tasks.c");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("runnables.o: synthetic_gen/runnables/_src/runnables.c");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("clang -g");
    {
      if (hasOpenMP) {
        _builder.append(" -fopenmp");
      }
    }
    _builder.append(" -c ");
    {
      if ((!experimentalCodeMatching)) {
        _builder.append("-Isynthetic_gen/labels/_inc -Isynthetic_gen/tasks/_inc -Isynthetic_gen/ticksUtils/_inc");
      }
    }
    _builder.append("  -Isynthetic_gen/runnables/_inc ");
    {
      if (externalCode) {
        String _headerFilesDirectory_2 = LinuxMakeGenerator.getHeaderFilesDirectory(configModel, workingDirectory);
        _builder.append(_headerFilesDirectory_2, "\t");
      }
    }
    _builder.append(" ");
    {
      if (experimentalCodeMatching) {
        _builder.append("-Isynthetic_gen/codesnippets/_inc ");
      }
    }
    _builder.append("synthetic_gen/runnables/_src/runnables.c -O2");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.newLine();
    {
      if ((!experimentalCodeMatching)) {
        _builder.append("labels.o: synthetic_gen/labels/_src/labels.c");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("clang -c -Isynthetic_gen/labels/_inc  synthetic_gen/labels/_src/labels.c -O2");
        _builder.newLine();
        _builder.newLine();
        _builder.append("ticksUtils.o: synthetic_gen/ticksUtils/_src/ticksUtils.c");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("clang -c -Isynthetic_gen/ticksUtils/_inc synthetic_gen/ticksUtils/_src/ticksUtils.c -O2");
        _builder.newLine();
        _builder.newLine();
      } else {
        _builder.append("codesnippets.o: synthetic_gen/codesnippets/_src/codesnippets.c");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("clang -c -Isynthetic_gen/codesnippets/_inc synthetic_gen/codesnippets/_src/codesnippets.c");
        _builder.newLine();
      }
    }
    _builder.newLine();
    {
      if ((instrumentation || instrumentation_R)) {
        _builder.append(" ");
        _builder.newLine();
        _builder.append("instrument.o: synthetic_gen/instrument.c");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("gcc -c -Isynthetic_gen synthetic_gen/instrument.c");
        _builder.newLine();
      }
    }
    _builder.newLine();
    return _builder.toString();
  }
}
