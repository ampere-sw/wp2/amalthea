/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.ros2.generators;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class RosNodeGenerator {
  private RosNodeGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String toHpp(final Tag tag, final String moduleName, final Set<String> headers, final List<String> declarations, final List<String> inits, final List<String> calls, final List<String> serviceCallbacks, final Set<String> codehookHeaders) {
    StringConcatenation _builder = new StringConcatenation();
    {
      for(final String codehookHeader : codehookHeaders) {
        _builder.append(codehookHeader);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("#include <chrono>");
    _builder.newLine();
    _builder.append("#include <memory>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("#include \"rclcpp/rclcpp.hpp\"");
    _builder.newLine();
    _builder.append("#include \"std_msgs/msg/string.hpp\"");
    _builder.newLine();
    _builder.newLine();
    {
      List<String> _sort = IterableExtensions.<String>sort(headers);
      for(final String header : _sort) {
        _builder.append(header);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("using namespace std::chrono_literals;");
    _builder.newLine();
    _builder.append("using std::placeholders::_1;");
    _builder.newLine();
    _builder.append("#define CONSOLE_ENABLED");
    _builder.newLine();
    _builder.newLine();
    _builder.append("#if defined(_OPENMP)");
    _builder.newLine();
    _builder.append("int check_");
    _builder.append(moduleName);
    _builder.append("(int *original, int *replicated){ return 1;}");
    _builder.newLineIfNotEmpty();
    _builder.append("#endif");
    _builder.newLine();
    _builder.append("class ");
    _builder.append(moduleName);
    _builder.append(" : public rclcpp::Node");
    _builder.newLineIfNotEmpty();
    _builder.append("{");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private:");
    _builder.newLine();
    {
      LinkedHashSet<String> _linkedHashSet = new LinkedHashSet<String>(declarations);
      for(final String declaration : _linkedHashSet) {
        _builder.append("\t\t");
        _builder.append(declaration, "\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public:");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append(moduleName, "\t\t");
    _builder.append("()");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append(": Node(\"");
    String _lowerCase = moduleName.toLowerCase();
    _builder.append(_lowerCase, "\t\t");
    _builder.append("\")");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("{");
    _builder.newLine();
    {
      for(final String init : inits) {
        _builder.append("\t\t\t");
        _builder.append(init, "\t\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    {
      for(final String call : calls) {
        _builder.append("\t");
        _builder.append(call, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      for(final String serviceCallback : serviceCallbacks) {
        _builder.append("\t");
        _builder.append(serviceCallback, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("};");
    _builder.newLine();
    return _builder.toString();
  }
}
