/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.microros.generators;

import com.google.inject.Singleton;
import java.util.List;
import java.util.Set;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.ros2.transformers.RosModel2TextTransformer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@Singleton
@SuppressWarnings("all")
public class MicroRosModel2TextGenerator {
  public static String toLaunchFile(final Set<String> nodes) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from launch import LaunchDescription");
    _builder.newLine();
    _builder.append("from launch_ros.actions import Node");
    _builder.newLine();
    _builder.newLine();
    _builder.append("def generate_launch_description():");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return LaunchDescription([");
    _builder.newLine();
    {
      List<String> _sort = IterableExtensions.<String>sort(nodes);
      for(final String node : _sort) {
        _builder.append("\t\t");
        _builder.append("Node(");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("\t");
        _builder.append("package=\'amalthea_micro_ros_model\',");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("\t");
        _builder.append("node_namespace=\'\',");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("\t");
        _builder.append("node_executable=\'");
        _builder.append(node, "\t\t\t");
        _builder.append("\',");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("),");
        _builder.newLine();
      }
    }
    _builder.append("\t");
    _builder.append("])");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toBuildScript(final Set<String> services) {
    StringConcatenation _builder = new StringConcatenation();
    {
      List<String> _sort = IterableExtensions.<String>sort(services);
      for(final String service : _sort) {
        _builder.append("#building service ");
        _builder.append(service);
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        _builder.append("cd services/");
        _builder.append(service);
        _builder.newLineIfNotEmpty();
        _builder.append("colcon build");
        _builder.newLine();
        _builder.append(". install/setup.bash");
        _builder.newLine();
        _builder.append("cd ../..");
        _builder.newLine();
        _builder.newLine();
      }
    }
    _builder.append("colcon build");
    _builder.newLine();
    _builder.append(". install/setup.bash");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toPackageXml(final Set<String> services) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\"?>");
    _builder.newLine();
    _builder.append("<?xml-model href=\"http://download.ros.org/schema/package_format2.xsd\" schematypens=\"http://www.w3.org/2001/XMLSchema\"?>");
    _builder.newLine();
    _builder.append("<package format=\"2\">");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<name>amalthea_micro_ros_model</name>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<version>1.0.1</version>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<description>Example of using rclc_executor</description>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<maintainer email=\"app4mc-dev@eclipse.org\">app4mc-dev</maintainer>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<license>Apache License 2.0</license>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<buildtool_depend>ament_cmake_ros</buildtool_depend>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<build_depend>rcl</build_depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<build_depend>rclc</build_depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<build_depend>rclc_lifecycle</build_depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<build_depend>std_msgs</build_depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<build_depend>lifecycle_msgs</build_depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<build_depend>example_interfaces</build_depend>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<exec_depend>rcl</exec_depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<exec_depend>rclc</exec_depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<exec_depend>rclc_lifecycle</exec_depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<exec_depend>std_msgs</exec_depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<exec_depend>lifecycle_msgs</exec_depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<exec_depend>example_interfaces</exec_depend>");
    _builder.newLine();
    {
      List<String> _sort = IterableExtensions.<String>sort(services);
      for(final String service : _sort) {
        _builder.append("\t");
        _builder.append("<depend>");
        _builder.append(service, "\t");
        _builder.append("</depend>");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<test_depend>ament_lint_auto</test_depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<test_depend>ament_lint_common</test_depend>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<export>");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<build_type>ament_cmake</build_type>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</export>");
    _builder.newLine();
    _builder.append("</package>");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toCmake(final RosModel2TextTransformer.TargetInfo targetInfo, final boolean externalCode, final ConfigModel configModel, final String workingDirectory) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# CMakeLists for Nodes");
    _builder.newLine();
    _builder.append("cmake_minimum_required(VERSION 3.5)");
    _builder.newLine();
    _builder.append("project(amalthea_micro_ros_model)");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Default to C11");
    _builder.newLine();
    _builder.append("if(NOT CMAKE_C_STANDARD)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("set(CMAKE_C_STANDARD 11)");
    _builder.newLine();
    _builder.append("endif()");
    _builder.newLine();
    _builder.newLine();
    _builder.append("if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES \"Clang\")");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("add_compile_options(-Wall -Wextra -Wpedantic)");
    _builder.newLine();
    _builder.append("endif()");
    _builder.newLine();
    _builder.newLine();
    _builder.append("set(CMAKE_VERBOSE_MAKEFILE ON)");
    _builder.newLine();
    _builder.newLine();
    _builder.append("find_package(ament_cmake_ros REQUIRED)");
    _builder.newLine();
    _builder.append("find_package(rcl REQUIRED)");
    _builder.newLine();
    _builder.append("find_package(rcl_lifecycle REQUIRED)");
    _builder.newLine();
    _builder.append("find_package(rclc REQUIRED)");
    _builder.newLine();
    _builder.append("find_package(rclc_lifecycle REQUIRED)");
    _builder.newLine();
    _builder.append("find_package(std_msgs REQUIRED)");
    _builder.newLine();
    _builder.append("find_package(lifecycle_msgs REQUIRED)");
    _builder.newLine();
    _builder.append("find_package(example_interfaces REQUIRED)");
    _builder.newLine();
    _builder.newLine();
    _builder.append("include_directories(include) ");
    _builder.newLine();
    {
      List<String> _sort = IterableExtensions.<String>sort(targetInfo.getServices());
      for(final String service : _sort) {
        _builder.append("find_package(");
        _builder.append(service);
        _builder.append(" REQUIRED)");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      boolean _hasChannelSend = targetInfo.hasChannelSend();
      if (_hasChannelSend) {
        _builder.newLine();
        _builder.append("add_library(CHANNELSEND_UTILS  STATIC");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("synthetic_gen/channelSendUtils/_src/channelSendUtils.cpp");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
        _builder.append("ament_target_dependencies(CHANNELSEND_UTILS rcl rclc std_msgs)");
        _builder.newLine();
        _builder.append("target_include_directories(CHANNELSEND_UTILS");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("PUBLIC synthetic_gen/channelSendUtils/_inc/");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
      }
    }
    {
      boolean _hasInterprocess = targetInfo.hasInterprocess();
      if (_hasInterprocess) {
        _builder.newLine();
        _builder.append("add_library(INTERPROCESSTRIGGER_UTIL STATIC");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("synthetic_gen/interProcessTriggerUtils/_src/interProcessTriggerUtils.cpp");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
        _builder.append("ament_target_dependencies(INTERPROCESSTRIGGER_UTIL rcl rclc ");
        {
          List<String> _sort_1 = IterableExtensions.<String>sort(targetInfo.getServices());
          for(final String service_1 : _sort_1) {
            _builder.append(service_1);
            _builder.append(" ");
          }
        }
        _builder.append(")");
        _builder.newLineIfNotEmpty();
        _builder.append("target_include_directories(INTERPROCESSTRIGGER_UTIL");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("PUBLIC synthetic_gen/interProcessTriggerUtils/_inc");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
      }
    }
    {
      boolean _hasPerformanceMeasurement = targetInfo.hasPerformanceMeasurement();
      if (_hasPerformanceMeasurement) {
        _builder.newLine();
        _builder.append("add_library(AML_LIB  STATIC");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("utils/aml/_src/aml.cpp");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
        _builder.append("target_include_directories(AML_LIB");
        _builder.newLine();
        _builder.append("    ");
        _builder.append("PUBLIC utils/aml/_inc/");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
      }
    }
    _builder.newLine();
    {
      boolean _hasLabel = targetInfo.hasLabel();
      if (_hasLabel) {
        _builder.append("add_subdirectory (synthetic_gen/labels)");
        _builder.newLine();
      }
    }
    {
      boolean _hasTicks = targetInfo.hasTicks();
      if (_hasTicks) {
        _builder.append("add_subdirectory (synthetic_gen/ticksUtils)");
        _builder.newLine();
      }
    }
    _builder.newLine();
    _builder.append("# RUNNABLES_LIB ################################################################");
    _builder.newLine();
    _builder.newLine();
    _builder.append("####");
    _builder.newLine();
    _builder.append("add_library(RUNNABLES_LIB STATIC");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("synthetic_gen/runnables/_src/runnables.cpp");
    _builder.newLine();
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("target_include_directories(RUNNABLES_LIB");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("PUBLIC  synthetic_gen/runnables/_inc");
    _builder.newLine();
    _builder.append(")\t");
    _builder.newLine();
    _builder.newLine();
    _builder.append("target_include_directories(RUNNABLES_LIB");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("PUBLIC ");
    _builder.newLine();
    {
      boolean _hasTicks_1 = targetInfo.hasTicks();
      if (_hasTicks_1) {
        _builder.append("\t");
        _builder.append("synthetic_gen/ticksUtils/_inc");
        _builder.newLine();
      }
    }
    {
      boolean _hasLabel_1 = targetInfo.hasLabel();
      if (_hasLabel_1) {
        _builder.append("\t");
        _builder.append("synthetic_gen/labels/_inc");
        _builder.newLine();
      }
    }
    {
      boolean _hasChannelSend_1 = targetInfo.hasChannelSend();
      if (_hasChannelSend_1) {
        _builder.append("\t");
        _builder.append("synthetic_gen/channelSendUtils/_inc");
        _builder.newLine();
      }
    }
    {
      boolean _hasInterprocess_1 = targetInfo.hasInterprocess();
      if (_hasInterprocess_1) {
        _builder.append("\t");
        _builder.append("synthetic_gen/interProcessTriggerUtils/_inc");
        _builder.newLine();
      }
    }
    {
      boolean _hasPerformanceMeasurement_1 = targetInfo.hasPerformanceMeasurement();
      if (_hasPerformanceMeasurement_1) {
        _builder.append("\t");
        _builder.append("utils/aml/_inc");
        _builder.newLine();
      }
    }
    _builder.append(")");
    _builder.newLine();
    _builder.append("target_link_libraries(RUNNABLES_LIB");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("PRIVATE ");
    {
      boolean _hasPerformanceMeasurement_2 = targetInfo.hasPerformanceMeasurement();
      if (_hasPerformanceMeasurement_2) {
        _builder.append("AML_LIB");
      }
    }
    _builder.append(" ");
    {
      boolean _hasLabel_2 = targetInfo.hasLabel();
      if (_hasLabel_2) {
        _builder.append("LABELS_LIB");
      }
    }
    _builder.append(" ");
    {
      boolean _hasTicks_2 = targetInfo.hasTicks();
      if (_hasTicks_2) {
        _builder.append("TICKS_UTILS");
      }
    }
    _builder.append(" ");
    {
      boolean _hasChannelSend_2 = targetInfo.hasChannelSend();
      if (_hasChannelSend_2) {
        _builder.append("CHANNELSEND_UTILS");
      }
    }
    _builder.append(" ");
    {
      boolean _hasInterprocess_2 = targetInfo.hasInterprocess();
      if (_hasInterprocess_2) {
        _builder.append("INTERPROCESSTRIGGER_UTIL");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    {
      List<String> _sort_2 = IterableExtensions.<String>sort(targetInfo.getNodes());
      for(final String node : _sort_2) {
        _builder.newLine();
        _builder.append("add_executable(");
        _builder.append(node);
        _builder.append(" ");
        _builder.append(node);
        _builder.append("/_src/");
        _builder.append(node);
        _builder.append(".cpp)");
        _builder.newLineIfNotEmpty();
        _builder.append("target_link_libraries(");
        _builder.append(node);
        _builder.append("  ");
        {
          boolean _hasLabel_3 = targetInfo.hasLabel();
          if (_hasLabel_3) {
            _builder.append("LABELS_LIB");
          }
        }
        _builder.append(" RUNNABLES_LIB)");
        _builder.newLineIfNotEmpty();
        _builder.append("ament_target_dependencies(");
        _builder.append(node);
        _builder.append(" rcl rclc std_msgs ");
        {
          List<String> _sort_3 = IterableExtensions.<String>sort(targetInfo.getServices());
          for(final String service_2 : _sort_3) {
            _builder.append(service_2);
            _builder.append(" ");
          }
        }
        _builder.append(")");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.newLine();
    _builder.append("install(TARGETS");
    _builder.newLine();
    {
      List<String> _sort_4 = IterableExtensions.<String>sort(targetInfo.getNodes());
      for(final String node_1 : _sort_4) {
        _builder.append("\t");
        _builder.append(node_1, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("DESTINATION lib/${PROJECT_NAME}");
    _builder.newLine();
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("ament_package()");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
}
