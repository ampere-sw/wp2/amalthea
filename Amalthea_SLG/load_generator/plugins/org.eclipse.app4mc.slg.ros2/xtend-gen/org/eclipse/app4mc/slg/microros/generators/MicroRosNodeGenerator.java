/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.microros.generators;

import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.Channel;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.slg.ros2.artefacts.ROS2SLGTransformationDefinition;
import org.eclipse.app4mc.slg.ros2.transformers.utils.Code;
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class MicroRosNodeGenerator {
  private MicroRosNodeGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String toC(final Tag tag, final String moduleName, final Properties properties, final Set<String> headers, final List<String> declarations, final List<String> inits, final List<String> periodicCalls, final List<String> calls, final List<String> serviceCallbacks, final Set<String> globalDeclarations, final Amalthea model, final Collection<Stimulus> stimuli) {
    String _xblockexpression = null;
    {
      final String nodeName = RosModelUtils.getROSNodeName(tag).toLowerCase();
      final Function1<Channel, String> _function = (Channel it) -> {
        return it.getName().toLowerCase();
      };
      final Iterable<String> subscriberChannelNames = IterableExtensions.<Channel, String>map(RosModelUtils.getSubscribers(tag), _function);
      final Function1<Channel, String> _function_1 = (Channel it) -> {
        return it.getName().toLowerCase();
      };
      final Iterable<String> publisherChannelNames = IterableExtensions.<Channel, String>map(RosModelUtils.getPublishers(tag), _function_1);
      final boolean isErika = RosModelUtils.getROSTypeName(tag).startsWith("Erika");
      final boolean genStatusOutput = Boolean.parseBoolean(properties.getProperty(ROS2SLGTransformationDefinition.PARAM_GENERATE_STATUS_OUTPUT));
      final boolean genDebugOutput = Boolean.parseBoolean(properties.getProperty(ROS2SLGTransformationDefinition.PARAM_GENERATE_DEBUG_OUTPUT));
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(Code.IFDEF_EXTERN_C_BEGIN);
      _builder.newLineIfNotEmpty();
      _builder.append("#include <stdio.h>");
      _builder.newLine();
      _builder.append("#include <std_msgs/msg/string.h>");
      _builder.newLine();
      _builder.append("#include <rclc/executor.h>");
      _builder.newLine();
      _builder.append("#include <rclc/rclc.h>");
      _builder.newLine();
      {
        for(final String header : headers) {
          _builder.append(header);
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      {
        if (isErika) {
          _builder.append("/* ERIKA include */");
          _builder.newLine();
          _builder.append("#include \"ee.h\"");
          _builder.newLine();
          _builder.append("#include \"erika_board_cfg.h\"");
          _builder.newLine();
          _builder.newLine();
          _builder.append("/* Console output */");
          _builder.newLine();
          _builder.append("#include \"output_console.h\"");
          _builder.newLine();
        }
      }
      _builder.newLine();
      _builder.append("/******************************************************************************");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("*  PERIODIC Tasks");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("*****************************************************************************/");
      _builder.newLine();
      {
        for(final String call : periodicCalls) {
          _builder.append(call);
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _builder.append("/******************************************************************************");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("*  MICROROS Task");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("*****************************************************************************/");
      _builder.newLine();
      _builder.append("/*** MICROROS DEFINITION ****/");
      _builder.newLine();
      _builder.append("#define PUB_MSG_CAPACITY 20");
      _builder.newLine();
      _builder.newLine();
      _builder.append("rcl_allocator_t allocator;");
      _builder.newLine();
      _builder.append("rclc_support_t support;");
      _builder.newLine();
      _builder.append("rclc_executor_t executor;");
      _builder.newLine();
      _builder.append("rcl_node_t ");
      _builder.append(nodeName);
      _builder.append("_node;");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("//timers --> Periodic tasks");
      _builder.newLine();
      _builder.newLine();
      {
        for(final String topic : publisherChannelNames) {
          _builder.append("//");
          _builder.append(topic);
          _builder.append(" publisher");
          _builder.newLineIfNotEmpty();
          _builder.append("rcl_publisher_t ");
          _builder.append(nodeName);
          _builder.append("Node_");
          _builder.append(topic);
          _builder.append("_publisher;");
          _builder.newLineIfNotEmpty();
          _builder.append("std_msgs__msg__String ");
          _builder.append(nodeName);
          _builder.append("Node_");
          _builder.append(topic);
          _builder.append("_msg;");
          _builder.newLineIfNotEmpty();
          _builder.append("char ");
          _builder.append(nodeName);
          _builder.append("Node_");
          _builder.append(topic);
          _builder.append("_msg_buffer[PUB_MSG_CAPACITY];");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      {
        for(final String topic_1 : subscriberChannelNames) {
          _builder.append("//");
          _builder.append(topic_1);
          _builder.append(" subscription");
          _builder.newLineIfNotEmpty();
          _builder.append("rcl_subscription_t  ");
          _builder.append(nodeName);
          _builder.append("Node_");
          _builder.append(topic_1);
          _builder.append("_subscription;");
          _builder.newLineIfNotEmpty();
          _builder.append("std_msgs__msg__String ");
          _builder.append(nodeName);
          _builder.append("Node_");
          _builder.append(topic_1);
          _builder.append("_msg;");
          _builder.newLineIfNotEmpty();
          _builder.append("char ");
          _builder.append(nodeName);
          _builder.append("Node_");
          _builder.append(topic_1);
          _builder.append("_msg_buffer[PUB_MSG_CAPACITY];");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _builder.append("/*** MICROROS CALLBACKS *******/");
      _builder.newLine();
      {
        for(final String call_1 : calls) {
          _builder.append(call_1);
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _builder.newLine();
      _builder.append("/*** MICROROS INITIALIZATION *****/");
      _builder.newLine();
      _builder.append("int uros_init(void)");
      _builder.newLine();
      _builder.append("{");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("allocator = rcl_get_default_allocator();");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("rcl_ret_t rc;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//Create init options");
      _builder.newLine();
      {
        if (genStatusOutput) {
          _builder.append("\t");
          _builder.append("printf(\"Erika: create uROS init_options\\n\");");
          _builder.newLine();
        }
      }
      _builder.append("\t");
      _builder.append("rc = rclc_support_init(&support, 0, NULL, &allocator);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("if (rc != RCL_RET_OK) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("printf(\"Error rclc_support_init.\\n\");");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("return -1;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//Create rcl node");
      _builder.newLine();
      {
        if (genStatusOutput) {
          _builder.append("\t");
          _builder.append("printf(\"Erika: create uROS node\\n\");");
          _builder.newLine();
        }
      }
      _builder.append("\t");
      _builder.append("rc = rclc_node_init_default(&");
      _builder.append(nodeName, "\t");
      _builder.append("_node, \"");
      _builder.append(nodeName, "\t");
      _builder.append("\", \"\", &support);");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("if (rc != RCL_RET_OK) {");
      _builder.newLine();
      _builder.append("\t    ");
      _builder.append("printf(\"Error in rclc_node_init_default\\n\");");
      _builder.newLine();
      _builder.append("\t    ");
      _builder.append("return -1;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.newLine();
      {
        for(final String topic_2 : publisherChannelNames) {
          _builder.append("\t");
          _builder.append("//Create a publisher of topic ");
          _builder.append(topic_2, "\t");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("const char * ");
          _builder.append(topic_2, "\t");
          _builder.append("_topic_name = \"");
          _builder.append(topic_2, "\t");
          _builder.append("\";");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("const rosidl_message_type_support_t * ");
          _builder.append(topic_2, "\t");
          _builder.append("_type_support = ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, String);");
          _builder.newLineIfNotEmpty();
          {
            if (genStatusOutput) {
              _builder.append("\t");
              _builder.append("printf(\"Erika: create publisher %s\\n\", ");
              _builder.append(topic_2, "\t");
              _builder.append("_topic_name);");
              _builder.newLineIfNotEmpty();
            }
          }
          _builder.append("\t");
          _builder.append("rc = rclc_publisher_init_default(");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("&");
          _builder.append(nodeName, "\t\t");
          _builder.append("Node_");
          _builder.append(topic_2, "\t\t");
          _builder.append("_publisher,");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("&");
          _builder.append(nodeName, "\t\t");
          _builder.append("_node,");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append(topic_2, "\t\t");
          _builder.append("_type_support,");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append(topic_2, "\t\t");
          _builder.append("_topic_name);");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("if (RCL_RET_OK != rc) {");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("printf(\"Error in rclc_publisher_init_default %s.\\n\", ");
          _builder.append(topic_2, "\t\t");
          _builder.append("_topic_name);");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("return -1;");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("}");
          _builder.newLine();
          _builder.append("\t");
          _builder.append(nodeName, "\t");
          _builder.append("Node_");
          _builder.append(topic_2, "\t");
          _builder.append("_msg.data.data = ");
          _builder.append(nodeName, "\t");
          _builder.append("Node_");
          _builder.append(topic_2, "\t");
          _builder.append("_msg_buffer;");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append(nodeName, "\t");
          _builder.append("Node_");
          _builder.append(topic_2, "\t");
          _builder.append("_msg.data.capacity = PUB_MSG_CAPACITY;");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("snprintf(");
          _builder.append(nodeName, "\t");
          _builder.append("Node_");
          _builder.append(topic_2, "\t");
          _builder.append("_msg.data.data, ");
          _builder.append(nodeName, "\t");
          _builder.append("Node_");
          _builder.append(topic_2, "\t");
          _builder.append("_msg.data.capacity, \"AAAAAAAAAAAAAAAAAAA\");");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append(nodeName, "\t");
          _builder.append("Node_");
          _builder.append(topic_2, "\t");
          _builder.append("_msg.data.size = strlen(");
          _builder.append(nodeName, "\t");
          _builder.append("Node_");
          _builder.append(topic_2, "\t");
          _builder.append("_msg.data.data);");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      {
        for(final String topic_3 : subscriberChannelNames) {
          _builder.append("\t");
          _builder.append("//Create subscription on topic ");
          _builder.append(topic_3, "\t");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("const char * ");
          _builder.append(topic_3, "\t");
          _builder.append("_topic_name = \"");
          _builder.append(topic_3, "\t");
          _builder.append("\";");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("const rosidl_message_type_support_t * ");
          _builder.append(topic_3, "\t");
          _builder.append("_type_support = ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, String);");
          _builder.newLineIfNotEmpty();
          {
            if (genStatusOutput) {
              _builder.append("\t");
              _builder.append("printf(\"Erika: create subscriber %s\\n\", ");
              _builder.append(topic_3, "\t");
              _builder.append("_topic_name);");
              _builder.newLineIfNotEmpty();
            }
          }
          _builder.append("\t");
          _builder.append("rc = rclc_subscription_init_default(");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("&");
          _builder.append(nodeName, "\t\t");
          _builder.append("Node_");
          _builder.append(topic_3, "\t\t");
          _builder.append("_subscription,");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("&");
          _builder.append(nodeName, "\t\t");
          _builder.append("_node,");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append(topic_3, "\t\t");
          _builder.append("_type_support,");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append(topic_3, "\t\t");
          _builder.append("_topic_name);");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("if (rc != RCL_RET_OK) {");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("printf(\"Failed to create subscriber %s.\\n\", ");
          _builder.append(topic_3, "\t\t");
          _builder.append("_topic_name);");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("return -1;");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("}");
          _builder.newLine();
          _builder.append("\t");
          _builder.append(nodeName, "\t");
          _builder.append("Node_");
          _builder.append(topic_3, "\t");
          _builder.append("_msg.data.data = ");
          _builder.append(nodeName, "\t");
          _builder.append("Node_");
          _builder.append(topic_3, "\t");
          _builder.append("_msg_buffer;");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append(nodeName, "\t");
          _builder.append("Node_");
          _builder.append(topic_3, "\t");
          _builder.append("_msg.data.capacity = PUB_MSG_CAPACITY;");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//Configure the RCL Executor");
      _builder.newLine();
      {
        if (genStatusOutput) {
          _builder.append("\t");
          _builder.append("printf(\"Configure executor\\n\");");
          _builder.newLine();
        }
      }
      _builder.append("\t");
      _builder.append("executor = rclc_executor_get_zero_initialized_executor();");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("//Setup the number of handles = #timers + #subscriptions");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("unsigned int num_handles =  ");
      int _size = stimuli.size();
      _builder.append(_size, "\t");
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      {
        if (genDebugOutput) {
          _builder.append("\t");
          _builder.append("//printf(\"Erika: number of DDS handles: %u\\n\", num_handles);");
          _builder.newLine();
        }
      }
      _builder.append("\t");
      _builder.append("rclc_executor_init(&executor, &support.context, num_handles, &allocator);");
      _builder.newLine();
      _builder.newLine();
      _builder.newLine();
      {
        for(final String item : subscriberChannelNames) {
          _builder.append("\t");
          _builder.append("//Add subscription ");
          _builder.append(item, "\t");
          _builder.append(" to executor");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("rc = rclc_executor_add_subscription(");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("&executor,");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("&");
          _builder.append(nodeName, "\t\t");
          _builder.append("Node_");
          _builder.append(item, "\t\t");
          _builder.append("_subscription,");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("&");
          _builder.append(nodeName, "\t\t");
          _builder.append("Node_");
          _builder.append(item, "\t\t");
          _builder.append("_msg, &");
          _builder.append(item, "\t\t");
          _builder.append("_subscription_callback,");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("ON_NEW_DATA);");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("if (rc != RCL_RET_OK) {");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("printf(\"Error in rclc_executor_add_subscription. \\n\");");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("\t");
          _builder.append("return -1;");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("}");
          _builder.newLine();
        }
      }
      _builder.newLine();
      {
        if (genStatusOutput) {
          _builder.append("\t");
          _builder.append("printf(\"Successful MicroROS Initalization\\n\");");
          _builder.newLine();
        }
      }
      _builder.append("\t");
      _builder.append("return 0;");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("/*** MICROROS TASK *****/");
      _builder.newLine();
      _builder.append("extern void uros_communication_setup(void);");
      _builder.newLine();
      _builder.append("int uros_init_done = 0;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("/* uROS Initialization task (one-shot) */");
      _builder.newLine();
      {
        if (isErika) {
          _builder.append("DeclareTask(uROSInitTask);");
          _builder.newLine();
          _builder.append("TASK(uROSInitTask)");
          _builder.newLine();
          _builder.append("{");
          _builder.newLine();
        }
      }
      {
        if (genStatusOutput) {
          _builder.append("\t");
          _builder.append("printf(\"Activate MicroROS Init Task\\n\");");
          _builder.newLine();
        }
      }
      _builder.append("\t");
      _builder.append("/* Setup microros transport functions */");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("uros_communication_setup();");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("/* Initialized microros data for PingPong app */");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("int ret = uros_init();");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("/* Set the initialization as DONE */");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("if (ret == 0) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("uros_init_done = 1;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      {
        if (isErika) {
          _builder.append("\t");
          _builder.append("TerminateTask();");
          _builder.newLine();
          _builder.append("}");
          _builder.newLine();
        }
      }
      _builder.newLine();
      _builder.append("/* uROS Ping Pong task (periodic = 10000usec) */");
      _builder.newLine();
      {
        if (isErika) {
          _builder.append("DeclareTask(uROSPeriodicTask);");
          _builder.newLine();
          _builder.append("TASK(uROSPeriodicTask)");
          _builder.newLine();
          _builder.append("{");
          _builder.newLine();
        }
      }
      _builder.append("\t");
      _builder.append("if(uros_init_done == 1) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("rclc_executor_spin_some(&executor, RCL_MS_TO_NS(10));");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      {
        if (isErika) {
          _builder.append("}");
          _builder.newLine();
        }
      }
      _builder.newLine();
      _builder.newLine();
      _builder.append("/******************************************************************************");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("*  MAIN");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("*****************************************************************************/");
      _builder.newLine();
      _builder.append("void idle_hook(void);");
      _builder.newLine();
      _builder.append("void idle_hook(void)");
      _builder.newLine();
      _builder.append("{");
      _builder.newLine();
      {
        if (genStatusOutput) {
          _builder.append("\t");
          _builder.append("printf(\"Starting communication over UART\\n\");");
          _builder.newLine();
        }
      }
      {
        if (isErika) {
          _builder.append("\t");
          _builder.append("/* Activate task performing MicroROS app */");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("ActivateTask(uROSInitTask);");
          _builder.newLine();
        }
      }
      _builder.append("\t");
      _builder.append("/* endless loop*/");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("while (1) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("asm volatile(\"wfi\": : : \"memory\");");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.append(" ");
      _builder.newLine();
      _builder.append("int main(void)");
      _builder.newLine();
      _builder.append("{");
      _builder.newLine();
      {
        if (isErika) {
          _builder.append("\t");
          _builder.append("erika_console_init();");
          _builder.newLine();
          {
            if (genStatusOutput) {
              _builder.append("\t");
              _builder.append("printf(\"Starting ");
              String _upperCase = nodeName.toUpperCase();
              _builder.append(_upperCase, "\t");
              _builder.append(" (MicroROS node)\\n\");");
              _builder.newLineIfNotEmpty();
            }
          }
          _builder.append("\t");
          _builder.append("StartOS(OSDEFAULTAPPMODE);");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("return 0;");
          _builder.newLine();
        }
      }
      _builder.append("}");
      _builder.newLine();
      _builder.append(Code.IFDEF_EXTERN_C_END);
      _builder.newLineIfNotEmpty();
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
}
