/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.ros2.generators;

import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.slg.ros2.transformers.utils.Utils;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class RosInterProcessTriggerUtilsGenerator {
  private RosInterProcessTriggerUtilsGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String toCPPHead() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"interProcessTriggerUtils.hpp\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("using namespace std::chrono_literals;");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toHeader() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <string>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("#include \"rclcpp/rclcpp.hpp\"");
    _builder.newLine();
    _builder.append("#include \"std_msgs/msg/string.hpp\"");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toCPP(final InterProcessTrigger ipt) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("void call_service_");
    String _name = ipt.getStimulus().getName();
    _builder.append(_name);
    _builder.append("(rclcpp::Client<");
    String _name_1 = ipt.getStimulus().getName();
    _builder.append(_name_1);
    _builder.append("_service::srv::");
    String _name_2 = ipt.getStimulus().getName();
    String _plus = (_name_2 + "_service");
    String _idlCompliantName = Utils.toIdlCompliantName(_plus);
    _builder.append(_idlCompliantName);
    _builder.append(">::SharedPtr& client) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("auto request = std::make_shared<");
    String _name_3 = ipt.getStimulus().getName();
    _builder.append(_name_3, "\t");
    _builder.append("_service::srv::");
    String _name_4 = ipt.getStimulus().getName();
    String _plus_1 = (_name_4 + "_service");
    String _idlCompliantName_1 = Utils.toIdlCompliantName(_plus_1);
    _builder.append(_idlCompliantName_1, "\t");
    _builder.append("::Request>();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("while (!client->wait_for_service(50ms)) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("if (!rclcpp::ok()) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("RCLCPP_ERROR(rclcpp::get_logger(\"rclcpp\"), \"Interrupted while waiting for the service. Exiting.\");");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("exit;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("RCLCPP_INFO(rclcpp::get_logger(\"rclcpp\"), \"service not available, waiting again...\");");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("auto result = client->async_send_request(request);\t");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toH(final InterProcessTrigger ipt) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _name = ipt.getStimulus().getName();
    _builder.append(_name);
    _builder.append("_service/srv/");
    String _name_1 = ipt.getStimulus().getName();
    _builder.append(_name_1);
    _builder.append("_service.hpp\"");
    _builder.newLineIfNotEmpty();
    _builder.append("void call_service_");
    String _name_2 = ipt.getStimulus().getName();
    _builder.append(_name_2);
    _builder.append("(rclcpp::Client<");
    String _name_3 = ipt.getStimulus().getName();
    _builder.append(_name_3);
    _builder.append("_service::srv::");
    String _name_4 = ipt.getStimulus().getName();
    String _plus = (_name_4 + "_service");
    String _idlCompliantName = Utils.toIdlCompliantName(_plus);
    _builder.append(_idlCompliantName);
    _builder.append(">::SharedPtr& client);");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder.toString();
  }
}
