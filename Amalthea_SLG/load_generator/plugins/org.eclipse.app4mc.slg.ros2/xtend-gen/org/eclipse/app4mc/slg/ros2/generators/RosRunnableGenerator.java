/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.ros2.generators;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class RosRunnableGenerator {
  private RosRunnableGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String toCMake(final String libName, final List<String> srcFiles) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# ");
    _builder.append(libName);
    _builder.append(" ################################################################");
    _builder.newLineIfNotEmpty();
    _builder.append("####");
    _builder.newLine();
    _builder.append("add_library(");
    _builder.append(libName);
    _builder.append(" STATIC");
    _builder.newLineIfNotEmpty();
    {
      for(final String srcFile : srcFiles) {
        _builder.append("\t${CMAKE_CURRENT_LIST_DIR}/_src/");
        _builder.append(srcFile);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("target_include_directories(");
    _builder.append(libName);
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("PUBLIC  ${CMAKE_CURRENT_LIST_DIR}/_inc");
    _builder.newLine();
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("target_include_directories(");
    _builder.append(libName);
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("PUBLIC ${CMAKE_CURRENT_LIST_DIR}/../ticksUtils/_inc");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("${CMAKE_CURRENT_LIST_DIR}/../labels/_inc");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("${CMAKE_CURRENT_LIST_DIR}/../channelSendUtils/_inc");
    _builder.newLine();
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("target_link_libraries(RUNNABLES_LIB ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("PRIVATE LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS");
    _builder.newLine();
    _builder.append(")");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toH(final String call, final Set<String> includes) {
    String _xblockexpression = null;
    {
      final StringBuilder builder = new StringBuilder();
      final Consumer<String> _function = (String include) -> {
        builder.append((("#include \"" + include) + "\"\n"));
      };
      IterableExtensions.<String>sort(includes).forEach(_function);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("void ");
      _builder.append(call);
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      builder.append(_builder);
      _xblockexpression = builder.toString();
    }
    return _xblockexpression;
  }
  
  public static String toCpp(final org.eclipse.app4mc.amalthea.model.Runnable runnable, final Set<String> includes, final String call, final List<String> calls, final boolean measure_performance) {
    String _xblockexpression = null;
    {
      final StringBuilder builder = new StringBuilder();
      builder.append((("void " + call) + "{\n"));
      if (measure_performance) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("uint64_t event_list[] = {0x11, 0x13, 0x17}; //CPU CYCLES, MEM ACCESS, L2 Cache Refill");
        _builder.newLine();
        _builder.append("int total_events =  sizeof(event_list)/sizeof(event_list[0]);");
        _builder.newLine();
        _builder.append("int fd = instrument_start(0,event_list, total_events);");
        _builder.newLine();
        builder.append(_builder);
      }
      final Consumer<String> _function = (String c) -> {
        builder.append((("\t" + c) + ";\n"));
      };
      calls.forEach(_function);
      if (measure_performance) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("instrument_stop(fd, \"");
        String _name = runnable.getName();
        _builder_1.append(_name);
        _builder_1.append(".log\");");
        _builder_1.newLineIfNotEmpty();
        builder.append(_builder_1);
      }
      builder.append("}\n\n");
      _xblockexpression = builder.toString();
    }
    return _xblockexpression;
  }
}
