/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.ros2.generators;

import java.util.Properties;
import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.slg.ros2.artefacts.ROS2SLGTransformationDefinition;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.IntegerRange;

@SuppressWarnings("all")
public class RosChannelSendUtilsGenerator {
  private RosChannelSendUtilsGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String toCPPHead() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"channelSendUtils.hpp\"");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toCPP(final ChannelSend cs, final Properties properties) {
    String _xblockexpression = null;
    {
      final boolean genStatusOutput = Boolean.parseBoolean(properties.getProperty(ROS2SLGTransformationDefinition.PARAM_GENERATE_STATUS_OUTPUT));
      StringConcatenation _builder = new StringConcatenation();
      _builder.newLine();
      _builder.append("void publish_to_");
      String _name = cs.getData().getName();
      _builder.append(_name);
      _builder.append("(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("auto message = std_msgs::msg::String();");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("message.data =\"");
      String _name_1 = cs.getData().getName();
      _builder.append(_name_1, "\t");
      _builder.append("\";");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("//message.data =\"");
      {
        long _numberBytes = cs.getData().getSize().getNumberBytes();
        IntegerRange _upTo = new IntegerRange(1, ((int) _numberBytes));
        for(final Integer i : _upTo) {
          _builder.append("A");
        }
      }
      _builder.append("\";");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("publisher->publish(message);");
      _builder.newLine();
      {
        if (genStatusOutput) {
          _builder.append("\t");
          _builder.append("std::cout << \"ROS2: Publishing message \" << message.data << std::endl;");
          _builder.newLine();
        }
      }
      _builder.append("}");
      _builder.newLine();
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  public static String toHeader() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <string>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("#include \"rclcpp/rclcpp.hpp\"");
    _builder.newLine();
    _builder.append("#include \"std_msgs/msg/string.hpp\"");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toH(final ChannelSend cs) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("void publish_to_");
    String _name = cs.getData().getName();
    _builder.append(_name);
    _builder.append("(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder.toString();
  }
}
