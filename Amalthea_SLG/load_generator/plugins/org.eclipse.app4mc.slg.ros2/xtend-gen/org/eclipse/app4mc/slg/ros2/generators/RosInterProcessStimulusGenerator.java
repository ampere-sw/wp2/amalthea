/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.ros2.generators;

import org.eclipse.app4mc.slg.ros2.transformers.utils.Utils;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class RosInterProcessStimulusGenerator {
  private RosInterProcessStimulusGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String toSrvFile() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("int64  request");
    _builder.newLine();
    _builder.append("---");
    _builder.newLine();
    _builder.append("int64  response");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toPackageXML(final String moduleName) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\"?>");
    _builder.newLine();
    _builder.append("<?xml-model href=\"http://download.ros.org/schema/package_format3.xsd\" schematypens=\"http://www.w3.org/2001/XMLSchema\"?>");
    _builder.newLine();
    _builder.append("<package format=\"3\">");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<name>");
    _builder.append(moduleName, "\t");
    _builder.append("</name>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<version>0.0.0</version>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<description>TODO: Package description</description>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<maintainer email=\"app4mc-dev@eclipse.org\">app4mc-dev</maintainer>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<license>TODO: License declaration</license>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<buildtool_depend>ament_cmake</buildtool_depend>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<build_depend>rosidl_default_generators</build_depend>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<exec_depend>rosidl_default_runtime</exec_depend>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<member_of_group>rosidl_interface_packages</member_of_group>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<test_depend>ament_lint_auto</test_depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<test_depend>ament_lint_common</test_depend>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<export>");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<build_type>ament_cmake</build_type>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</export>");
    _builder.newLine();
    _builder.append("</package>");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toCMake(final String moduleName) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("cmake_minimum_required(VERSION 3.5)");
    _builder.newLine();
    _builder.append("project(");
    _builder.append(moduleName);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.newLine();
    _builder.append("if(NOT CMAKE_C_STANDARD)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("set(CMAKE_C_STANDARD 99)");
    _builder.newLine();
    _builder.append("endif()");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Default to C++14");
    _builder.newLine();
    _builder.append("if(NOT CMAKE_CXX_STANDARD)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("set(CMAKE_CXX_STANDARD 14)");
    _builder.newLine();
    _builder.append("endif()");
    _builder.newLine();
    _builder.append("if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES \"Clang\")");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("add_compile_options(-Wall -Wextra -Wpedantic)");
    _builder.newLine();
    _builder.append("endif()");
    _builder.newLine();
    _builder.append("find_package(ament_cmake REQUIRED)");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append("find_package(rosidl_default_generators REQUIRED)");
    _builder.newLine();
    _builder.append("rosidl_generate_interfaces(${PROJECT_NAME}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("\"srv/");
    String _idlCompliantName = Utils.toIdlCompliantName(moduleName);
    _builder.append(_idlCompliantName, "\t");
    _builder.append(".srv\"");
    _builder.newLineIfNotEmpty();
    _builder.append(")");
    _builder.newLine();
    _builder.append("ament_package()");
    _builder.newLine();
    return _builder.toString();
  }
}
