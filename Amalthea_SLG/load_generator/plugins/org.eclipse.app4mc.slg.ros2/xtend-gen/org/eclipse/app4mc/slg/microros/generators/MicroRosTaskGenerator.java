/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * 	 Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.microros.generators;

import java.math.BigInteger;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import org.eclipse.app4mc.amalthea.model.ChannelReceive;
import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.amalthea.model.EventStimulus;
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.RelativePeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.slg.ros2.artefacts.ROS2SLGTransformationDefinition;
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils;
import org.eclipse.app4mc.slg.ros2.transformers.utils.Utils;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class MicroRosTaskGenerator {
  private MicroRosTaskGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String getGlobalDeclaration(final Tag tag) {
    String _xblockexpression = null;
    {
      final String nodeName = RosModelUtils.getROSNodeName(tag);
      final HashSet<String> publishers = new HashSet<String>();
      final HashSet<String> messages = new HashSet<String>();
      List<Task> _taggedTasks = RosModelUtils.getTaggedTasks(tag);
      for (final Task task : _taggedTasks) {
        {
          Set<ChannelSend> _nestedChannelSends = RosModelUtils.getNestedChannelSends(task.getActivityGraph());
          for (final ChannelSend cs : _nestedChannelSends) {
            {
              final String data = cs.getData().getName().toLowerCase();
              publishers.add(data);
              messages.add(data);
            }
          }
          Set<ChannelReceive> _nestedChannelReceives = RosModelUtils.getNestedChannelReceives(task.getActivityGraph());
          for (final ChannelReceive cr : _nestedChannelReceives) {
            {
              final String data = cr.getData().getName().toLowerCase();
              messages.add(data);
            }
          }
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.newLine();
      {
        List<String> _sort = IterableExtensions.<String>sort(publishers);
        for(final String publisher : _sort) {
          _builder.append("rcl_publisher_t ");
          String _lowerCase = nodeName.toLowerCase();
          _builder.append(_lowerCase);
          _builder.append("Node_");
          _builder.append(publisher);
          _builder.append("_publisher;");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        List<String> _sort_1 = IterableExtensions.<String>sort(messages);
        for(final String message : _sort_1) {
          _builder.append("std_msgs__msg__String ");
          String _lowerCase_1 = nodeName.toLowerCase();
          _builder.append(_lowerCase_1);
          _builder.append("Node_");
          _builder.append(message);
          _builder.append("_msg;");
          _builder.newLineIfNotEmpty();
        }
      }
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  public static String getPeriodicTaskCalls(final Tag tag, final Collection<Stimulus> stimuli, final Collection<String> stepCalls) {
    StringConcatenation _builder = new StringConcatenation();
    {
      for(final Stimulus stimulus : stimuli) {
        {
          if (((stimulus instanceof PeriodicStimulus) || (stimulus instanceof RelativePeriodicStimulus))) {
            _builder.append("int debug_");
            String _name = stimulus.getName();
            _builder.append(_name);
            _builder.append("_counter = 0;");
            _builder.newLineIfNotEmpty();
            _builder.append("DeclareTask(");
            String _name_1 = stimulus.getName();
            _builder.append(_name_1);
            _builder.append("Task);");
            _builder.newLineIfNotEmpty();
            _builder.append("TASK(");
            String _name_2 = stimulus.getName();
            _builder.append(_name_2);
            _builder.append("Task)");
            _builder.newLineIfNotEmpty();
            _builder.append("{");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("if(");
            BigInteger _value = MicroRosTaskGenerator.getPeriod(stimulus).getValue();
            _builder.append(_value, "\t");
            _builder.append(" < ");
            Long _countPerSecond = MicroRosTaskGenerator.getCountPerSecond(MicroRosTaskGenerator.getPeriod(stimulus).getUnit());
            _builder.append(_countPerSecond, "\t");
            _builder.append(") {");
            _builder.newLineIfNotEmpty();
            _builder.append("\t\t");
            _builder.append("debug_");
            String _name_3 = stimulus.getName();
            _builder.append(_name_3, "\t\t");
            _builder.append("_counter += ");
            BigInteger _value_1 = MicroRosTaskGenerator.getPeriod(stimulus).getValue();
            _builder.append(_value_1, "\t\t");
            _builder.append(";");
            _builder.newLineIfNotEmpty();
            _builder.append("\t \t");
            _builder.append("if(debug_");
            String _name_4 = stimulus.getName();
            _builder.append(_name_4, "\t \t");
            _builder.append("_counter >= ");
            Long _countPerSecond_1 = MicroRosTaskGenerator.getCountPerSecond(MicroRosTaskGenerator.getPeriod(stimulus).getUnit());
            _builder.append(_countPerSecond_1, "\t \t");
            _builder.append(") {");
            _builder.newLineIfNotEmpty();
            _builder.append("\t \t\t");
            _builder.append("debug_printf(\"Task ");
            String _name_5 = stimulus.getName();
            _builder.append(_name_5, "\t \t\t");
            _builder.append(" (%d instances in 1 second)\\n\", ");
            Long _countPerSecond_2 = MicroRosTaskGenerator.getCountPerSecond(MicroRosTaskGenerator.getPeriod(stimulus).getUnit());
            _builder.append(_countPerSecond_2, "\t \t\t");
            _builder.append("/");
            BigInteger _value_2 = MicroRosTaskGenerator.getPeriod(stimulus).getValue();
            _builder.append(_value_2, "\t \t\t");
            _builder.append(");");
            _builder.newLineIfNotEmpty();
            _builder.append("\t \t\t");
            _builder.append("debug_");
            String _name_6 = stimulus.getName();
            _builder.append(_name_6, "\t \t\t");
            _builder.append("_counter = 0;");
            _builder.newLineIfNotEmpty();
            _builder.append("\t\t");
            _builder.append("}");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("} else {");
            _builder.newLine();
            _builder.append("\t\t");
            _builder.append("debug_printf(\"Task ");
            String _name_7 = stimulus.getName();
            _builder.append(_name_7, "\t\t");
            _builder.append("\\n\");");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("}");
            _builder.newLine();
            {
              for(final String call : stepCalls) {
                _builder.append("\t");
                _builder.append("\t");
                _builder.append(call, "\t");
                _builder.append(";");
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.append("}");
            _builder.newLine();
          }
        }
      }
    }
    _builder.newLine();
    return _builder.toString();
  }
  
  private static Time getPeriod(final Stimulus stimulus) {
    if ((stimulus instanceof PeriodicStimulus)) {
      return ((PeriodicStimulus)stimulus).getRecurrence();
    }
    if ((stimulus instanceof RelativePeriodicStimulus)) {
      return ((RelativePeriodicStimulus)stimulus).getNextOccurrence().getLowerBound();
    }
    return FactoryUtil.createTime();
  }
  
  private static Long getCountPerSecond(final TimeUnit unit) {
    long _switchResult = (long) 0;
    if (unit != null) {
      switch (unit) {
        case S:
          _switchResult = 1L;
          break;
        case MS:
          _switchResult = 1_000L;
          break;
        case US:
          _switchResult = 1_000_000L;
          break;
        case NS:
          _switchResult = 1_000_000_000L;
          break;
        case PS:
          _switchResult = 1_000_000_000_000L;
          break;
        default:
          _switchResult = 0L;
          break;
      }
    } else {
      _switchResult = 0L;
    }
    return Long.valueOf(_switchResult);
  }
  
  public static String getDeclaration(final Collection<Stimulus> stimuli, final Collection<String> publishers, final Collection<String> clientDeclarations) {
    StringConcatenation _builder = new StringConcatenation();
    {
      for(final Stimulus stimulus : stimuli) {
        {
          if ((stimulus instanceof PeriodicStimulus)) {
            _builder.append("rclcpp::TimerBase::SharedPtr ");
            String _name = ((PeriodicStimulus) stimulus).getName();
            _builder.append(_name);
            _builder.append("_timer_;");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          if ((stimulus instanceof EventStimulus)) {
            _builder.append("rclcpp::Subscription<std_msgs::msg::String>::SharedPtr ");
            String _name_1 = ((EventStimulus) stimulus).getName();
            _builder.append(_name_1);
            _builder.append("_subscription_;");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          if ((stimulus instanceof InterProcessStimulus)) {
            _builder.append("rclcpp::Service<");
            String _name_2 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_2);
            _builder.append("_service::srv::");
            String _name_3 = ((InterProcessStimulus) stimulus).getName();
            String _plus = (_name_3 + "_service");
            String _idlCompliantName = Utils.toIdlCompliantName(_plus);
            _builder.append(_idlCompliantName);
            _builder.append(">::SharedPtr ");
            String _name_4 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_4);
            _builder.append("_service;");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    {
      for(final String publisher : publishers) {
        _builder.append("rclcpp::Publisher<std_msgs::msg::String>::SharedPtr ");
        _builder.append(publisher);
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      for(final String decl : clientDeclarations) {
        _builder.append("\t");
        _builder.append(decl, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }
  
  public static String getInitialisation(final String nodeName, final Collection<Stimulus> stimuli, final Collection<String> publishers, final Collection<String> clientInits) {
    StringConcatenation _builder = new StringConcatenation();
    {
      for(final Stimulus stimulus : stimuli) {
        {
          if ((stimulus instanceof PeriodicStimulus)) {
            String _name = ((PeriodicStimulus) stimulus).getName();
            _builder.append(_name);
            _builder.append("_timer_ = this->create_wall_timer(");
            _builder.newLineIfNotEmpty();
            _builder.append("\t\t");
            BigInteger _value = ((PeriodicStimulus) stimulus).getRecurrence().getValue();
            _builder.append(_value, "\t\t");
            TimeUnit _unit = ((PeriodicStimulus) stimulus).getRecurrence().getUnit();
            _builder.append(_unit, "\t\t");
            _builder.append(", std::bind(&");
            _builder.append(nodeName, "\t\t");
            _builder.append("::");
            String _name_1 = ((PeriodicStimulus) stimulus).getName();
            _builder.append(_name_1, "\t\t");
            _builder.append("_timer_callback, this));");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          if ((stimulus instanceof EventStimulus)) {
            String _name_2 = ((EventStimulus) stimulus).getName();
            _builder.append(_name_2);
            _builder.append("_subscription_ = this->create_subscription<std_msgs::msg::String>(");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("\"");
            String _name_3 = ((EventStimulus) stimulus).getName();
            _builder.append(_name_3, "\t");
            _builder.append("\", 10, std::bind(&");
            _builder.append(nodeName, "\t");
            _builder.append("::");
            String _name_4 = ((EventStimulus) stimulus).getName();
            _builder.append(_name_4, "\t");
            _builder.append("_subscription_callback, this, _1));");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          if ((stimulus instanceof InterProcessStimulus)) {
            String _name_5 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_5);
            _builder.append("_service = this->create_service<");
            String _name_6 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_6);
            _builder.append("_service::srv::");
            String _name_7 = ((InterProcessStimulus) stimulus).getName();
            String _plus = (_name_7 + "_service");
            String _idlCompliantName = Utils.toIdlCompliantName(_plus);
            _builder.append(_idlCompliantName);
            _builder.append(">(\"");
            String _name_8 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_8);
            _builder.append("_service\", &");
            String _name_9 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_9);
            _builder.append("_service_callback);");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    {
      for(final String publisher : publishers) {
        _builder.append(publisher);
        _builder.append(" = this->create_publisher<std_msgs::msg::String>(\"");
        String _replace = publisher.replace("_publisher", "");
        _builder.append(_replace);
        _builder.append("\", 10);");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      for(final String init : clientInits) {
        _builder.append(init);
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }
  
  public static String getServiceCallback(final Collection<Stimulus> stimuli, final Collection<String> stepCalls) {
    StringConcatenation _builder = new StringConcatenation();
    {
      for(final Stimulus stimulus : stimuli) {
        {
          if ((stimulus instanceof InterProcessStimulus)) {
            _builder.append("void ");
            String _name = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name);
            _builder.append("_service_callback(const std::shared_ptr<");
            String _name_1 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_1);
            _builder.append("_service::srv::");
            String _name_2 = ((InterProcessStimulus) stimulus).getName();
            String _plus = (_name_2 + "_service");
            String _idlCompliantName = Utils.toIdlCompliantName(_plus);
            _builder.append(_idlCompliantName);
            _builder.append("::Request> request,");
            _builder.newLineIfNotEmpty();
            _builder.append("std::shared_ptr<");
            String _name_3 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_3);
            _builder.append("_service::srv::");
            String _name_4 = ((InterProcessStimulus) stimulus).getName();
            String _plus_1 = (_name_4 + "_service");
            String _idlCompliantName_1 = Utils.toIdlCompliantName(_plus_1);
            _builder.append(_idlCompliantName_1);
            _builder.append("::Response> response) {");
            _builder.newLineIfNotEmpty();
            {
              for(final String call : stepCalls) {
                _builder.append("\t");
                _builder.append(call, "\t");
                _builder.append(";");
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.append("}");
            _builder.newLine();
          }
        }
      }
    }
    return _builder.toString();
  }
  
  public static String getCallback(final Tag tag, final Properties properties, final Collection<Stimulus> stimuli, final Collection<String> stepCalls) {
    String _xblockexpression = null;
    {
      final String nodeName = RosModelUtils.getROSNodeName(tag);
      final HashSet<String> publishers = new HashSet<String>();
      List<Task> _taggedTasks = RosModelUtils.getTaggedTasks(tag);
      for (final Task task : _taggedTasks) {
        Set<ChannelSend> _nestedChannelSends = RosModelUtils.getNestedChannelSends(task.getActivityGraph());
        for (final ChannelSend cs : _nestedChannelSends) {
          {
            final String data = cs.getData().getName().toLowerCase();
            publishers.add(data);
          }
        }
      }
      final boolean genStatusOutput = Boolean.parseBoolean(properties.getProperty(ROS2SLGTransformationDefinition.PARAM_GENERATE_STATUS_OUTPUT));
      final boolean genDebugOutput = Boolean.parseBoolean(properties.getProperty(ROS2SLGTransformationDefinition.PARAM_GENERATE_DEBUG_OUTPUT));
      StringConcatenation _builder = new StringConcatenation();
      _builder.newLine();
      {
        for(final Stimulus stimulus : stimuli) {
          {
            if ((stimulus instanceof EventStimulus)) {
              _builder.append("void ");
              String _lowerCase = ((EventStimulus) stimulus).getName().toLowerCase();
              _builder.append(_lowerCase);
              _builder.append("_subscription_callback(const void * msgin)");
              _builder.newLineIfNotEmpty();
              _builder.append("{");
              _builder.newLine();
              _builder.append("\t");
              _builder.append("const std_msgs__msg__String * msg = (const std_msgs__msg__String *)msgin;\t");
              _builder.newLine();
              _builder.append("\t");
              _builder.append("if (msg == NULL) {");
              _builder.newLine();
              _builder.append("\t\t");
              _builder.append("// no calls");
              _builder.newLine();
              {
                if (genStatusOutput) {
                  _builder.append("\t\t");
                  _builder.append("printf(\"Callback: msg NULL\\n\");");
                  _builder.newLine();
                }
              }
              _builder.append("\t");
              _builder.append("} else {");
              _builder.newLine();
              {
                if (genStatusOutput) {
                  _builder.append("\t\t");
                  _builder.append("printf(\"Callback: Received message on \'");
                  String _lowerCase_1 = ((EventStimulus) stimulus).getName().toLowerCase();
                  _builder.append(_lowerCase_1, "\t\t");
                  _builder.append("\' topic\\n\");");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t\t");
                  _builder.append("printf(\"Callback: I heard \'%s\'\\n\", msg->data.data);");
                  _builder.newLine();
                }
              }
              {
                for(final String call : stepCalls) {
                  _builder.append("\t\t");
                  _builder.append(call, "\t\t");
                  _builder.append(";");
                  _builder.newLineIfNotEmpty();
                }
              }
              _builder.append("\t");
              _builder.append("}");
              _builder.newLine();
              _builder.newLine();
              _builder.append("}");
              _builder.newLine();
            }
          }
        }
      }
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  public static Collection<String> getHeaders(final Collection<String> includes) {
    final Function1<String, String> _function = (String i) -> {
      return (("#include \"" + i) + "\"\n");
    };
    return IterableExtensions.<String>toList(IterableExtensions.<String, String>map(includes, _function));
  }
}
