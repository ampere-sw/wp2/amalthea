/**
 * Copyright (c) 2020-2022 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.microros.generators;

import java.util.Properties;
import org.eclipse.app4mc.amalthea.model.Channel;
import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.slg.ros2.artefacts.ROS2SLGTransformationDefinition;
import org.eclipse.app4mc.slg.ros2.transformers.utils.Code;
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class MicroRosChannelSendUtilsGenerator {
  private MicroRosChannelSendUtilsGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String toCPPHead() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"channelSendUtils.h\"");
    _builder.newLine();
    _builder.append("#include \"output_console.h\"");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toCPP(final ChannelSend cs, final Tag tag, final Properties properties) {
    String _xblockexpression = null;
    {
      Channel _data = cs.getData();
      String _name = null;
      if (_data!=null) {
        _name=_data.getName();
      }
      final String param = _name;
      final String rosNodeName = RosModelUtils.getROSNodeName(tag);
      final boolean genDebugOutput = Boolean.parseBoolean(properties.getProperty(ROS2SLGTransformationDefinition.PARAM_GENERATE_DEBUG_OUTPUT));
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(Code.IFDEF_EXTERN_C_BEGIN);
      _builder.newLineIfNotEmpty();
      _builder.append("extern rcl_publisher_t ");
      String _lowerCase = rosNodeName.toLowerCase();
      _builder.append(_lowerCase);
      _builder.append("Node_");
      _builder.append(param);
      _builder.append("_publisher;");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("void ");
      _builder.append(rosNodeName);
      _builder.append("_publish_to_");
      _builder.append(param);
      _builder.append("(std_msgs__msg__String ");
      _builder.append(rosNodeName);
      _builder.append("Node_");
      _builder.append(param);
      _builder.append("_msg){");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("rcl_ret_t rc;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("snprintf(");
      _builder.append(rosNodeName, "\t");
      _builder.append("Node_");
      _builder.append(param, "\t");
      _builder.append("_msg.data.data, ");
      _builder.append(rosNodeName, "\t");
      _builder.append("Node_");
      _builder.append(param, "\t");
      _builder.append("_msg.data.capacity, \"AAAAAAAAAAAAAAAAAAA\");");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append(rosNodeName, "\t");
      _builder.append("Node_");
      _builder.append(param, "\t");
      _builder.append("_msg.data.size = strlen(");
      _builder.append(rosNodeName, "\t");
      _builder.append("Node_");
      _builder.append(param, "\t");
      _builder.append("_msg.data.data);");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("rc = rcl_publish(&");
      String _lowerCase_1 = rosNodeName.toLowerCase();
      _builder.append(_lowerCase_1, "\t");
      _builder.append("Node_");
      _builder.append(param, "\t");
      _builder.append("_publisher, &");
      _builder.append(rosNodeName, "\t");
      _builder.append("Node_");
      _builder.append(param, "\t");
      _builder.append("_msg, NULL);");
      _builder.newLineIfNotEmpty();
      {
        if (genDebugOutput) {
          _builder.newLine();
          _builder.append("\t");
          _builder.append("debug_printf(\"  Published message on \'");
          _builder.append(param, "\t");
          _builder.append("\' topic\\n\");");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("debug_printf(\"  Published message \'%s\'\\n\", ");
          _builder.append(rosNodeName, "\t");
          _builder.append("Node_");
          _builder.append(param, "\t");
          _builder.append("_msg.data.data);");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("}");
      _builder.newLine();
      _builder.append(Code.IFDEF_EXTERN_C_END);
      _builder.newLineIfNotEmpty();
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  public static String toHeader() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <stdio.h>");
    _builder.newLine();
    _builder.append("#include <std_msgs/msg/string.h>");
    _builder.newLine();
    _builder.append("#include <rclc/executor.h>");
    _builder.newLine();
    _builder.append("#include <rclc/rclc.h>");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toH(final ChannelSend cs, final Tag tag) {
    String _xblockexpression = null;
    {
      Channel _data = cs.getData();
      String _name = null;
      if (_data!=null) {
        _name=_data.getName();
      }
      final String param = _name;
      final String rosNodeName = RosModelUtils.getROSNodeName(tag);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(Code.IFDEF_EXTERN_C_BEGIN);
      _builder.newLineIfNotEmpty();
      _builder.append("void ");
      _builder.append(rosNodeName);
      _builder.append("_publish_to_");
      _builder.append(param);
      _builder.append("(std_msgs__msg__String ");
      _builder.append(rosNodeName);
      _builder.append("Node_");
      _builder.append(param);
      _builder.append("_msg);");
      _builder.newLineIfNotEmpty();
      _builder.append(Code.IFDEF_EXTERN_C_END);
      _builder.newLineIfNotEmpty();
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
}
