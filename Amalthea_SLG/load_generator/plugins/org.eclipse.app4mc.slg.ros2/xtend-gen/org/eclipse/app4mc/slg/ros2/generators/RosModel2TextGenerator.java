/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.ros2.generators;

import com.google.inject.Singleton;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.app4mc.slg.config.CodehookType;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.util.ConfigModelUtils;
import org.eclipse.app4mc.slg.ros2.transformers.RosModel2TextTransformer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@Singleton
@SuppressWarnings("all")
public class RosModel2TextGenerator {
  public static String toMain(final Set<String> nodes, final Map<String, Boolean> hasOpenMP, final Map<String, List<RosModel2TextTransformer.SchedulingParameters>> schedParams) {
    StringConcatenation _builder = new StringConcatenation();
    {
      List<String> _sort = IterableExtensions.<String>sort(nodes);
      for(final String node : _sort) {
        _builder.append("#include \"");
        _builder.append(node);
        _builder.append(".hpp\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("int main(int argc, char *argv[])");
    _builder.newLine();
    _builder.append("{");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("std::vector<std::thread> threads;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("setvbuf(stdout, NULL, _IONBF, BUFSIZ);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("rclcpp::init(argc, argv);");
    _builder.newLine();
    String _executors = RosModel2TextGenerator.getExecutors(IterableExtensions.<String>sort(nodes), hasOpenMP, schedParams);
    _builder.append(_executors);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//WAITING OF THREADS");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("for (auto& th : threads)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("th.join();");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("rclcpp::shutdown();");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return 0;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  private static String getExecutors(final List<String> nodes, final Map<String, Boolean> hasOpenMP, final Map<String, List<RosModel2TextTransformer.SchedulingParameters>> schedParams) {
    String _xblockexpression = null;
    {
      final StringBuilder sb = new StringBuilder();
      for (int i = 1; (i <= nodes.size()); i++) {
        {
          final String node = nodes.get((i - 1));
          final StringBuilder schedString = new StringBuilder();
          final List<RosModel2TextTransformer.SchedulingParameters> threadParams = schedParams.get(node);
          for (int j = 0; (j < threadParams.size()); j++) {
            schedString.append((((((((("thread_sched(" + Integer.valueOf(j)) + ",") + Integer.valueOf(threadParams.get(j).period)) + ",") + Integer.valueOf(threadParams.get(j).deadline)) + ",") + Integer.valueOf(threadParams.get(j).runtime)) + ") "));
          }
          Boolean _get = hasOpenMP.get(node);
          if ((_get).booleanValue()) {
            StringConcatenation _builder = new StringConcatenation();
            _builder.newLine();
            _builder.append("//EXECUTOR ");
            _builder.append(i);
            _builder.newLineIfNotEmpty();
            _builder.newLine();
            _builder.append("rclcpp::executors::SingleThreadedExecutor executor");
            _builder.append(i);
            _builder.append(";");
            _builder.newLineIfNotEmpty();
            _builder.append("auto node");
            _builder.append(i);
            _builder.append(" = std::make_shared<");
            _builder.append(node);
            _builder.append(">();");
            _builder.newLineIfNotEmpty();
            _builder.append("executor");
            _builder.append(i);
            _builder.append(".add_node(node");
            _builder.append(i);
            _builder.append(");");
            _builder.newLineIfNotEmpty();
            _builder.newLine();
            _builder.append("auto spin_executor");
            _builder.append(i);
            _builder.append(" = [&executor");
            _builder.append(i);
            _builder.append("]()");
            _builder.newLineIfNotEmpty();
            _builder.append("{");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("#pragma omp parallel ");
            sb.append(_builder);
            sb.append(schedString);
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.newLine();
            _builder_1.append("\t");
            _builder_1.append("#pragma omp single");
            _builder_1.newLine();
            _builder_1.append("\t");
            _builder_1.append("executor");
            _builder_1.append(i, "\t");
            _builder_1.append(".spin();");
            _builder_1.newLineIfNotEmpty();
            _builder_1.append("};");
            _builder_1.newLine();
            _builder_1.append("threads.emplace_back(std::thread(spin_executor");
            _builder_1.append(i);
            _builder_1.append("));");
            _builder_1.newLineIfNotEmpty();
            sb.append(_builder_1);
          } else {
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.newLine();
            _builder_2.append("//EXECUTOR ");
            _builder_2.append(i);
            _builder_2.newLineIfNotEmpty();
            _builder_2.newLine();
            _builder_2.append("rclcpp::executors::SingleThreadedExecutor executor");
            _builder_2.append(i);
            _builder_2.append(";");
            _builder_2.newLineIfNotEmpty();
            _builder_2.append("auto node");
            _builder_2.append(i);
            _builder_2.append(" = std::make_shared<");
            _builder_2.append(node);
            _builder_2.append(">();");
            _builder_2.newLineIfNotEmpty();
            _builder_2.append("executor");
            _builder_2.append(i);
            _builder_2.append(".add_node(node");
            _builder_2.append(i);
            _builder_2.append(");");
            _builder_2.newLineIfNotEmpty();
            _builder_2.newLine();
            _builder_2.append("auto spin_executor");
            _builder_2.append(i);
            _builder_2.append(" = [&executor");
            _builder_2.append(i);
            _builder_2.append("]()");
            _builder_2.newLineIfNotEmpty();
            _builder_2.append("{");
            _builder_2.newLine();
            _builder_2.append("\t");
            _builder_2.append("executor");
            _builder_2.append(i, "\t");
            _builder_2.append(".spin();");
            _builder_2.newLineIfNotEmpty();
            _builder_2.append("};");
            _builder_2.newLine();
            _builder_2.append("threads.emplace_back(std::thread(spin_executor");
            _builder_2.append(i);
            _builder_2.append("));");
            _builder_2.newLineIfNotEmpty();
            sb.append(_builder_2);
          }
        }
      }
      _xblockexpression = sb.toString();
    }
    return _xblockexpression;
  }
  
  public static String toGPUTicks() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#define RATIO 0.1");
    _builder.newLine();
    _builder.append("__global__ void kernel(long long nclocks){");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("long long start=clock64();");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("while (clock64() < start+nclocks);");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("void executeGPUTicks(long long average,long long lowerBound, long long upperBound) {");
    _builder.newLine();
    _builder.append("   ");
    _builder.append("kernel<<<1,1>>>(average * RATIO);");
    _builder.newLine();
    _builder.append("   ");
    _builder.append("cudaDeviceSynchronize();");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("void executeGPUTicksConstant(long long ticks) {");
    _builder.newLine();
    _builder.append("   ");
    _builder.append("kernel<<<1,1>>>(ticks * RATIO);");
    _builder.newLine();
    _builder.append("   ");
    _builder.append("cudaDeviceSynchronize();");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public static String toLaunchFile(final Set<String> nodes) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from launch import LaunchDescription");
    _builder.newLine();
    _builder.append("from launch_ros.actions import Node");
    _builder.newLine();
    _builder.newLine();
    _builder.append("def generate_launch_description():");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("return LaunchDescription([");
    _builder.newLine();
    {
      List<String> _sort = IterableExtensions.<String>sort(nodes);
      for(final String node : _sort) {
        _builder.append("\t\t");
        _builder.append("Node(");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("\t");
        _builder.append("package=\'amalthea_ros2_model\',");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("\t");
        _builder.append("node_namespace=\'\',");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("\t");
        _builder.append("node_executable=\'");
        _builder.append(node, "\t\t\t");
        _builder.append("\',");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("),");
        _builder.newLine();
      }
    }
    _builder.append("\t");
    _builder.append("])");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toBuildScript(final Set<String> services) {
    StringConcatenation _builder = new StringConcatenation();
    {
      List<String> _sort = IterableExtensions.<String>sort(services);
      for(final String service : _sort) {
        _builder.append("#building service ");
        _builder.append(service);
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        _builder.append("cd services/");
        _builder.append(service);
        _builder.newLineIfNotEmpty();
        _builder.append("colcon build");
        _builder.newLine();
        _builder.append(". install/setup.bash");
        _builder.newLine();
        _builder.append("cd ../..");
        _builder.newLine();
        _builder.newLine();
      }
    }
    _builder.append("colcon build");
    _builder.newLine();
    _builder.append(". install/setup.bash");
    _builder.newLine();
    return _builder.toString();
  }
  
  public static String toPackageXml(final Set<String> services) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\"?>");
    _builder.newLine();
    _builder.append("<?xml-model href=\"http://download.ros.org/schema/package_format3.xsd\" schematypens=\"http://www.w3.org/2001/XMLSchema\"?>");
    _builder.newLine();
    _builder.append("<package format=\"3\">");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<name>amalthea_ros2_model</name>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<version>0.0.0</version>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<description>TODO: Package description</description>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<maintainer email=\"app4mc-dev@eclipse.org\">app4mc-dev</maintainer>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<license>TODO: License declaration</license>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<buildtool_depend>ament_cmake</buildtool_depend>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<depend>rclcpp</depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<depend>std_msgs</depend>");
    _builder.newLine();
    _builder.newLine();
    {
      List<String> _sort = IterableExtensions.<String>sort(services);
      for(final String service : _sort) {
        _builder.append("\t");
        _builder.append("<depend>");
        _builder.append(service, "\t");
        _builder.append("</depend>");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<test_depend>ament_lint_auto</test_depend>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<test_depend>ament_lint_common</test_depend>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<export>");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<build_type>ament_cmake</build_type>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</export>");
    _builder.newLine();
    _builder.append("</package>");
    _builder.newLine();
    return _builder.toString();
  }
  
  private static String getHeaderFileDirectories(final ConfigModel configModel, final String workingDirectory) {
    final StringBuilder sb = new StringBuilder();
    final List<String> runnableHeaders = ConfigModelUtils.getHeaderFilesDirectories(configModel, CodehookType.RUNNABLE, workingDirectory, null);
    final List<String> tasksHeader = ConfigModelUtils.getHeaderFilesDirectories(configModel, CodehookType.TASK, workingDirectory, null);
    final ArrayList<String> allHeaderDirs = new ArrayList<String>();
    allHeaderDirs.addAll(runnableHeaders);
    allHeaderDirs.addAll(tasksHeader);
    for (final String hfileDir : allHeaderDirs) {
      {
        sb.append(((("include_directories(" + "\"") + hfileDir) + "\")"));
        sb.append(System.getProperty("line.separator"));
      }
    }
    return sb.toString().trim();
  }
  
  private static String getLibraries(final ConfigModel configModel, final String workingDirectory) {
    String _xblockexpression = null;
    {
      final ArrayList<String> listLibsNames = new ArrayList<String>();
      final ArrayList<String> listpackageNames = new ArrayList<String>();
      final ArrayList<String> listKeywordLibs = new ArrayList<String>();
      final StringBuilder sb = new StringBuilder();
      List<String> _linkedLibraryInstructions = ConfigModelUtils.getLinkedLibraryInstructions(configModel, workingDirectory);
      for (final String libPath : _linkedLibraryInstructions) {
        {
          final String fileName = Paths.get(libPath).getFileName().toString();
          boolean _endsWith = fileName.endsWith(".a");
          if (_endsWith) {
            final String name = FilenameUtils.removeExtension(fileName);
            listLibsNames.add(name);
            sb.append((("add_library(" + name) + " STATIC IMPORTED)\n"));
            sb.append((("set_property(TARGET " + name) + " PROPERTY\n"));
            sb.append((("\t\tIMPORTED_LOCATION \"" + libPath) + "\")"));
          } else {
            boolean _endsWith_1 = fileName.endsWith(".so");
            if (_endsWith_1) {
              final String name_1 = FilenameUtils.removeExtension(fileName);
              listLibsNames.add(name_1);
              sb.append((("add_library(" + name_1) + " SHARED IMPORTED)\n"));
              sb.append((("set_property(TARGET " + name_1) + " PROPERTY\n"));
              sb.append((("\t\tIMPORTED_LOCATION \"" + libPath) + "\")"));
            } else {
              listKeywordLibs.add(libPath);
            }
          }
        }
      }
      List<String> _packageNames = ConfigModelUtils.getPackageNames(configModel, workingDirectory);
      for (final String packageName : _packageNames) {
        {
          sb.append((("find_package(" + packageName) + ")\n"));
          listpackageNames.add(packageName);
        }
      }
      _xblockexpression = sb.toString();
    }
    return _xblockexpression;
  }
  
  public static String toCmake(final RosModel2TextTransformer.TargetInfo targetInfo, final boolean externalCode, final ConfigModel configModel, final String workingDirectory, final boolean hasGlobalOpenMP) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# CMakeLists for Nodes");
    _builder.newLine();
    _builder.append("cmake_minimum_required(VERSION 3.5)");
    _builder.newLine();
    _builder.append("project(amalthea_ros2_model)");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Default to C++14");
    _builder.newLine();
    _builder.append("if(NOT CMAKE_CXX_STANDARD)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("set(CMAKE_CXX_STANDARD 14)");
    _builder.newLine();
    _builder.append("endif()");
    _builder.newLine();
    _builder.newLine();
    _builder.append("if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES \"Clang\")");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("add_compile_options(-Wall -Wextra -Wpedantic)");
    _builder.newLine();
    _builder.append("endif()");
    _builder.newLine();
    _builder.newLine();
    {
      if (externalCode) {
        _builder.append("# Adding the external code\'s include directories");
        _builder.newLine();
        String _headerFileDirectories = RosModel2TextGenerator.getHeaderFileDirectories(configModel, workingDirectory);
        _builder.append(_headerFileDirectories);
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        _builder.append("# Adding the libraries created out of the external code");
        _builder.newLine();
        String _libraries = RosModel2TextGenerator.getLibraries(configModel, workingDirectory);
        _builder.append(_libraries);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("find_package(ament_cmake REQUIRED)");
    _builder.newLine();
    _builder.append("find_package(rclcpp REQUIRED)");
    _builder.newLine();
    _builder.append("find_package(std_msgs REQUIRED)");
    _builder.newLine();
    _builder.append("find_package(CUDA REQUIRED)");
    _builder.newLine();
    {
      if (hasGlobalOpenMP) {
        _builder.append("find_package(OpenMP)");
        _builder.newLine();
        _builder.newLine();
        _builder.append("if(OPENMP_FOUND)");
        _builder.newLine();
        _builder.append("    ");
        _builder.append("set (CMAKE_C_FLAGS \"${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS} -fopenmp-taskgraph -static-tdg\")");
        _builder.newLine();
        _builder.append("    ");
        _builder.append("set (CMAKE_CXX_FLAGS \"${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS} -fopenmp-taskgraph -static-tdg\")");
        _builder.newLine();
        _builder.append("    ");
        _builder.append("set (CMAKE_EXE_LINKER_FLAGS \"${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}\")");
        _builder.newLine();
        _builder.append("endif()");
        _builder.newLine();
      }
    }
    {
      List<String> _sort = IterableExtensions.<String>sort(targetInfo.getServices());
      for(final String service : _sort) {
        _builder.append("find_package(");
        _builder.append(service);
        _builder.append(" REQUIRED)");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      boolean _hasChannelSend = targetInfo.hasChannelSend();
      if (_hasChannelSend) {
        _builder.newLine();
        _builder.append("add_library(CHANNELSEND_UTILS  STATIC");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("synthetic_gen/channelSendUtils/_src/channelSendUtils.cpp");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
        _builder.append("ament_target_dependencies(CHANNELSEND_UTILS rclcpp std_msgs ");
        String _join = IterableExtensions.join(IterableExtensions.<String>sort(targetInfo.getServices()), " ");
        _builder.append(_join);
        _builder.append(")");
        _builder.newLineIfNotEmpty();
        _builder.append("target_include_directories(CHANNELSEND_UTILS");
        _builder.newLine();
        _builder.append("    ");
        _builder.append("PUBLIC synthetic_gen/channelSendUtils/_inc/");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
      }
    }
    {
      boolean _hasInterprocess = targetInfo.hasInterprocess();
      if (_hasInterprocess) {
        _builder.newLine();
        _builder.append("add_library(INTERPROCESSTRIGGER_UTIL STATIC");
        _builder.newLine();
        _builder.append("\t  ");
        _builder.append("synthetic_gen/interProcessTriggerUtils/_src/interProcessTriggerUtils.cpp");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
        _builder.append("ament_target_dependencies(INTERPROCESSTRIGGER_UTIL rclcpp std_msgs ");
        String _join_1 = IterableExtensions.join(IterableExtensions.<String>sort(targetInfo.getServices()), " ");
        _builder.append(_join_1);
        _builder.append(")");
        _builder.newLineIfNotEmpty();
        _builder.append("target_include_directories(INTERPROCESSTRIGGER_UTIL");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("PUBLIC synthetic_gen/interProcessTriggerUtils/_inc");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
      }
    }
    {
      boolean _hasPerformanceMeasurement = targetInfo.hasPerformanceMeasurement();
      if (_hasPerformanceMeasurement) {
        _builder.newLine();
        _builder.append("add_library(AML_LIB  STATIC");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("utils/aml/_src/aml.cpp");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
        _builder.append("target_include_directories(AML_LIB");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("PUBLIC utils/aml/_inc/");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
      }
    }
    _builder.newLine();
    {
      boolean _hasLabel = targetInfo.hasLabel();
      if (_hasLabel) {
        _builder.append("add_subdirectory (synthetic_gen/labels)");
        _builder.newLine();
      }
    }
    {
      boolean _hasTicks = targetInfo.hasTicks();
      if (_hasTicks) {
        _builder.append("add_subdirectory (synthetic_gen/ticksUtils)");
        _builder.newLine();
      }
    }
    _builder.newLine();
    _builder.append("# RUNNABLES_LIB ################################################################");
    _builder.newLine();
    _builder.newLine();
    _builder.append("####");
    _builder.newLine();
    _builder.append("add_library(RUNNABLES_LIB STATIC");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("synthetic_gen/runnables/_src/runnables.cpp");
    _builder.newLine();
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("target_include_directories(RUNNABLES_LIB");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("PUBLIC synthetic_gen/runnables/_inc");
    _builder.newLine();
    _builder.append(")\t");
    _builder.newLine();
    _builder.newLine();
    _builder.append("target_include_directories(RUNNABLES_LIB");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("PUBLIC ");
    _builder.newLine();
    String _otherDirectories = RosModel2TextGenerator.getOtherDirectories(targetInfo);
    _builder.append(_otherDirectories);
    _builder.newLineIfNotEmpty();
    _builder.append(")");
    _builder.newLine();
    _builder.append("target_link_libraries(RUNNABLES_LIB");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("PRIVATE ");
    String _otherLibraries = RosModel2TextGenerator.getOtherLibraries(targetInfo);
    _builder.append(_otherLibraries, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append(")");
    _builder.newLine();
    {
      List<String> _sort_1 = IterableExtensions.<String>sort(targetInfo.getNodes());
      for(final String node : _sort_1) {
        _builder.newLine();
        _builder.newLine();
        _builder.append("# ********** Node ");
        _builder.append(node);
        _builder.append(" **********");
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        _builder.append("add_library(");
        _builder.append(node);
        _builder.append("_LIB STATIC");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append(node, "\t");
        _builder.append("/_inc/");
        _builder.append(node, "\t");
        _builder.append(".hpp");
        _builder.newLineIfNotEmpty();
        _builder.append(")");
        _builder.newLine();
        _builder.newLine();
        _builder.append("target_include_directories(");
        _builder.append(node);
        _builder.append("_LIB");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("PUBLIC");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("synthetic_gen/runnables/_inc");
        _builder.newLine();
        String _otherDirectories_1 = RosModel2TextGenerator.getOtherDirectories(targetInfo);
        _builder.append(_otherDirectories_1);
        _builder.newLineIfNotEmpty();
        _builder.append(")");
        _builder.newLine();
        _builder.newLine();
        _builder.append("target_link_libraries(");
        _builder.append(node);
        _builder.append("_LIB ");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("PRIVATE  RUNNABLES_LIB ");
        String _otherLibraries_1 = RosModel2TextGenerator.getOtherLibraries(targetInfo);
        _builder.append(_otherLibraries_1, "\t");
        _builder.newLineIfNotEmpty();
        _builder.append(")");
        _builder.newLine();
      }
    }
    _builder.newLine();
    {
      if (hasGlobalOpenMP) {
        _builder.append("add_library(MAIN_OBJ OBJECT main.cpp)");
        _builder.newLine();
        _builder.newLine();
        _builder.append("add_custom_command(");
        _builder.newLine();
        _builder.append("    ");
        _builder.append("COMMAND echo \"\"");
        _builder.newLine();
        _builder.append("    ");
        _builder.append("OUTPUT main_tdg.cpp");
        _builder.newLine();
        _builder.append("    ");
        _builder.append("DEPENDS main.cpp");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
        _builder.append("target_include_directories(MAIN_OBJ");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("PUBLIC");
        _builder.newLine();
        {
          List<String> _sort_2 = IterableExtensions.<String>sort(targetInfo.getNodes());
          for(final String node_1 : _sort_2) {
            _builder.append("\t");
            _builder.append(node_1, "\t");
            _builder.append("/_inc");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append(")");
        _builder.newLine();
        _builder.newLine();
        _builder.append("target_link_libraries(MAIN_OBJ ");
        final Function1<String, String> _function = (String n) -> {
          return (n + "_LIB");
        };
        String _join_2 = IterableExtensions.join(ListExtensions.<String, String>map(IterableExtensions.<String>sort(targetInfo.getNodes()), _function), " ");
        _builder.append(_join_2);
        _builder.append(")");
        _builder.newLineIfNotEmpty();
        _builder.append("ament_target_dependencies(MAIN_OBJ rclcpp std_msgs ");
        String _join_3 = IterableExtensions.join(IterableExtensions.<String>sort(targetInfo.getServices()), " ");
        _builder.append(_join_3);
        _builder.append(")");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("SET_SOURCE_FILES_PROPERTIES(GPUTicks.cu PROPERTIES LANGUAGE CXX)");
    _builder.newLine();
    _builder.append("add_executable(MAIN_BIN main.cpp GPUTicks.cu)");
    _builder.newLine();
    _builder.newLine();
    _builder.append("target_include_directories(MAIN_BIN");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("PUBLIC");
    _builder.newLine();
    {
      List<String> _sort_3 = IterableExtensions.<String>sort(targetInfo.getNodes());
      for(final String node_2 : _sort_3) {
        _builder.append("\t");
        _builder.append(node_2, "\t");
        _builder.append("/_inc");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("target_link_libraries(MAIN_BIN ${CUDA_LIBRARIES} ");
    final Function1<String, String> _function_1 = (String n) -> {
      return (n + "_LIB");
    };
    String _join_4 = IterableExtensions.join(ListExtensions.<String, String>map(IterableExtensions.<String>sort(targetInfo.getNodes()), _function_1), " ");
    _builder.append(_join_4);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("ament_target_dependencies(MAIN_BIN rclcpp std_msgs ");
    String _join_5 = IterableExtensions.join(IterableExtensions.<String>sort(targetInfo.getServices()), " ");
    _builder.append(_join_5);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    {
      if (hasGlobalOpenMP) {
        _builder.append("target_sources(MAIN_BIN PRIVATE main_tdg.cpp)");
        _builder.newLine();
        _builder.append("add_dependencies(MAIN_BIN MAIN_OBJ)");
        _builder.newLine();
      }
    }
    _builder.newLine();
    _builder.append("install(TARGETS");
    _builder.newLine();
    {
      List<String> _sort_4 = IterableExtensions.<String>sort(targetInfo.getNodes());
      for(final String node_3 : _sort_4) {
        _builder.append("\t");
        _builder.append("# ");
        _builder.append(node_3, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("MAIN_BIN");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("DESTINATION lib/${PROJECT_NAME}");
    _builder.newLine();
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("ament_package()");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
  
  private static String getOtherDirectories(final RosModel2TextTransformer.TargetInfo targetInfo) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _hasTicks = targetInfo.hasTicks();
      if (_hasTicks) {
        _builder.append("\tsynthetic_gen/ticksUtils/_inc");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      boolean _hasLabel = targetInfo.hasLabel();
      if (_hasLabel) {
        _builder.append("\tsynthetic_gen/labels/_inc");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      boolean _hasChannelSend = targetInfo.hasChannelSend();
      if (_hasChannelSend) {
        _builder.append("\tsynthetic_gen/channelSendUtils/_inc");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      boolean _hasInterprocess = targetInfo.hasInterprocess();
      if (_hasInterprocess) {
        _builder.append("\tsynthetic_gen/interProcessTriggerUtils/_inc");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      boolean _hasPerformanceMeasurement = targetInfo.hasPerformanceMeasurement();
      if (_hasPerformanceMeasurement) {
        _builder.append("\tutils/aml/_inc");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }
  
  private static String getOtherLibraries(final RosModel2TextTransformer.TargetInfo targetInfo) {
    String _xblockexpression = null;
    {
      final ArrayList<Object> libs = new ArrayList<Object>();
      boolean _hasPerformanceMeasurement = targetInfo.hasPerformanceMeasurement();
      if (_hasPerformanceMeasurement) {
        libs.add("AML_LIB");
      }
      boolean _hasLabel = targetInfo.hasLabel();
      if (_hasLabel) {
        libs.add("LABELS_LIB");
      }
      boolean _hasTicks = targetInfo.hasTicks();
      if (_hasTicks) {
        libs.add("TICKS_UTILS");
      }
      boolean _hasChannelSend = targetInfo.hasChannelSend();
      if (_hasChannelSend) {
        libs.add("CHANNELSEND_UTILS");
      }
      boolean _hasInterprocess = targetInfo.hasInterprocess();
      if (_hasInterprocess) {
        libs.add("INTERPROCESSTRIGGER_UTIL");
      }
      _xblockexpression = IterableExtensions.join(libs, " ");
    }
    return _xblockexpression;
  }
}
