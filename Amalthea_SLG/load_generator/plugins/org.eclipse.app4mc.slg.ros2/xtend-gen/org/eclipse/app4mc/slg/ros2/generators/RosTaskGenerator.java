/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.slg.ros2.generators;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Properties;
import org.eclipse.app4mc.amalthea.model.EventStimulus;
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.RelativePeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.slg.ros2.artefacts.ROS2SLGTransformationDefinition;
import org.eclipse.app4mc.slg.ros2.transformers.utils.Utils;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class RosTaskGenerator {
  private RosTaskGenerator() {
    throw new IllegalStateException("Utility class");
  }
  
  public static String getDeclaration(final Collection<Stimulus> stimuli, final Collection<String> publishers, final Collection<String> clientDeclarations) {
    StringConcatenation _builder = new StringConcatenation();
    {
      for(final Stimulus stimulus : stimuli) {
        {
          if ((stimulus instanceof PeriodicStimulus)) {
            _builder.append("rclcpp::TimerBase::SharedPtr timer_");
            String _name = ((PeriodicStimulus) stimulus).getName();
            _builder.append(_name);
            _builder.append("_;");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          if ((stimulus instanceof EventStimulus)) {
            _builder.append("rclcpp::Subscription<std_msgs::msg::String>::SharedPtr ");
            String _name_1 = ((EventStimulus) stimulus).getName();
            _builder.append(_name_1);
            _builder.append("_subscription_;");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          if ((stimulus instanceof InterProcessStimulus)) {
            _builder.append("rclcpp::Service<");
            String _name_2 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_2);
            _builder.append("_service::srv::");
            String _name_3 = ((InterProcessStimulus) stimulus).getName();
            String _plus = (_name_3 + "_service");
            String _idlCompliantName = Utils.toIdlCompliantName(_plus);
            _builder.append(_idlCompliantName);
            _builder.append(">::SharedPtr ");
            String _name_4 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_4);
            _builder.append("_service;");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    {
      for(final String publisher : publishers) {
        _builder.append("rclcpp::Publisher<std_msgs::msg::String>::SharedPtr ");
        _builder.append(publisher);
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      for(final String decl : clientDeclarations) {
        _builder.append(decl);
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }
  
  public static String getInitialisation(final String nodeName, final Collection<Stimulus> stimuli, final Collection<String> publishers, final Collection<String> clientInits) {
    StringConcatenation _builder = new StringConcatenation();
    {
      for(final Stimulus stimulus : stimuli) {
        {
          if ((stimulus instanceof PeriodicStimulus)) {
            _builder.append("timer_");
            String _name = ((PeriodicStimulus) stimulus).getName();
            _builder.append(_name);
            _builder.append("_ = this->create_wall_timer(");
            _builder.newLineIfNotEmpty();
            _builder.append("\t\t");
            BigInteger _value = ((PeriodicStimulus) stimulus).getRecurrence().getValue();
            _builder.append(_value, "\t\t");
            TimeUnit _unit = ((PeriodicStimulus) stimulus).getRecurrence().getUnit();
            _builder.append(_unit, "\t\t");
            _builder.append(", std::bind(&");
            _builder.append(nodeName, "\t\t");
            _builder.append("::timer_");
            String _name_1 = ((PeriodicStimulus) stimulus).getName();
            _builder.append(_name_1, "\t\t");
            _builder.append("_callback, this));");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          if ((stimulus instanceof EventStimulus)) {
            String _name_2 = ((EventStimulus) stimulus).getName();
            _builder.append(_name_2);
            _builder.append("_subscription_ = this->create_subscription<std_msgs::msg::String>(");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("\"");
            String _lowerCase = ((EventStimulus) stimulus).getName().toLowerCase();
            _builder.append(_lowerCase, "\t");
            _builder.append("\", rclcpp::QoS(10), std::bind(&");
            _builder.append(nodeName, "\t");
            _builder.append("::");
            String _name_3 = ((EventStimulus) stimulus).getName();
            _builder.append(_name_3, "\t");
            _builder.append("_subscription_callback, this, _1));");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          if ((stimulus instanceof InterProcessStimulus)) {
            String _name_4 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_4);
            _builder.append("_service = this->create_service<");
            String _name_5 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_5);
            _builder.append("_service::srv::");
            String _name_6 = ((InterProcessStimulus) stimulus).getName();
            String _plus = (_name_6 + "_service");
            String _idlCompliantName = Utils.toIdlCompliantName(_plus);
            _builder.append(_idlCompliantName);
            _builder.append(">(");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("\"");
            String _name_7 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_7, "\t");
            _builder.append("_service\", ");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("std::bind(&");
            _builder.append(nodeName, "\t");
            _builder.append("::");
            String _name_8 = ((InterProcessStimulus) stimulus).getName();
            _builder.append(_name_8, "\t");
            _builder.append("_service_callback, this, std::placeholders::_1, std::placeholders::_2));");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    {
      for(final String publisher : publishers) {
        _builder.append(publisher);
        _builder.append(" = this->create_publisher<std_msgs::msg::String>(\"");
        String _lowerCase_1 = publisher.replace("_publisher", "").toLowerCase();
        _builder.append(_lowerCase_1);
        _builder.append("\", 10);");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      for(final String init : clientInits) {
        _builder.append(init);
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }
  
  public static String getServiceCallback(final Collection<Stimulus> stimuli, final Collection<String> stepCalls, final Properties properties) {
    String _xblockexpression = null;
    {
      final boolean genStatusOutput = Boolean.parseBoolean(properties.getProperty(ROS2SLGTransformationDefinition.PARAM_GENERATE_STATUS_OUTPUT));
      StringConcatenation _builder = new StringConcatenation();
      {
        for(final Stimulus stimulus : stimuli) {
          {
            if ((stimulus instanceof InterProcessStimulus)) {
              _builder.append("void ");
              String _name = ((InterProcessStimulus) stimulus).getName();
              _builder.append(_name);
              _builder.append("_service_callback(const std::shared_ptr<");
              String _name_1 = ((InterProcessStimulus) stimulus).getName();
              _builder.append(_name_1);
              _builder.append("_service::srv::");
              String _name_2 = ((InterProcessStimulus) stimulus).getName();
              String _plus = (_name_2 + "_service");
              String _idlCompliantName = Utils.toIdlCompliantName(_plus);
              _builder.append(_idlCompliantName);
              _builder.append("::Request> request,");
              _builder.newLineIfNotEmpty();
              _builder.append("\t");
              _builder.append("std::shared_ptr<");
              String _name_3 = ((InterProcessStimulus) stimulus).getName();
              _builder.append(_name_3, "\t");
              _builder.append("_service::srv::");
              String _name_4 = ((InterProcessStimulus) stimulus).getName();
              String _plus_1 = (_name_4 + "_service");
              String _idlCompliantName_1 = Utils.toIdlCompliantName(_plus_1);
              _builder.append(_idlCompliantName_1, "\t");
              _builder.append("::Response> response) {");
              _builder.newLineIfNotEmpty();
              _builder.append("\t\t");
              _builder.append("(void)request;");
              _builder.newLine();
              _builder.append("\t\t");
              _builder.append("(void)response;");
              _builder.newLine();
              {
                if (genStatusOutput) {
                  _builder.append("#ifdef CONSOLE_ENABLED");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("std::cout << \"Starting ");
                  String _name_5 = ((InterProcessStimulus) stimulus).getName();
                  _builder.append(_name_5, "\t");
                  _builder.append("_service_callback\" << std::endl;");
                  _builder.newLineIfNotEmpty();
                  _builder.append("#endif");
                  _builder.newLine();
                }
              }
              {
                for(final String call : stepCalls) {
                  _builder.append("\t");
                  _builder.append(call, "\t");
                  _builder.append(";");
                  _builder.newLineIfNotEmpty();
                }
              }
              _builder.append("}");
              _builder.newLine();
            }
          }
          _builder.newLine();
        }
      }
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  public static String getCallback(final Collection<Stimulus> stimuli, final Collection<String> stepCalls, final Properties properties) {
    String _xblockexpression = null;
    {
      final boolean genStatusOutput = Boolean.parseBoolean(properties.getProperty(ROS2SLGTransformationDefinition.PARAM_GENERATE_STATUS_OUTPUT));
      StringConcatenation _builder = new StringConcatenation();
      {
        for(final Stimulus stimulus : stimuli) {
          {
            if (((stimulus instanceof PeriodicStimulus) || (stimulus instanceof RelativePeriodicStimulus))) {
              _builder.append("void timer_");
              String _name = stimulus.getName();
              _builder.append(_name);
              _builder.append("_callback() {");
              _builder.newLineIfNotEmpty();
              {
                if (genStatusOutput) {
                  _builder.append("#ifdef CONSOLE_ENABLED");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("std::cout << \"Timer_");
                  String _name_1 = stimulus.getName();
                  _builder.append(_name_1, "\t");
                  _builder.append("_callback (");
                  BigInteger _value = RosTaskGenerator.getPeriod(stimulus).getValue();
                  _builder.append(_value, "\t");
                  TimeUnit _unit = RosTaskGenerator.getPeriod(stimulus).getUnit();
                  _builder.append(_unit, "\t");
                  _builder.append(")\" << std::endl;");
                  _builder.newLineIfNotEmpty();
                  _builder.append("#endif");
                  _builder.newLine();
                }
              }
              {
                for(final String call : stepCalls) {
                  _builder.append("\t");
                  _builder.append(call, "\t");
                  _builder.append(";");
                  _builder.newLineIfNotEmpty();
                }
              }
              _builder.append("}");
              _builder.newLine();
            }
          }
          {
            if ((stimulus instanceof EventStimulus)) {
              _builder.append("void ");
              String _name_2 = ((EventStimulus) stimulus).getName();
              _builder.append(_name_2);
              _builder.append("_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {");
              _builder.newLineIfNotEmpty();
              {
                if (genStatusOutput) {
                  _builder.append("#ifdef CONSOLE_ENABLED");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("std::cout << \"ROS2: subscription callback \" << msg->data << std::endl ;");
                  _builder.newLine();
                  _builder.append("#endif");
                  _builder.newLine();
                }
              }
              {
                for(final String call_1 : stepCalls) {
                  _builder.append("\t");
                  _builder.append(call_1, "\t");
                  _builder.append(";");
                  _builder.newLineIfNotEmpty();
                }
              }
              _builder.append("}");
              _builder.newLine();
            }
          }
          _builder.newLine();
        }
      }
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  private static Time getPeriod(final Stimulus stimulus) {
    if ((stimulus instanceof PeriodicStimulus)) {
      return ((PeriodicStimulus)stimulus).getRecurrence();
    }
    if ((stimulus instanceof RelativePeriodicStimulus)) {
      return ((RelativePeriodicStimulus)stimulus).getNextOccurrence().getLowerBound();
    }
    return FactoryUtil.createTime();
  }
  
  public static Collection<String> getHeaders(final Collection<String> includes) {
    final Function1<String, String> _function = (String include) -> {
      return (("#include \"" + include) + "\"\n");
    };
    return IterableExtensions.<String>toList(IterableExtensions.<String, String>map(includes, _function));
  }
}
