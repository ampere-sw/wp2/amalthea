/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.microros.transformers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Group;
import org.eclipse.app4mc.amalthea.model.ITaggable;
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.microros.generators.MicroRosLabelGenerator;
import org.eclipse.app4mc.slg.ros2.transformers.MicroRosBaseTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.stimuli.RosInterProcessStimulusTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.sw.RosRunnableCache;
import org.eclipse.app4mc.slg.ros2.transformers.sw.RosRunnableCache.RunnableStore;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class MicroRosTaskTransformer extends MicroRosBaseTransformer {

	@Inject private MicroRosLabelTransformer labelTransformer;
	@Inject private MicroRosRunnableTransformer runnableTransformer;
	@Inject private RosInterProcessStimulusTransformer interProcessStimulusTransformer;
	@Inject private MicroRosTaskCache taskCache;
	@Inject private RosRunnableCache runnableCache;

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final Task task, final Tag tag) {
		final List<Object> key = List.of(task);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(task, tag);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, task, tag);
		}

		return tu;
	}

	// ---------------------------------------------------

	protected SLGTranslationUnit createTranslationUnit(Task task, final Tag tag) { 

		if ((task == null)) {
			return new SLGTranslationUnit("UNSPECIFIED TASK");
		} else {
			String basePath = "";
			String moduleName = task.getName();
			String call = "" + task.getName() + "()"; // unused
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	protected void doTransform(final SLGTranslationUnit tu, final Task task, final Tag tag) {
		genFiles(tu, task, tag);
	}

	protected void genFiles(SLGTranslationUnit tu, Task task, final Tag tag) {

		final Set<String> includes = new LinkedHashSet<>();
		final List<String> initCalls = new LinkedList<>();
		final List<String> stepCalls = new LinkedList<>();
		final List<Stimulus> stimuli = new ArrayList<>();
		final List<String> publishers = new ArrayList<>();
		final List<String> clientDeclarations = new ArrayList<>();
		final List<String> clientInits = new ArrayList<>();
		final Map<Runnable, Entry<String, String>> runnable_DataMap = new HashMap<>();

		if (task != null && task.getActivityGraph() != null) {
			for (ActivityGraphItem item : task.getActivityGraph().getItems()) {

				if ((item instanceof RunnableCall)) {
					final RunnableCall runnableCall = (RunnableCall) item;
					final Runnable runnable = runnableCall.getRunnable();

					SLGTranslationUnit runnableTU = runnableTransformer.transform(runnable, tag);
					RunnableStore runnableStore = runnableCache.getStore(runnableTU);

					includes.add(getIncFile(runnableTU));

					if (hasTagNamed(runnableCall, "initialize")) {
						initCalls.add(runnableStore.getNodeCall(runnableCall));
					} else {
						stepCalls.add(runnableStore.getNodeCall(runnableCall));
						//System.out.println("\n Stepcall: " + runnableStore.getNodeCall(runnableCall)+ "\n");
					}

					// TODO: Make set
					publishers.addAll(runnableStore.getPublishers());
					clientDeclarations.addAll(runnableStore.getClientDeclarations());
					clientInits.addAll(runnableStore.getClientInits());
					// TODO: add terminate function, if requested

				} else if (item instanceof Group) {
					final Group group = ((Group) item);
					for (ActivityGraphItem groupitem : group.getItems()) {
						if ((groupitem instanceof RunnableCall)) {
							final RunnableCall runnableCall = (RunnableCall) groupitem;
							final Runnable runnable = runnableCall.getRunnable();

							SLGTranslationUnit runnableTU = runnableTransformer.transform(runnable, tag);
							RunnableStore runnableStore = runnableCache.getStore(runnableTU);

							includes.add(getIncFile(runnableTU));

							if (hasTagNamed(runnableCall, "initialize")) {
								initCalls.add(runnableStore.getNodeCall(runnableCall));
							} else {
								stepCalls.add(runnableStore.getNodeCall(runnableCall));
							}
							// TODO: add terminate function, if requested
						}
					}
				}
			}
		}

		// labels must be initialized before usage, generated labels provide this method

		for (SLGTranslationUnit labelTU : labelTransformer.getCache().values()) {
			includes.add(getIncFile(labelTU));
			initCalls.add(MicroRosLabelGenerator.initCall(labelTU.getCall()));
		}

//		rosLabelTransformer.getCache().forEach(
//				(BiConsumer<ArrayList<?>, LabelTranslationUnit>) (ArrayList<?> label, LabelTranslationUnit tu) -> {
//
//				});

		// add header for srv file in case of an interprocessstimulus
		// create .srv file for the messages to be translated

		if (task != null) {
			for (Stimulus stimulus : task.getStimuli()) {
				if (stimulus instanceof InterProcessStimulus) {
					String name = stimulus.getName();
					includes.add(name + "_service/srv/" + name + "_service.hpp");

					interProcessStimulusTransformer.transform(((InterProcessStimulus) stimulus));
				}
			}

			stimuli.addAll(task.getStimuli());
		}
 
		// store characteristic values in task cache
		taskCache.storeValues(tu, task, stimuli, includes, initCalls, stepCalls, publishers, clientDeclarations, clientInits, runnable_DataMap);
	}

	private boolean hasTagNamed(ITaggable element, String name) {
		for (Tag tag : element.getTags()) {
			if (tag.getName().equals(name))
				return true;
		}
		return false;
	}

}
