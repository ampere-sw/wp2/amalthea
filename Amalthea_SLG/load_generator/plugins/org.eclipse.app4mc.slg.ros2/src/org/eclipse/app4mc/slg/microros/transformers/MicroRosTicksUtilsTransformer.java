/**
 ********************************************************************************
 * Copyright (c) 2022-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.microros.transformers;

import static org.eclipse.app4mc.slg.commons.m2t.transformers.SLGBaseSettings.OTHER_TYPE;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.slg.commons.m2t.CustomObjectsStore;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.PlatformArchitecture;
import org.eclipse.app4mc.slg.microros.generators.MicroRosTicksUtilsGenerator;
import org.eclipse.app4mc.slg.ros2.transformers.MicroRosBaseTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.utils.Code;
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.ecore.EClass;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class MicroRosTicksUtilsTransformer extends MicroRosBaseTransformer {

	@Inject private OutputBuffer outputBuffer;
	@Inject private CustomObjectsStore customObjsStore;

	public SLGTranslationUnit transform(final IDiscreteValueDeviation value, final Tag tag) {
		if (value == null)
			return new SLGTranslationUnit("UNSPECIFIED TICKS");

		return transformClass(value.eClass(), tag); // hash according to class not instance
	}

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transformClass(final EClass eClass, final Tag tag) {
		final String target = RosModelUtils.getROSTargetName(tag);
		final List<Object> key = List.of(eClass, target);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(eClass, target);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, eClass);
		}
		return tu;
	}

	// ---------------------------------------------------

	protected SLGTranslationUnit createTranslationUnit(final EClass eClass, final String target) {
		if (eClass == null) {
			return new SLGTranslationUnit("UNSPECIFIED TICKS");
		} else {
			String basePath = target + "/synthetic_gen";
			String moduleName = "ticksUtils";
			String call = "burnTicks(<params>)";
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	protected void doTransform(final SLGTranslationUnit tu, final EClass eClass) {
		genFiles(tu, eClass);
	}

	protected void genFiles(SLGTranslationUnit tu, final EClass eClass) {
		if (isIncFileEmpty(tu)) {
			incAppend(tu, "#pragma once \n");
			if (Code.ERIKA) incAppend(tu, Code.IFDEF_EXTERN_C_BEGIN);
			toH(tu);
			incAppend(tu, MicroRosTicksUtilsGenerator.generateTicksDeclaration(eClass));
			if (Code.ERIKA) incAppend(tu, Code.IFDEF_EXTERN_C_END);
		}

		if (isSrcFileEmpty(tu)) {
			srcAppend(tu, "#include \"" + getIncFile(tu) + "\"\n");
			srcAppend(tu, "#include \"output_console.h\"\n");

			if (Code.ERIKA) srcAppend(tu, Code.IFDEF_EXTERN_C_BEGIN);
			toCPP(tu);
			srcAppend(tu, MicroRosTicksUtilsGenerator.generateTicks(eClass));
			if (Code.ERIKA) srcAppend(tu, Code.IFDEF_EXTERN_C_END);
		}
	}

	protected void toCPP(SLGTranslationUnit tu) {

		final ConfigModel configModel = customObjsStore.<ConfigModel>getInstance(ConfigModel.class);

		PlatformArchitecture platformArchitectureType = configModel.getPlatformArchitectureType();

		if (platformArchitectureType != null) {

			srcAppend(tu, "#define __" + platformArchitectureType.getName() + "__\n");
		}
		final String ticksCodeSnippet = configModel.getCustomTickImpl().getValue();
		final boolean ticksCodeEnabled = configModel.getCustomTickImpl().isEnable();

		final String burnTicksBody = ticksCodeEnabled ? ticksCodeSnippet : MicroRosTicksUtilsGenerator.burnTicksDefault();

		srcAppend(tu, MicroRosTicksUtilsGenerator.burnTicks(burnTicksBody));
		srcAppend(tu, MicroRosTicksUtilsGenerator.burnTicksStatistics(configModel));
	}

	protected void toH(SLGTranslationUnit tu) {

		incAppend(tu, MicroRosTicksUtilsGenerator.burnTicksDeclaration());
		incAppend(tu, MicroRosTicksUtilsGenerator.burnTicksStatisticsDeclaration());
	}


	public void createCMake(String target) {
		// Building rule: basePath + moduleName + "CMakeLists.txt"
		String makeFilePath = target + "/synthetic_gen/ticksUtils/CMakeLists.txt";
		outputBuffer.appendTo(OTHER_TYPE, makeFilePath, MicroRosTicksUtilsGenerator.toCMake(getSrcFiles()));
	}

}
