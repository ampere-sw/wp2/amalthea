/**
 * Copyright (c) 2022-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.ros2.transformers.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.Channel;
import org.eclipse.app4mc.amalthea.model.ChannelAccess;
import org.eclipse.app4mc.amalthea.model.ChannelReceive;
import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.amalthea.model.IActivityGraphItemContainer;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.amalthea.model.util.SoftwareUtil;
import org.eclipse.emf.common.util.EList;

public class RosModelUtils {

	// Suppress default constructor
	private RosModelUtils() {
		throw new IllegalStateException("Utility class");
	}

	public static final List<String> ROS2_TYPES = List.of("ROS2");
	public static final List<String> MICROROS_TYPES = List.of("MicroROS", "ErikaMicroROS");
	public static final List<String> ROS_TYPES = List.of("ROS2", "MicroROS", "ErikaMicroROS");
	public static final List<String> ROS_TAG_TYPES = List.of("ROS:Type", "ROS:Type+Target", "ROS:Type+Node", "ROS:Type+Target+Node");

	private static final String REGEX_NAME_SEPARATOR = "\\s+-\\s+";
	private static final String REGEX_TYPE_SEPARATOR = "\\+";


	public static List<Task> getTaggedTasks(final Tag tag) {
		return tag.getTaggedObjects().stream()
				.filter(Task.class::isInstance)
				.map(Task.class::cast)
				.collect(Collectors.toList());
	}

	public static Set<Channel> getPublishers(final Tag tag) {
		return getChannels(tag, ChannelSend.class);
	}

	public static Set<Channel> getSubscribers(final Tag tag) {
		return getChannels(tag, ChannelReceive.class);
	}
	
	private static <T extends ChannelAccess> Set<Channel> getChannels(final Tag tag, final Class<T> targetClass) {
		Set<T> accessItems = new HashSet<>();
		
		// get corresponding tasks
		List<Task> tasks = getTaggedTasks(tag);
		
		// collect nested ChannelReceive items of all tasks
		for (Task task : tasks) {
			addNestedGraphItems(task.getActivityGraph(), targetClass, accessItems);
		}
		
		return accessItems.stream()
				.map(ChannelAccess::getData)
				.filter(Objects::nonNull)
				.collect(Collectors.toSet());
	}

	public static Set<LabelAccess> getNestedLabelAccesses(final IActivityGraphItemContainer container) {
				return getNestedGraphItems(container, LabelAccess.class);
	}

	public static Set<ChannelSend> getNestedChannelSends(final IActivityGraphItemContainer container) {
		return getNestedGraphItems(container, ChannelSend.class);
	}

	public static Set<ChannelReceive> getNestedChannelReceives(final IActivityGraphItemContainer container) {
		return getNestedGraphItems(container, ChannelReceive.class);
	}

	private static <T extends ActivityGraphItem> Set<T> getNestedGraphItems(final IActivityGraphItemContainer container, final Class<T> targetClass) {
		Set<T> result = new HashSet<>();
		addNestedGraphItems(container, targetClass, result);
		return result;
	}

	private static <T extends ActivityGraphItem> void addNestedGraphItems(final IActivityGraphItemContainer container, final Class<T> targetClass, final Set<T> itemList) {
		if (container == null)
			return;

		// collect items in activity graph
		itemList.addAll(SoftwareUtil.collectActivityGraphItems(container, null, targetClass));

		// recursively collect items for all runnables that are called in the activity graph
		EList<RunnableCall> calls = SoftwareUtil.collectActivityGraphItems(container, null, RunnableCall.class);
		for (RunnableCall call : calls) {
			if (call.getRunnable() != null) {
				addNestedGraphItems(call.getRunnable().getActivityGraph(), targetClass, itemList);
			}
		}
	}

	/**
	 * Gets all Tag elements with ROS types that are referred by a task
	 * 
	 * @param model
	 * @return selected tags
	 */
	public static List<Tag> getROSTags(final Amalthea model) {
		return ModelUtil.getOrCreateCommonElements(model).getTags().stream()
				.filter(RosModelUtils::isROSTag)
				.filter(t -> t.getTaggedObjects().stream().anyMatch(Task.class::isInstance))
				.collect(Collectors.toList());
	}

	/**
	 * Gets all ROS tags of a task
	 * 
	 * @param task
	 * @return selected tags
	 */
	public static List<Tag> getROSTags(final Task task) {
		return task.getTags().stream()
				.filter(RosModelUtils::isROSTag)
				.collect(Collectors.toList());
	}

	public static boolean isROSTag(Tag tag) {
		if (tag == null)
			return false;

		String name = clean(tag.getName()); // characters between square brackets are ignored
		String type = tag.getTagType();

		return name != null && type != null
				&& ROS_TAG_TYPES.contains(type)
				&& ROS_TYPES.stream().anyMatch(name::startsWith)
				&& type.split(REGEX_TYPE_SEPARATOR).length == name.split(REGEX_NAME_SEPARATOR).length;
	}

	public static boolean isTypeMicroROS(Tag tag) {
		String rosTypeName = getROSTypeName(tag);
		return rosTypeName != null && MICROROS_TYPES.contains(rosTypeName);
	}

	public static boolean checkAndExtendROSTags(final Amalthea model) {
		List<Task> rosTasks = ModelUtil.getOrCreateSwModel(model).getTasks().stream()
				.filter(t -> t.getTags().stream().anyMatch(RosModelUtils::isROSTag))
				.collect(Collectors.toList());

		for (Task task : rosTasks) {
			// Check: only one ROS tag per task is allowed
			List<Tag> tags = task.getTags();
			if (tags.size() > 1 && tags.stream().filter(RosModelUtils::isROSTag).count() > 1L) {
				System.out.println("***** Model is not consistent: multiple ROS tags per task detected *****");
				return false;
			}

			// Check: task name must be set
			if (task.getName() == null || task.getName().trim().isEmpty()) {
				System.out.println("***** Model is not consistent: unnamed task detected *****");
				return false;
			}
		}

		Map<String, String> targetTypeMap = new HashMap<>();
		for (Tag tag : getROSTags(model)) {
			String target = getROSTargetName(tag);
			String type = getROSTypeName(tag);
			String oldType = targetTypeMap.put(target, type);

			// Check: target must always have the same ROS type
			if (oldType != null && !oldType.equals(type)) {
				System.out.println("***** Model is not consistent: different types assigned to target *****");
				return false;
			}
		}

		// Modify model: tasks with implicit node names get a specific new tag
		for (Task task : rosTasks) {
			Tag rosTag = getROSTags(task).get(0);
			if (getROSNodeName(rosTag) == null) {
				String rosType = getROSTypeName(rosTag);
				String rosTarget = getROSTargetName(rosTag);
				String rosNode = "Node_" + task.getName();

				String tagType = ROS_TAG_TYPES.get(3);
				String tagName = rosType + " - " + rosTarget + " - " + rosNode;

				Tag newTag = AmaltheaFactory.eINSTANCE.createTag();
				newTag.setTagType(tagType);
				newTag.setName(tagName);
				ModelUtil.getOrCreateCommonElements(model).getTags().add(newTag);

				task.getTags().remove(rosTag);
				task.getTags().add(newTag);
			}
		}
		
		return true;
	}

	public static String getROSTargetName(Task task) {
		Tag rosTag = task.getTags().stream().filter(RosModelUtils::isROSTag).findAny().orElse(null);
		return getROSTargetName(rosTag);
	}

	public static String getROSTargetName(Tag tag) {
		if (!isROSTag(tag))
			return null;

		String name = tag.getName();
		String type = tag.getTagType();
			
		if (type.contains("Target")) {
			return extract(name, type, "Target");
		} else {
			return extract(name, type, "Type");
		}
	}

	public static String getROSNodeName(Task task) {
		Tag rosTag = task.getTags().stream().filter(RosModelUtils::isROSTag).findAny().orElse(null);
		String nodeName = getROSNodeName(rosTag);

		if (nodeName != null)
			return nodeName;

		// default case
		if (task.getName() != null)
			return task.getName().toLowerCase() + "_node";

		return null;
	}

	public static String getROSNodeName(Tag tag) {
		if (!isROSTag(tag))
			return null;

		String name = tag.getName();
		String type = tag.getTagType();
			
		if (type.contains("Node")) {
			return extract(name, type, "Node");
		} else {
			return null;
		}
	}

	public static String getROSTypeName(Tag tag) {
		if (!isROSTag(tag))
			return null;

		return clean(tag.getName()).split(REGEX_NAME_SEPARATOR)[0].trim();
	}

	private static String extract(String name, String type, String selector) {
		// ensure that name and type were extracted from a valid ROS tag

		String[] typeParts = type.substring(4).split(REGEX_TYPE_SEPARATOR);
		String[] nameParts = clean(name).split(REGEX_NAME_SEPARATOR);

		for (int i = 0; i < typeParts.length; i++) {
			if (typeParts[i].contains(selector)) {
				return nameParts[i].replace("(" + selector + ")", "").trim();
			}
		}
		return null;
	}

	private static String clean(String name) {
		// remove:
		// - additional info in square brackets
		// - leading and trailing whitespaces

		return name.replaceAll("\\[[^\\[\\]]*\\]", "").trim();
	}

}
