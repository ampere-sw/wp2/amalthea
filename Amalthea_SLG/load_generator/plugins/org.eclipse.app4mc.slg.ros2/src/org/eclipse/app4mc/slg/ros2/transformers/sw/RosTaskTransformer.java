/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers.sw;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Group;
import org.eclipse.app4mc.amalthea.model.ILocalModeValueSource;
import org.eclipse.app4mc.amalthea.model.ITaggable;
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.LocalModeLabelAssignment;
import org.eclipse.app4mc.amalthea.model.ModeLiteralConst;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.slg.commons.m2t.AmaltheaModelUtils;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.commons.m2t.transformers.sw.RunnableTransformer;
import org.eclipse.app4mc.slg.ros2.generators.RosLabelGenerator;
import org.eclipse.app4mc.slg.ros2.transformers.RosBaseTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.stimuli.RosInterProcessStimulusTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.sw.RosRunnableCache.RunnableStore;
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EStructuralFeature;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class RosTaskTransformer extends RosBaseTransformer {

	@Inject private RosLabelTransformer rosLabelTransformer;
	@Inject private RosRunnableTransformer rosRunnableTransformer;
	@Inject private RosRunnableCache rosRunnableCache;
	@Inject private RosInterProcessStimulusTransformer rosInterProcessStimulusTransformer;
	@Inject private RosTaskCache rosTaskCache;
	@Inject private RunnableTransformer runnableTransformer;
	@Inject private Properties properties;

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final Task task, final Tag tag) {
		final List<Object> key = List.of(task);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(task, tag);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, task, tag);
		}

		return tu;
	}

	// ---------------------------------------------------

	protected SLGTranslationUnit createTranslationUnit(final Task task, final Tag tag) {
		if ((task == null)) {
			return new SLGTranslationUnit("UNSPECIFIED TASK");
		} else {
			String basePath = "";
			String moduleName = task.getName();
			String call = "" + task.getName() + "()"; // unused
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	protected void doTransform(final SLGTranslationUnit tu, final Task task, final Tag tag) {
		genFiles(tu, task, tag);
	}
	
	class TaskDeps {
		public List<Label> inputLabels;
		public List<Label> outputLabels;
		public List<LabelAccess> allLabels;

		public TaskDeps() {
			inputLabels = new ArrayList<Label>();
			outputLabels = new ArrayList<Label>();
			allLabels = new ArrayList<LabelAccess>();
		}
	};

	private void addRunnable(Runnable runnable, RunnableCall runnableCall, List<TaskDeps> TaskDepsList) {
		TaskDeps NewTask = new TaskDeps();
		for (ActivityGraphItem runitem : runnable.getActivityGraph().getItems()) {
			if (runitem instanceof LabelAccess) {
				LabelAccess la = (LabelAccess) runitem;
				Label label = la.getData();
				NewTask.allLabels.add(la);
				if (la.getAccess() == LabelAccessEnum.READ) {
					NewTask.inputLabels.add(label);
				}
				if (la.getAccess() == LabelAccessEnum.WRITE) {
					NewTask.outputLabels.add(label);
				}
			}
		}
		TaskDepsList.add(NewTask);
	}
	
	private boolean isValidInput(Label label, int taskID, List<TaskDeps> TaskDepsList) {
		for (int j = 0; j < TaskDepsList.get(taskID).allLabels.size(); j++) {
			LabelAccess la = TaskDepsList.get(taskID).allLabels.get(j);
			Label labelFound = TaskDepsList.get(taskID).allLabels.get(j).getData();
			if (labelFound.equals(label)) {
				if (la.getAccess() == LabelAccessEnum.WRITE)
					return false;
				else
					break;
			}
		}
		for (int i = 0; i < TaskDepsList.size(); i++) {
			if (i == taskID)
				continue;
			for (int j = 0; j < TaskDepsList.get(i).outputLabels.size(); j++) {
				if (TaskDepsList.get(i).outputLabels.get(j).equals(label))
					return true;
			}
		}
		return false;
	}
	
	private boolean isValidOutput(Label label, int taskID, List<TaskDeps> TaskDepsList) {
		for (int i = 0; i < TaskDepsList.size(); i++) {
			if (i == taskID)
				continue;
			for (int j = 0; j < TaskDepsList.get(i).inputLabels.size(); j++) {
				if (TaskDepsList.get(i).inputLabels.get(j).equals(label))
					return true;
			}
			for (int j = 0; j < TaskDepsList.get(i).outputLabels.size(); j++) {
				if (TaskDepsList.get(i).outputLabels.get(j).equals(label))
					return true;
			}
		}
		return false;
	}

	private String processRunnable(SLGTranslationUnit tu, Runnable runnable, RunnableCall runnableCall, Set<Label> externLabels, int id,
			List<TaskDeps> TaskDepsList, boolean hasOpenMP) {
		String statements = "";
		String localMode = null;
		int numReplications = 0;

		EList<LocalModeLabelAssignment> context = (runnableCall).getContext();

		for (LocalModeLabelAssignment localModeLabelAssignment : context) {

			ILocalModeValueSource valueSource = localModeLabelAssignment.getValueSource();

			if (valueSource != null && valueSource instanceof ModeLiteralConst) {

				localMode = ((ModeLiteralConst) valueSource).getValue().getName();
			}
		}

		final boolean hasReplication = Boolean.parseBoolean(this.properties.getProperty("Replication", "false"));
		if (hasReplication) {
			EList<Tag> tags = runnable.getTags();
			for (Tag runnableTag : tags) {
				if (runnableTag.getName().equals("SIL4") || runnableTag.getName().equals("ASILB")) {
					hasOpenMP = true;
					localMode = "OpenMP";
					numReplications = 2;
					break;
				}
			}
		}

		if ((localMode != null && localMode.equals("GPU")) || hasOpenMP == true) {

			final Set<LabelAccess> labelAccesses = RosModelUtils.getNestedLabelAccesses(runnable.getActivityGraph());

			final Set<Label> labelsOut = labelAccesses.stream()
                    .filter(la -> la.getAccess() == LabelAccessEnum.WRITE)
                    .map(LabelAccess::getData).filter(Objects::nonNull)
                    .filter(label->isValidOutput(label, id, TaskDepsList))
                    .collect(Collectors.toSet());

			final Set<Label> labelsIn = labelAccesses.stream()
                    .filter(la -> la.getAccess() == LabelAccessEnum.READ)
                    .map(LabelAccess::getData).filter(Objects::nonNull)
                    .filter(label->isValidInput(label, id, TaskDepsList))
                    .collect(Collectors.toSet());

			final Set<Label> labelsMapOut = labelAccesses.stream()
                    .filter(la -> la.getAccess() == LabelAccessEnum.WRITE)
                    .map(LabelAccess::getData).filter(Objects::nonNull)
                    .collect(Collectors.toSet());

			final Set<Label> labelsMapIn = labelAccesses.stream()
                    .filter(la -> la.getAccess() == LabelAccessEnum.READ)
                    .map(LabelAccess::getData).filter(Objects::nonNull)
                    .collect(Collectors.toSet());

			final Set<Label> labelsMapInOut = new HashSet<>(labelsMapIn);
            labelsMapInOut.retainAll(labelsMapOut);
            labelsMapIn.removeAll(labelsMapInOut);
            labelsMapOut.removeAll(labelsMapInOut);

			final Set<Label> labelsInOut = new HashSet<>(labelsIn);
            labelsInOut.retainAll(labelsOut);
            labelsIn.removeAll(labelsInOut);
            labelsOut.removeAll(labelsInOut);

            externLabels.addAll(labelsMapInOut);
            externLabels.addAll(labelsMapIn);
            externLabels.addAll(labelsMapOut);

			String isOpenMP = "\n#if defined(_OPENMP)";
			String isOMPSS = "\n#elif defined(_OMPSS)";

			String OpenMPPragma = "\n #pragma omp task";
			String OmpssPragma = "\n #pragma oss task";

			boolean isTarget = false;
			if (localMode != null && (localMode.equals("GPU")))
				isTarget = true;

			String depends = "";
			String maps = "";
			if (labelsIn.size() != 0) {
				depends += " depend(in:";
				for (Label inLabel : labelsIn) {
					depends += inLabel.getName() + ",";
				}
				depends = depends.substring(0, depends.length() - 1);
				depends += ")";
			}
			if (isTarget && labelsMapIn.size()!=0) {
					maps += " map(to:";
					for (Label inMapLabel : labelsMapIn)
						maps += inMapLabel.getName() + "[0:" + AmaltheaModelUtils.getLabelArraySize(inMapLabel) + "],";
					maps = maps.substring(0, maps.length() - 1);
					maps += ")";
			}


			if (labelsOut.size() != 0) {
				depends += " depend(out:";
				for (Label outLabel : labelsOut) {
					depends += outLabel.getName() + ",";
				}
				depends = depends.substring(0, depends.length() - 1);
				depends += ")";
			}
			if (isTarget && labelsMapOut.size()!=0) {
				maps += " map(from:";
				for (Label outMapLabel : labelsMapOut)
					maps += outMapLabel.getName() + "[0:" + AmaltheaModelUtils.getLabelArraySize(outMapLabel) + "],";
				maps = maps.substring(0, maps.length() - 1);
				maps += ")";
			}

			if (labelsInOut.size() != 0) {
				depends += " depend(inout:";
				for (Label inoutLabel : labelsInOut) {
					depends += inoutLabel.getName() + ",";
					if (isTarget)
						maps += inoutLabel.getName() + "[0:" + AmaltheaModelUtils.getLabelArraySize(inoutLabel) + "],";
				}
				depends = depends.substring(0, depends.length() - 1);
				depends += ")";
			}
			if (isTarget && labelsMapInOut.size()!=0) {
				maps += " map(tofrom:";
				for (Label inoutMapLabel : labelsMapInOut)
					maps += inoutMapLabel.getName() + "[0:" + AmaltheaModelUtils.getLabelArraySize(inoutMapLabel) + "],";
				maps = maps.substring(0, maps.length() - 1);
				maps += ")";
			}

			if (numReplications > 0)
				depends += " replicated(" + numReplications + ", dummy, check_Node_" +tu.getModuleName()+")";

			OpenMPPragma += depends;
			if(isTarget)
				OpenMPPragma += "\n #pragma omp target "+ maps;
			OmpssPragma += depends;

			if (isTarget)
				OmpssPragma += maps + " copy_deps";

			statements += (isOpenMP + OpenMPPragma + isOMPSS + OmpssPragma + "\n#endif\n");

		}
		return statements;
	}

	protected void genFiles(SLGTranslationUnit tu, Task task, Tag tag) {

		final List<String> callsOverwrite = new ArrayList<>();
		final Set<String> includes = new HashSet<>();
		final List<String> initCalls = new ArrayList<>();
		final List<String> stepCalls = new ArrayList<>();
		final List<Stimulus> stimuli = new ArrayList<>();
		final List<String> publishers = new ArrayList<>();
		final List<String> clientDeclarations = new ArrayList<>();
		final List<String> clientInits = new ArrayList<>();
		boolean extOverwrite = false;
		extOverwrite = runnableTransformer.processCustomProperties(extOverwrite, stepCalls, callsOverwrite,
				task.getActivityGraph());

		final Set<Label> externLabels = new HashSet<Label>();
		final List<TaskDeps> TaskDepsList = new ArrayList<TaskDeps>();
		boolean printOMPHeader = false;
		String Parallel = String.join("\n", "\n #if defined(_OPENMP)", " #pragma omp taskgraph tdg_type(static) nowait", "#endif", "{");
		String statements = "";
		boolean hasOpenMP = false;
		if (task.getCustomProperties().containsKey("OpenMP")) {
			Value V = task.getCustomProperties().get("OpenMP");
			EStructuralFeature feature = V.eClass().getEStructuralFeatures().get(0);
			hasOpenMP = (boolean) V.eGet(feature);
		}

		if (task != null && task.getActivityGraph() != null) {
			for (ActivityGraphItem item : task.getActivityGraph().getItems()) {

				if (item instanceof Group) {
					for (ActivityGraphItem item2 : ((Group) item).getItems()) {
						if ((item2 instanceof RunnableCall)) {
							final Runnable runnable = ((RunnableCall) item2).getRunnable();
							if ((runnable != null)) {
								addRunnable(runnable, (RunnableCall) item2, TaskDepsList);
							}
						}
					}
				} else if (item instanceof RunnableCall) {
					final Runnable runnable = ((RunnableCall) item).getRunnable();
					if ((runnable != null)) {
						addRunnable(runnable, (RunnableCall) item, TaskDepsList);
					}
				}
			}
		}

		int TaskId = 0;

		if (task != null && task.getActivityGraph() != null) {
			for (ActivityGraphItem item : task.getActivityGraph().getItems()) {

				if ((item instanceof RunnableCall)) {
					final RunnableCall runnableCall = (RunnableCall) item;
					final Runnable runnable = runnableCall.getRunnable();
					statements = processRunnable(tu, runnable, (RunnableCall) item, externLabels, TaskId, TaskDepsList,
							hasOpenMP);
					if (!statements.equals(""))
						printOMPHeader = true;
					SLGTranslationUnit runnableTU = rosRunnableTransformer.transform(runnable, tag);
					RunnableStore runnableStore = rosRunnableCache.getStore(runnableTU);
					includes.add(getIncFile(runnableTU));

					if (hasTagNamed(runnableCall, "initialize")) {
						initCalls.add(runnableStore.getNodeCall(runnableCall));
					} else {
						if (extOverwrite) {
							stepCalls.addAll(callsOverwrite);
						} else {
								stepCalls.add(statements + "run_" + runnable.getName() + "(" + runnableStore.getNodeParam() + ")");
						}
					}

					// TODO: Make set
					publishers.addAll(runnableStore.getPublishers());
					clientDeclarations.addAll(runnableStore.getClientDeclarations());
					clientInits.addAll(runnableStore.getClientInits());
					// TODO: add terminate function, if requested
					TaskId++;

				} else if (item instanceof Group) {
					final Group group = ((Group) item);
					for (ActivityGraphItem groupitem : group.getItems()) {
						if ((groupitem instanceof RunnableCall)) {
							final RunnableCall runnableCall = (RunnableCall) groupitem;
							final Runnable runnable = runnableCall.getRunnable();
							statements = processRunnable(tu, runnable, (RunnableCall) item, externLabels, TaskId,
									TaskDepsList, hasOpenMP);
							if (!statements.equals(""))
								printOMPHeader = true;
							SLGTranslationUnit runnableTU = rosRunnableTransformer.transform(runnable, tag);
							RunnableStore runnableStore = rosRunnableCache.getStore(runnableTU);

							includes.add(getIncFile(runnableTU));

							if (hasTagNamed(runnableCall, "initialize")) {
								initCalls.add(runnableStore.getNodeCall(runnableCall));
							} else {
								stepCalls.add(statements + "run_" + runnable.getName() + "(" + runnableStore.getNodeParam() + ")");
							}
							// TODO: add terminate function, if requested
							TaskId++;
						}
					}
				}
			}
		}
		// labels must be initialized before usage, generated labels provide this method

		for (SLGTranslationUnit labelTU : rosLabelTransformer.getCache().values()) {
			includes.add(getIncFile(labelTU));
			initCalls.add(RosLabelGenerator.initCall(labelTU.getCall()));
		}

//		rosLabelTransformer.getCache().forEach(
//				(BiConsumer<ArrayList<?>, LabelTranslationUnit>) (ArrayList<?> label, LabelTranslationUnit tu) -> {
//
//				});

		// add header for srv file in case of an interprocessstimulus
		// create .srv file for the messages to be translated

		if (task != null) {
			for (Stimulus stimulus : task.getStimuli()) {
				if (stimulus instanceof InterProcessStimulus) {
					String name = stimulus.getName();
					includes.add(name + "_service/srv/" + name + "_service.hpp");

					rosInterProcessStimulusTransformer.transform(((InterProcessStimulus) stimulus));
				}
			}

			stimuli.addAll(task.getStimuli());
		}

		if (printOMPHeader) {
			this.properties.setProperty("OpenMP", "true");
			String initialStep = stepCalls.remove(0);
			stepCalls.add(0, Parallel + initialStep);
			String lastStep = stepCalls.remove(stepCalls.size() - 1);
			stepCalls.add(lastStep + ";\n}\n#pragma omp taskwait");
			stepCalls.add(0, "int dummy");
		}
		for (Label externLa : externLabels) {
			long num = AmaltheaModelUtils.getLabelArraySize(externLa);
			if (num == 0)
				num = 1;
			stepCalls.add(0, "extern int " + externLa.getName() + "[" + num + "]");
		}
		// store characteristic values in task cache
		rosTaskCache.storeValues(tu, task, stimuli, includes, initCalls, stepCalls, publishers, clientDeclarations,
				clientInits);

	}

	private boolean hasTagNamed(ITaggable element, String name) {
		for (Tag tag : element.getTags()) {
			if (tag.getName().equals(name))
				return true;
		}
		return false;
	}

}
