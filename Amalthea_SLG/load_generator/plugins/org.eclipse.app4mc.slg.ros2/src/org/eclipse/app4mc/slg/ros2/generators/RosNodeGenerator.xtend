/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.ros2.generators

import java.util.LinkedHashSet
import java.util.List
import java.util.Set
import org.eclipse.app4mc.amalthea.model.Tag

class RosNodeGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def String toHpp(Tag tag, String moduleName, Set<String> headers, List<String> declarations,
		List<String> inits, List<String> calls, List<String> serviceCallbacks, Set<String> codehookHeaders)
	'''
		«FOR codehookHeader : codehookHeaders»
			«codehookHeader»
		«ENDFOR»
		#include <chrono>
		#include <memory>
		
		#include "rclcpp/rclcpp.hpp"
		#include "std_msgs/msg/string.hpp"
		
		«FOR header : headers.sort»
			«header»
		«ENDFOR»
		
		using namespace std::chrono_literals;
		using std::placeholders::_1;
		#define CONSOLE_ENABLED
		
		#if defined(_OPENMP)
		int check_«moduleName»(int *original, int *replicated){ return 1;}
		#endif
		class «moduleName» : public rclcpp::Node
		{
			private:
				«FOR declaration : new LinkedHashSet(declarations)»
					«declaration»
				«ENDFOR»
		
			public:
				«moduleName»()
				: Node("«moduleName.toLowerCase»")
				{
					«FOR init : inits»
						«init»
					«ENDFOR»
				}
			«FOR call : calls»
				«call»
			«ENDFOR»
			«FOR serviceCallback : serviceCallbacks»
				«serviceCallback»
			«ENDFOR»
		};
	'''

}
