/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.microros.generators

import java.util.List
import java.util.Properties
import org.eclipse.app4mc.amalthea.model.Label
import org.eclipse.app4mc.slg.commons.m2t.AmaltheaModelUtils

import static org.eclipse.app4mc.slg.ros2.artefacts.ROS2SLGTransformationDefinition.PARAM_GENERATE_DEBUG_OUTPUT

class MicroRosLabelGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	// ---------- names of generated 'C' functions ----------

	static def String initCall(String labelName) '''initialize_«labelName»()'''

	// static def String initCall(String labelName, String param) 	'''initialize_«labelName»(«labelName»«param»)'''	
	static def String readCall(String labelName, String param) 	'''read_«labelName»(«param»)'''	
	static def String writeCall(String labelName, String param)	'''write_«labelName»(«param»)'''	

	// ---------- generate file contents ----------

	static def String toCMake(String libName, List<String> srcFiles)
	'''
		# LABELS_LIB ################################################################
		####
		add_library(LABELS_LIB  STATIC                «««»» Temporary

		${CMAKE_CURRENT_LIST_DIR}/_src/labels.cpp
		«FOR srcFile : srcFiles»
			«""»	${CMAKE_CURRENT_LIST_DIR}/_src/«srcFile»
		«ENDFOR»
		)
		
		target_include_directories(«libName»
				PUBLIC ${CMAKE_CURRENT_LIST_DIR}/_inc
		)	
	'''

	static def String toH(Label label) {
		val name = if (label?.name.isNullOrEmpty) "<undefined label>" else label.name

		'''
		extern "C" {
		void «initCall(name)»;
		void «readCall(name, "int labelAccessStatistics")»;
		void «writeCall(name, "int labelAccessStatistics")»;
		}
		
		'''
	}

	static def String toCpp(Label label, Properties properties) {
		val name = if (label?.name.isNullOrEmpty) "<undefined label>" else label.name
		val numberOfBytes = if (label?.size === null) 0 else label.size.numberBytes

		val genDebugOutput = Boolean.parseBoolean(properties.getProperty(PARAM_GENERATE_DEBUG_OUTPUT))

		'''
		int «name»[«AmaltheaModelUtils.getLabelArraySize(label)»];

		static bool isIinitialized_«name» = false;
		void «initCall(name)» {
			if (!isIinitialized_«name»){
				int i;
				for (i=0; i < «AmaltheaModelUtils.getLabelArraySize(label)»; i++){
					«name»[i] = i+1;
				}
				isIinitialized_«name» = true;
			}
		}

		void «readCall(name, "int labelAccessStatistics")»
		{
			«IF genDebugOutput»
				//debug_printf("  read «name»\n");
			«ENDIF»
			int numberOfBytes = «numberOfBytes»;
			int repeat;
			for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
				if(numberOfBytes < 4){
					numberOfBytes = 4;
				}
				int arraysize = sizeof(«name») / 4;
		
				int leftOverElements=arraysize%10;
				int arraySizeWith10Multiples=arraysize-leftOverElements;
				int i = 0;
				int a = 0;
				for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
					a = «name»[i];
					a = «name»[i+1];
					a = «name»[i+2];
					a = «name»[i+3];
					a = «name»[i+4];
					a = «name»[i+5];
					a = «name»[i+6];
					a = «name»[i+7];
					a = «name»[i+8];
					a = «name»[i+9];
				}
				for(;i<arraysize;i++){
					a = «name»[i];
				}

				(void)a;
			}
		}

		void «writeCall(name, "int labelAccessStatistics")»
		{
			«IF genDebugOutput»
				//debug_printf("  write «name»\n");
			«ENDIF»
			int numberOfBytes = «numberOfBytes»;
			int repeat;
			for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
				if(numberOfBytes < 4){
					numberOfBytes = 4;
				}
				int arraysize = sizeof(«name») / 4;
				int leftOverElements=arraysize%10;
				int arraySizeWith10Multiples=arraysize-leftOverElements;
				
				int i = 0;
				for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
					«name»[i]   = 0x800A;
					«name»[i+1] = 0xAFFE;
					«name»[i+2] = 0xAFFE;
					«name»[i+3] = 0xAFFE;
					«name»[i+4] = 0xAFFE;
					«name»[i+5] = 0xAFFE;
					«name»[i+6] = 0xAFFE;
					«name»[i+7] = 0xAFFE;
					«name»[i+8] = 0xAFFE;
					«name»[i+9] = 0xAFFE;
				}
				for(;i<arraysize;i++){
						«name»[i]=0xAFFE;
				}
			}
		}
		'''
	}

}
