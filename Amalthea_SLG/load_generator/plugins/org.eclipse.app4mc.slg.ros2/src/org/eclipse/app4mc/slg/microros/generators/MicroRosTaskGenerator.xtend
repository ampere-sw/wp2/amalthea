/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *	 Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.microros.generators

import java.util.Collection
import java.util.HashSet
import java.util.Properties
import org.eclipse.app4mc.amalthea.model.ChannelReceive
import org.eclipse.app4mc.amalthea.model.ChannelSend
import org.eclipse.app4mc.amalthea.model.EventStimulus
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus
import org.eclipse.app4mc.amalthea.model.RelativePeriodicStimulus
import org.eclipse.app4mc.amalthea.model.Stimulus
import org.eclipse.app4mc.amalthea.model.Tag
import org.eclipse.app4mc.amalthea.model.Task
import org.eclipse.app4mc.amalthea.model.Time
import org.eclipse.app4mc.amalthea.model.TimeUnit
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils
import org.eclipse.app4mc.slg.ros2.transformers.utils.Utils

import static org.eclipse.app4mc.slg.ros2.artefacts.ROS2SLGTransformationDefinition.PARAM_GENERATE_DEBUG_OUTPUT
import static org.eclipse.app4mc.slg.ros2.artefacts.ROS2SLGTransformationDefinition.PARAM_GENERATE_STATUS_OUTPUT

class MicroRosTaskGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def String getGlobalDeclaration(Tag tag) {

		val nodeName = RosModelUtils.getROSNodeName(tag)

		val publishers = new HashSet()
		val messages = new HashSet()

		for (Task task : RosModelUtils.getTaggedTasks(tag)) {

			for (ChannelSend cs : RosModelUtils.getNestedChannelSends(task.activityGraph)) {
				val data = cs.data.name.toLowerCase()
				publishers.add(data)
				messages.add(data)
			}

			for (ChannelReceive cr : RosModelUtils.getNestedChannelReceives(task.activityGraph)) {
				val data = cr.data.name.toLowerCase()
				messages.add(data)
			}
		}

		'''

		«FOR publisher : publishers.sort»
			rcl_publisher_t «nodeName.toLowerCase»Node_«publisher»_publisher;
		«ENDFOR»
		«FOR message : messages.sort»
			std_msgs__msg__String «nodeName.toLowerCase»Node_«message»_msg;
		«ENDFOR»
		'''
	}

// time unit is not read !!! counter is not reliable !!!
	static def String getPeriodicTaskCalls(Tag tag, Collection<Stimulus> stimuli, Collection<String> stepCalls)
	'''
	«FOR stimulus : stimuli»
		«IF stimulus instanceof PeriodicStimulus || stimulus instanceof RelativePeriodicStimulus»
			int debug_«stimulus.name»_counter = 0;
			DeclareTask(«stimulus.name»Task);
			TASK(«stimulus.name»Task)
			{
				if(«getPeriod(stimulus).value» < «getCountPerSecond(getPeriod(stimulus).unit)») {
					debug_«stimulus.name»_counter += «getPeriod(stimulus).value»;
				 	if(debug_«stimulus.name»_counter >= «getCountPerSecond(getPeriod(stimulus).unit)») {
				 		debug_printf("Task «stimulus.name» (%d instances in 1 second)\n", «getCountPerSecond(getPeriod(stimulus).unit)»/«getPeriod(stimulus).value»);
				 		debug_«stimulus.name»_counter = 0;
					}
				} else {
					debug_printf("Task «stimulus.name»\n");
				}
				«FOR call : stepCalls»
				«""»	«call»;
				«ENDFOR»
			}
		«ENDIF»
	«ENDFOR»
	
	'''

	private static def Time getPeriod(Stimulus stimulus) {
		if (stimulus instanceof PeriodicStimulus) {
			return stimulus.recurrence
		}
		if (stimulus instanceof RelativePeriodicStimulus) {
			return stimulus.nextOccurrence.lowerBound
		}
		return FactoryUtil.createTime()
	}

	private static def Long getCountPerSecond(TimeUnit unit) {
		switch (unit) {
			case S:  1L
			case MS: 1_000L
			case US: 1_000_000L
			case NS: 1_000_000_000L
			case PS: 1_000_000_000_000L
			default: 0L // should never happen
		}
	}

	static def String getDeclaration(Collection<Stimulus> stimuli, Collection<String> publishers, Collection<String> clientDeclarations)
	'''
		«FOR stimulus : stimuli»
			«IF stimulus instanceof PeriodicStimulus»
				rclcpp::TimerBase::SharedPtr «(stimulus as PeriodicStimulus).name»_timer_;
			«ENDIF»
			«IF stimulus instanceof EventStimulus»
				rclcpp::Subscription<std_msgs::msg::String>::SharedPtr «(stimulus as EventStimulus).name»_subscription_;
			«ENDIF»
			«IF stimulus instanceof InterProcessStimulus»
				rclcpp::Service<«(stimulus as InterProcessStimulus).name»_service::srv::«Utils.toIdlCompliantName((stimulus as InterProcessStimulus).name + '_service')»>::SharedPtr «(stimulus as InterProcessStimulus).name»_service;
			«ENDIF»
		«ENDFOR»
		«FOR publisher : publishers»
			rclcpp::Publisher<std_msgs::msg::String>::SharedPtr «publisher»;
			«ENDFOR»
			«FOR decl: clientDeclarations»
				«decl»;
			«ENDFOR»
	'''

	static def String getInitialisation(String nodeName, Collection<Stimulus> stimuli, Collection<String> publishers, Collection<String> clientInits)
	'''
		«FOR stimulus : stimuli»
			«IF stimulus instanceof PeriodicStimulus»
				«(stimulus as PeriodicStimulus).name»_timer_ = this->create_wall_timer(
						«(stimulus as PeriodicStimulus).recurrence.value»«(stimulus as PeriodicStimulus).recurrence.unit», std::bind(&«nodeName»::«(stimulus as PeriodicStimulus).name»_timer_callback, this));
			«ENDIF»
			«IF stimulus instanceof EventStimulus»
				«(stimulus as EventStimulus).name»_subscription_ = this->create_subscription<std_msgs::msg::String>(
					"«(stimulus as EventStimulus).name»", 10, std::bind(&«nodeName»::«(stimulus as EventStimulus).name»_subscription_callback, this, _1));
			«ENDIF»
			«IF stimulus instanceof InterProcessStimulus»
				«(stimulus as InterProcessStimulus).name»_service = this->create_service<«(stimulus as InterProcessStimulus).name»_service::srv::«Utils.toIdlCompliantName((stimulus as InterProcessStimulus).name + '_service')»>("«(stimulus as InterProcessStimulus).name»_service", &«(stimulus as InterProcessStimulus).name»_service_callback);
			«ENDIF»
		«ENDFOR»
		«FOR publisher : publishers»
			«publisher» = this->create_publisher<std_msgs::msg::String>("«publisher.replace("_publisher", "")»", 10);
		«ENDFOR»
		«FOR init : clientInits»
			«init»;
		«ENDFOR»
	'''

	static def String getServiceCallback(Collection<Stimulus> stimuli, Collection<String> stepCalls)
	'''
	«FOR stimulus : stimuli»
		«IF stimulus instanceof InterProcessStimulus»
			void «(stimulus as InterProcessStimulus).name»_service_callback(const std::shared_ptr<«(stimulus as InterProcessStimulus).name»_service::srv::«Utils.toIdlCompliantName((stimulus as InterProcessStimulus).name + '_service')»::Request> request,
			std::shared_ptr<«(stimulus as InterProcessStimulus).name»_service::srv::«Utils.toIdlCompliantName((stimulus as InterProcessStimulus).name + '_service')»::Response> response) {
				«FOR call : stepCalls»
					«call»;
				«ENDFOR»
			}
		«ENDIF»
	«ENDFOR»
	'''
	
	static def String getCallback(Tag tag, Properties properties, Collection<Stimulus> stimuli, Collection<String> stepCalls)  {
		val nodeName = RosModelUtils.getROSNodeName(tag)
		val publishers = new HashSet()

		for (Task task : RosModelUtils.getTaggedTasks(tag)) {
			for (ChannelSend cs : RosModelUtils.getNestedChannelSends(task.activityGraph)) {
				val data = cs.data.name.toLowerCase()
				publishers.add(data)
			}
		}

		val genStatusOutput = Boolean.parseBoolean(properties.getProperty(PARAM_GENERATE_STATUS_OUTPUT))
		val genDebugOutput = Boolean.parseBoolean(properties.getProperty(PARAM_GENERATE_DEBUG_OUTPUT))

		'''

		«FOR stimulus : stimuli»
«««			«IF stimulus instanceof PeriodicStimulus»
«««				void «nodeName.toLowerCase»_timer_callback(rcl_timer_t * timer, int64_t last_call_time)
«««				{
«««					rcl_ret_t rc;
«««					RCLC_UNUSED(last_call_time);
«««					if (timer != NULL) {
«««						«FOR publisher : publishers.sort»
«««							rc = rcl_publish(&«nodeName.toLowerCase»Node_«publisher»_publisher, &«nodeName.toLowerCase»Node_«publisher»_msg, NULL);
«««						«ENDFOR»
«««					
«««						«FOR call : stepCalls»
«««							«call»;
«««							«System.out.println(call)»
«««						«ENDFOR»
«««					} else {
«««						printf("timer_callback Error: timer parameter is NULL\n");
«««					}
«««				}
«««			«ENDIF»
			«IF stimulus instanceof EventStimulus»
				void «(stimulus as EventStimulus).name.toLowerCase»_subscription_callback(const void * msgin)
				{
					const std_msgs__msg__String * msg = (const std_msgs__msg__String *)msgin;	
					if (msg == NULL) {
						// no calls
						«IF genStatusOutput»
							printf("Callback: msg NULL\n");
						«ENDIF»
					} else {
						«IF genStatusOutput»
							printf("Callback: Received message on '«(stimulus as EventStimulus).name.toLowerCase»' topic\n");
							printf("Callback: I heard '%s'\n", msg->data.data);
						«ENDIF»
						«FOR call : stepCalls»
							«call»;
						«ENDFOR»
					}
				
				}
			«ENDIF»
		«ENDFOR»
		'''
	}

	static def Collection<String> getHeaders(Collection<String> includes) {
		includes.map[i |"#include \"" + i + "\"\n"].toList
	}

}
