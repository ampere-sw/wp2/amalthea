/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers.common;

import static org.eclipse.app4mc.slg.ros2.transformers.RosBaseSettings.C_SRC_FOLDER;
import static org.eclipse.app4mc.slg.ros2.transformers.RosBaseSettings.C_SRC_TYPE;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaServices;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.slg.commons.m2t.CustomObjectsStore;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.config.CodehookType;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.util.ConfigModelUtils;
import org.eclipse.app4mc.slg.microros.generators.MicroRosNodeGenerator;
import org.eclipse.app4mc.slg.microros.transformers.MicroRosTaskCache;
import org.eclipse.app4mc.slg.microros.transformers.MicroRosTaskCache.MicroRosTaskStore;
import org.eclipse.app4mc.slg.microros.transformers.MicroRosTaskTransformer;
import org.eclipse.app4mc.slg.ros2.generators.RosNodeGenerator;
import org.eclipse.app4mc.slg.ros2.transformers.RosBaseTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.sw.RosTaskCache;
import org.eclipse.app4mc.slg.ros2.transformers.sw.RosTaskCache.TaskStore;
import org.eclipse.app4mc.slg.ros2.transformers.sw.RosTaskTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class RosNodeTransformer extends RosBaseTransformer {

	@Inject private RosTaskTransformer rosTaskTransformer;
	@Inject private MicroRosTaskTransformer microRosTaskTransformer;

	@Inject private RosTaskCache rosTaskCache;
	@Inject private MicroRosTaskCache microRosTaskCache;

	@Inject private Properties properties;
	@Inject private CustomObjectsStore customObjsStore;
	@Inject private SessionLogger logger;

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final Tag tag) {
		final List<Object> key = List.of(tag);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(tag);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, tag);
		}

		return tu;
	}

	// ---------------------------------------------------

	private SLGTranslationUnit createTranslationUnit(final Tag tag) {
		if (tag == null) {
			return new SLGTranslationUnit("UNSPECIFIED TAG");
		} else {
			// set base path and module name
			String basePath = RosModelUtils.getROSTargetName(tag);
			String moduleName = RosModelUtils.getROSNodeName(tag);
			String call = "";
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	private void doTransform(final SLGTranslationUnit tu, final Tag tag) {
		genFiles(tu, tag);
	}

	private void genFiles(final SLGTranslationUnit tu, final Tag tag) {

		boolean isMicroRos = RosModelUtils.isTypeMicroROS(tag);

		HashSet<String> codehookHeaders = new HashSet<>();

		Set<String> headers = new HashSet<>();
		List<String> declarations = new LinkedList<>();
		List<String> initializations = new LinkedList<>();
		List<String> calls = new LinkedList<>();
		List<String> serviceCallbacks = new LinkedList<>();

		Set<String> headersMicroRos = new HashSet<>();
		List<String> declarationsMicroRos = new LinkedList<>();
		List<String> initializationsMicroRos = new LinkedList<>();
		List<String> periodicCallsMicroRos = new LinkedList<>();
		List<String> callsMicroRos = new LinkedList<>();
		List<String> serviceCallbacksMicroRos = new LinkedList<>();
		Set<Stimulus> stimuliMicroRos = new HashSet<>();
		Set<String> globalDeclarationsMicroRos = new LinkedHashSet<>();

		Amalthea model = AmaltheaServices.getContainerOfType(tag, Amalthea.class);

		for (Task task : RosModelUtils.getTaggedTasks(tag)) {

			if(isMicroRos) {

				final SLGTranslationUnit taskTU = microRosTaskTransformer.transform(task, tag);
				final MicroRosTaskStore storeMicroRos = microRosTaskCache.getStore(taskTU);
				
				headersMicroRos.addAll(storeMicroRos.getHeaders());

				globalDeclarationsMicroRos.add(storeMicroRos.getGlobalDeclaration(tag));
				declarationsMicroRos.add(storeMicroRos.getDeclaration());
				initializationsMicroRos.add(storeMicroRos.getInitialisation(tu.getModuleName()));
				periodicCallsMicroRos.add(storeMicroRos.getPeriodicTaskCalls(tag));
				callsMicroRos.add(storeMicroRos.getCallback(tag));
				serviceCallbacksMicroRos.add(storeMicroRos.getServiceCallback()); 
				
				stimuliMicroRos.addAll(storeMicroRos.getStimuli());
			}
			
			else {
				//----------------------------------/* fetch headers names*/--------------------------------------
				// to fetch the headers names we use the getHeaderFilesDirectories function and we go through every directory with the for loop

				final ConfigModel configModel = customObjsStore.<ConfigModel>getInstance(ConfigModel.class);
				boolean enableExtCode = Boolean.parseBoolean(properties.getProperty("enableExternalCode"));
				if (enableExtCode) {
					// input property to test if we need to include the external code headerfiles
					String workingDirectory = customObjsStore.getData(TransformationConstants.WORKING_DIRECTORY);
					for (String hDir : ConfigModelUtils.getHeaderFilesDirectories(configModel, CodehookType.TASK, workingDirectory, logger)) {
						final File folder = new File(hDir.trim()); // fetching all the names in the headerfile directory.
						String names = ConfigModelUtils.getHeaderFilesIncludeMultiString(folder, logger);
						codehookHeaders.add(names);
					}
				}
				//------------------------------------------------------------------------
				
				final SLGTranslationUnit taskTU = rosTaskTransformer.transform(task, tag);
				final TaskStore store = rosTaskCache.getStore(taskTU);

				headers.addAll(store.getHeaders());

				declarations.addAll(List.of(store.getDeclaration().split(System.getProperty("line.separator"))));
				
				initializations.add(store.getInitialisation(tu.getModuleName()));
				calls.add(store.getCallback());
				// System.out.println("Those are calls" + calls);
				serviceCallbacks.add(store.getServiceCallback());
			}
		}

		if(isMicroRos) {
			// generate C file (requires special handling because default is C++) 
			String targetFile = tu.getModulePath() + C_SRC_FOLDER + tu.getModuleName();
			String code = MicroRosNodeGenerator.toC(tag, tu.getModuleName(), properties, headersMicroRos, declarationsMicroRos, initializationsMicroRos, periodicCallsMicroRos, callsMicroRos, serviceCallbacksMicroRos, globalDeclarationsMicroRos, model, stimuliMicroRos);
			customAppend(C_SRC_TYPE, targetFile, code);
		} else {
			incAppend(tu, RosNodeGenerator.toHpp(tag, tu.getModuleName(), headers, declarations, initializations, calls, serviceCallbacks, codehookHeaders));
		}
	}

}
