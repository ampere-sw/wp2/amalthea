/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers.sw;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.ros2.generators.RosLabelGenerator;
import org.eclipse.app4mc.slg.ros2.transformers.RosBaseTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils;
import org.eclipse.app4mc.transformation.util.OutputBuffer;

import com.google.inject.Inject;


public class RosLabelTransformer extends RosBaseTransformer {

	public static final String LIB_NAME = "LABELS_LIB";
    public static final String BASE_PATH = "synthetic_gen";
    public static final String MODULE_NAME = "labels";
    public static final String MODULE_PATH = BASE_PATH + "/" + MODULE_NAME;
    public static final String MAKEFILE_PATH = MODULE_PATH + "/CMakeLists.txt";

    @Inject private OutputBuffer outputBuffer;

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final Label label, final Tag tag) {
		final String target = RosModelUtils.getROSTargetName(tag);
		final List<Object> key = List.of(label, target);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(label, target);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, label);
		}

		return tu;
	}

	// ---------------------------------------------------

	protected SLGTranslationUnit createTranslationUnit(Label label, String target) {
		if ((label == null)) {
			return new SLGTranslationUnit("UNSPECIFIED LABEL");
		} else {
			String basePath = target + "/" + BASE_PATH;
			String moduleName = MODULE_NAME;
			String call = label.getName();
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	protected void doTransform(final SLGTranslationUnit tu, final Label label) {
		genFiles(tu, label);
	}

	protected void genFiles(SLGTranslationUnit tu, Label label) {
		if (isSrcFileEmpty(tu)) {
			srcAppend(tu, "#include \"" + getIncFile(tu) + "\"\n\n");
			srcAppend(tu, "#include <stdlib.h>\n");
		}

		incAppend(tu, "#pragma once \n\n");
		incAppend(tu, RosLabelGenerator.toH(label));
		srcAppend(tu, RosLabelGenerator.toCpp(label));
	}

	public boolean createCMake(String target) {
		return outputBuffer.appendTo("OTHER", target + "/" + MAKEFILE_PATH,
				RosLabelGenerator.toCMake(LIB_NAME, getSrcFiles()));
	}

}
