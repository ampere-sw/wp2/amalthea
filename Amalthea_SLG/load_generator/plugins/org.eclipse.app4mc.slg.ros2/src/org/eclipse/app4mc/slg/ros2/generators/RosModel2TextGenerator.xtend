/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.ros2.generators

import com.google.inject.Singleton
import java.nio.file.Paths
import java.util.ArrayList
import java.util.List
import java.util.Set
import org.apache.commons.io.FilenameUtils
import org.eclipse.app4mc.slg.config.CodehookType
import org.eclipse.app4mc.slg.config.ConfigModel
import org.eclipse.app4mc.slg.config.util.ConfigModelUtils
import org.eclipse.app4mc.slg.ros2.transformers.RosModel2TextTransformer.TargetInfo
import java.util.Map
import org.eclipse.app4mc.slg.ros2.transformers.RosModel2TextTransformer.SchedulingParameters

@Singleton
class RosModel2TextGenerator {

	static def String toMain(Set<String> nodes, Map<String, Boolean> hasOpenMP, Map<String, List<SchedulingParameters>> schedParams) '''
		«FOR node : nodes.sort»
			#include "«node».hpp"
		«ENDFOR»
		
		int main(int argc, char *argv[])
		{
			std::vector<std::thread> threads;
		
			setvbuf(stdout, NULL, _IONBF, BUFSIZ);
			rclcpp::init(argc, argv);
		«getExecutors(nodes.sort, hasOpenMP, schedParams)»

			//WAITING OF THREADS

			for (auto& th : threads)
				th.join();
		
			rclcpp::shutdown();
		
			return 0;
		}
	'''

	private static def String getExecutors(List<String> nodes, Map<String, Boolean> hasOpenMP, Map<String, List<SchedulingParameters>> schedParams) {
		val sb = new StringBuilder()
		for (var i = 1 ; i <= nodes.size ; i++) {
			val node = nodes.get(i-1);
			val schedString = new StringBuilder;
			val List<SchedulingParameters> threadParams = schedParams.get(node);
			for (var j = 0 ; j < threadParams.size ; j++)
				schedString.append("thread_sched("+ j +"," + threadParams.get(j).period+"," + threadParams.get(j).deadline + "," + threadParams.get(j).runtime + ") ");
			if (hasOpenMP.get(node)){
			sb.append('''

				//EXECUTOR «i»

				rclcpp::executors::SingleThreadedExecutor executor«i»;
				auto node«i» = std::make_shared<«node»>();
				executor«i».add_node(node«i»);

				auto spin_executor«i» = [&executor«i»]()
				{
					#pragma omp parallel ''')
			sb.append(schedString)
			sb.append('''
				
					#pragma omp single
					executor«i».spin();
				};
				threads.emplace_back(std::thread(spin_executor«i»));
			''')
			}
			else{
			sb.append('''

				//EXECUTOR «i»

				rclcpp::executors::SingleThreadedExecutor executor«i»;
				auto node«i» = std::make_shared<«node»>();
				executor«i».add_node(node«i»);

				auto spin_executor«i» = [&executor«i»]()
				{
					executor«i».spin();
				};
				threads.emplace_back(std::thread(spin_executor«i»));
			''')
			}
		}

		sb.toString
	}

   static def String toGPUTicks() '''
		#define RATIO 0.1
		__global__ void kernel(long long nclocks){
			long long start=clock64();
			while (clock64() < start+nclocks);
		}
		void executeGPUTicks(long long average,long long lowerBound, long long upperBound) {
		   kernel<<<1,1>>>(average * RATIO);
		   cudaDeviceSynchronize();
		}
		void executeGPUTicksConstant(long long ticks) {
		   kernel<<<1,1>>>(ticks * RATIO);
		   cudaDeviceSynchronize();
		}
	'''

	static def String toLaunchFile(Set<String> nodes) '''
		from launch import LaunchDescription
		from launch_ros.actions import Node
		
		def generate_launch_description():
		    return LaunchDescription([
				«FOR node : nodes.sort»
					Node(
						package='amalthea_ros2_model',
						node_namespace='',
						node_executable='«node»',
					),
				«ENDFOR»
			])
	'''

	static def String toBuildScript(Set<String> services) '''
		«FOR service : services.sort»
			#building service «service»
			
			cd services/«service»
			colcon build
			. install/setup.bash
			cd ../..
			
		«ENDFOR»
		colcon build
		. install/setup.bash
	'''

	// TODO: define attributes via ConfigModel
	static def String toPackageXml(Set<String> services) '''
		<?xml version="1.0"?>
		<?xml-model href="http://download.ros.org/schema/package_format3.xsd" schematypens="http://www.w3.org/2001/XMLSchema"?>
		<package format="3">
			<name>amalthea_ros2_model</name>
			<version>0.0.0</version>
			<description>TODO: Package description</description>
			<maintainer email="app4mc-dev@eclipse.org">app4mc-dev</maintainer>
			<license>TODO: License declaration</license>
		
			<buildtool_depend>ament_cmake</buildtool_depend>
		
			<depend>rclcpp</depend>
			<depend>std_msgs</depend>
		
			«FOR service : services.sort»
				<depend>«service»</depend>
			«ENDFOR»
		
			<test_depend>ament_lint_auto</test_depend>
			<test_depend>ament_lint_common</test_depend>
		
			<export>
				<build_type>ament_cmake</build_type>
			</export>
		</package>
	'''

	private static def String getHeaderFileDirectories(ConfigModel configModel, String workingDirectory){

		val sb = new StringBuilder

		val runnableHeaders = ConfigModelUtils.getHeaderFilesDirectories(configModel, CodehookType.RUNNABLE, workingDirectory, null);
		val tasksHeader = ConfigModelUtils.getHeaderFilesDirectories(configModel, CodehookType.TASK, workingDirectory, null);

		val allHeaderDirs = new ArrayList

		allHeaderDirs.addAll(runnableHeaders)
		allHeaderDirs.addAll(tasksHeader)

		for (hfileDir : allHeaderDirs) {
			sb.append("include_directories(" + "\"" + hfileDir + "\")")
			sb.append(System.getProperty("line.separator"));
		}
		return sb.toString.trim;
	}

	private static def String getLibraries(ConfigModel configModel, String workingDirectory) {
		val listLibsNames = new ArrayList<String>
		val listpackageNames = new ArrayList<String>
		val listKeywordLibs = new ArrayList<String>

		val sb = new StringBuilder()

		for (libPath : ConfigModelUtils.getLinkedLibraryInstructions(configModel, workingDirectory)) {
			val fileName = Paths.get(libPath).getFileName().toString()

			if (fileName.endsWith(".a")) {
				// We have a static library
				val name = FilenameUtils.removeExtension(fileName) // removing the extension to get the name
				listLibsNames.add(name)

				sb.append("add_library(" + name + " STATIC IMPORTED)\n")
				sb.append("set_property(TARGET " + name + " PROPERTY\n")
				sb.append("\t\tIMPORTED_LOCATION \"" + libPath + "\")")

			} else if (fileName.endsWith(".so")) {
				// we have a shared library
				val name = FilenameUtils.removeExtension(fileName) // removing the extension to get the name
				listLibsNames.add(name)

				sb.append("add_library(" + name + " SHARED IMPORTED)\n")
				sb.append("set_property(TARGET " + name + " PROPERTY\n")
				sb.append("\t\tIMPORTED_LOCATION \"" + libPath + "\")")
				
			} else {
				listKeywordLibs.add(libPath)
			}
		}

		for (packageName : ConfigModelUtils.getPackageNames(configModel, workingDirectory)) {
			sb.append("find_package(" + packageName + ")\n")
			
			listpackageNames.add(packageName)
		}

		sb.toString
	}

	static def String toCmake(TargetInfo targetInfo, boolean externalCode, ConfigModel configModel, String workingDirectory, boolean hasGlobalOpenMP) '''
		# CMakeLists for Nodes
		cmake_minimum_required(VERSION 3.5)
		project(amalthea_ros2_model)
		
		# Default to C++14
		if(NOT CMAKE_CXX_STANDARD)
			set(CMAKE_CXX_STANDARD 14)
		endif()
		
		if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
			add_compile_options(-Wall -Wextra -Wpedantic)
		endif()
		
		«IF externalCode»
			# Adding the external code's include directories
			«getHeaderFileDirectories(configModel, workingDirectory)»
			
			# Adding the libraries created out of the external code
			«getLibraries(configModel, workingDirectory)»
		«ENDIF»			
		
		find_package(ament_cmake REQUIRED)
		find_package(rclcpp REQUIRED)
		find_package(std_msgs REQUIRED)
		find_package(CUDA REQUIRED)
		«IF hasGlobalOpenMP»
			find_package(OpenMP)

			if(OPENMP_FOUND)
			    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS} -fopenmp-taskgraph -static-tdg")
			    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS} -fopenmp-taskgraph -static-tdg")
			    set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
			endif()
		«ENDIF»
		«FOR service : targetInfo.services.sort»
			find_package(«service» REQUIRED)
		«ENDFOR»
		«IF targetInfo.hasChannelSend»

			add_library(CHANNELSEND_UTILS  STATIC
				synthetic_gen/channelSendUtils/_src/channelSendUtils.cpp
			)
			ament_target_dependencies(CHANNELSEND_UTILS rclcpp std_msgs «targetInfo.services.sort.join(" ")»)
			target_include_directories(CHANNELSEND_UTILS
			    PUBLIC synthetic_gen/channelSendUtils/_inc/
			)
		«ENDIF»
		«IF targetInfo.hasInterprocess»
			
			add_library(INTERPROCESSTRIGGER_UTIL STATIC
				  synthetic_gen/interProcessTriggerUtils/_src/interProcessTriggerUtils.cpp
			)
			ament_target_dependencies(INTERPROCESSTRIGGER_UTIL rclcpp std_msgs «targetInfo.services.sort.join(" ")»)
			target_include_directories(INTERPROCESSTRIGGER_UTIL
				PUBLIC synthetic_gen/interProcessTriggerUtils/_inc
			)
		«ENDIF»
		«IF targetInfo.hasPerformanceMeasurement»
			
			add_library(AML_LIB  STATIC
				utils/aml/_src/aml.cpp
			)
			target_include_directories(AML_LIB
				PUBLIC utils/aml/_inc/
			)
		«ENDIF»
		
		«IF targetInfo.hasLabel»
			add_subdirectory (synthetic_gen/labels)
		«ENDIF»
		«IF targetInfo.hasTicks»
			add_subdirectory (synthetic_gen/ticksUtils)
		«ENDIF»
		
		# RUNNABLES_LIB ################################################################
		
		####
		add_library(RUNNABLES_LIB STATIC
			synthetic_gen/runnables/_src/runnables.cpp
		)
		
		target_include_directories(RUNNABLES_LIB
			PUBLIC synthetic_gen/runnables/_inc
		)	
		
		target_include_directories(RUNNABLES_LIB
			PUBLIC 
		«getOtherDirectories(targetInfo)»
		)
		target_link_libraries(RUNNABLES_LIB
			PRIVATE «getOtherLibraries(targetInfo)»
		)
		«FOR node : targetInfo.nodes.sort»
			
			
			# ********** Node «node» **********
			
			add_library(«node»_LIB STATIC
				«node»/_inc/«node».hpp
			)
			
			target_include_directories(«node»_LIB
				PUBLIC
				synthetic_gen/runnables/_inc
			«getOtherDirectories(targetInfo)»
			)
			
			target_link_libraries(«node»_LIB 
				PRIVATE  RUNNABLES_LIB «getOtherLibraries(targetInfo)»
			)
		«ENDFOR»
		
		«IF hasGlobalOpenMP»
			add_library(MAIN_OBJ OBJECT main.cpp)

			add_custom_command(
			    COMMAND echo ""
			    OUTPUT main_tdg.cpp
			    DEPENDS main.cpp
			)
			target_include_directories(MAIN_OBJ
				PUBLIC
				«FOR node : targetInfo.nodes.sort»
					«node»/_inc
				«ENDFOR»
			)

			target_link_libraries(MAIN_OBJ «targetInfo.nodes.sort.map[n | n + "_LIB"].join(" ")»)
			ament_target_dependencies(MAIN_OBJ rclcpp std_msgs «targetInfo.services.sort.join(" ")»)
		«ENDIF»
		
		SET_SOURCE_FILES_PROPERTIES(GPUTicks.cu PROPERTIES LANGUAGE CXX)
		add_executable(MAIN_BIN main.cpp GPUTicks.cu)
		
		target_include_directories(MAIN_BIN
			PUBLIC
			«FOR node : targetInfo.nodes.sort»
				«node»/_inc
			«ENDFOR»
		)
		
		target_link_libraries(MAIN_BIN ${CUDA_LIBRARIES} «targetInfo.nodes.sort.map[n | n + "_LIB"].join(" ")»)
		ament_target_dependencies(MAIN_BIN rclcpp std_msgs «targetInfo.services.sort.join(" ")»)
		
		«IF hasGlobalOpenMP»
			target_sources(MAIN_BIN PRIVATE main_tdg.cpp)
			add_dependencies(MAIN_BIN MAIN_OBJ)
		«ENDIF»
		
		install(TARGETS
			«FOR node : targetInfo.nodes.sort»
				# «node»
			«ENDFOR»
			MAIN_BIN
			DESTINATION lib/${PROJECT_NAME}
		)
		
		ament_package()

	'''

	private static def String getOtherDirectories(TargetInfo targetInfo) '''
		«IF targetInfo.hasTicks»
			«""»	synthetic_gen/ticksUtils/_inc
		«ENDIF»
		«IF targetInfo.hasLabel»
			«""»	synthetic_gen/labels/_inc
		«ENDIF»
		«IF targetInfo.hasChannelSend»
			«""»	synthetic_gen/channelSendUtils/_inc
		«ENDIF»
		«IF targetInfo.hasInterprocess»
			«""»	synthetic_gen/interProcessTriggerUtils/_inc
		«ENDIF»
		«IF targetInfo.hasPerformanceMeasurement»
			«""»	utils/aml/_inc
		«ENDIF»
	'''

	private static def String getOtherLibraries(TargetInfo targetInfo) {
		val libs = new ArrayList()

		if (targetInfo.hasPerformanceMeasurement) {
			libs.add("AML_LIB")
		}
		if (targetInfo.hasLabel) {
			libs.add("LABELS_LIB")
		}
		if (targetInfo.hasTicks) {
			libs.add("TICKS_UTILS")
		}
		if (targetInfo.hasChannelSend) {
			libs.add("CHANNELSEND_UTILS")
		}
		if (targetInfo.hasInterprocess) {
			libs.add("INTERPROCESSTRIGGER_UTIL")
		}

		libs.join(" ")
	}

}
