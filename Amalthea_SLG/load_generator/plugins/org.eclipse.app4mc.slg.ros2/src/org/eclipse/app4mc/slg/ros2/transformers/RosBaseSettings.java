/**
********************************************************************************
* Copyright (c) 2021-2023 Robert Bosch GmbH.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0
*
* SPDX-License-Identifier: EPL-2.0
*
* Contributors:
*     Robert Bosch GmbH - initial API and implementation
********************************************************************************
*/

package org.eclipse.app4mc.slg.ros2.transformers;

import org.eclipse.app4mc.transformation.util.OutputBuffer;

public class RosBaseSettings {

	private static final String THIS_CODE_IS_AUTO_GENERATED = "// This code is auto-generated\n\n";

	// Suppress default constructor
	private RosBaseSettings() {
		throw new IllegalStateException("Utility class");
	}

	public static final String C_INC_TYPE = "C_INC";
	public static final String C_INC_EXT = ".h";
	public static final String C_INC_FOLDER = "/_inc/";
	
	public static final String C_SRC_TYPE = "C_SRC";
	public static final String C_SRC_EXT = ".c";
	public static final String C_SRC_FOLDER = "/_src/";
	
	public static final String CPP_INC_TYPE = "CPP_INC";
	public static final String CPP_INC_EXT = ".hpp";
	public static final String CPP_INC_FOLDER = "/_inc/";
	
	public static final String CPP_SRC_TYPE = "CPP_SRC";
	public static final String CPP_SRC_EXT = ".cpp";
	public static final String CPP_SRC_FOLDER = "/_src/";
	
	public static final String OTHER_TYPE = "OTHER";

	public static void initializeOutputBuffer(OutputBuffer buffer, String outputFolder) {
		buffer.initialize(outputFolder);
		buffer.configureFiletype(C_INC_TYPE, C_INC_EXT, THIS_CODE_IS_AUTO_GENERATED, null);
		buffer.configureFiletype(C_SRC_TYPE, C_SRC_EXT, THIS_CODE_IS_AUTO_GENERATED, null);
		buffer.configureFiletype(CPP_INC_TYPE, CPP_INC_EXT, THIS_CODE_IS_AUTO_GENERATED, null);
		buffer.configureFiletype(CPP_SRC_TYPE, CPP_SRC_EXT, THIS_CODE_IS_AUTO_GENERATED, null);
		buffer.configureFiletype(OTHER_TYPE, "", "", "");
	}

}
