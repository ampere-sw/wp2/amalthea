/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.microros.transformers;

import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class MicroRosChannelSendTransformer {

	@Inject private MicroRosChannelSendUtilsTransformer channelSendUtilTransformer;

	public SLGTranslationUnit transform(final ChannelSend cs, final Tag tag) {
		final SLGTranslationUnit utilTU = this.channelSendUtilTransformer.transform(cs, tag);

		return createTranslationUnit(cs, utilTU, tag);
	}

	private SLGTranslationUnit createTranslationUnit(final ChannelSend cs, final SLGTranslationUnit utilTU, final Tag tag) {
		if (cs == null) {
			return new SLGTranslationUnit("UNSPECIFIED CHANNEL SEND");
		}

		String basePath = utilTU.getBasePath();
		String moduleName = utilTU.getModuleName();
		String call = computeCall(cs, tag);
		return new SLGTranslationUnit(basePath, moduleName, call);
	}

	private String computeCall(final ChannelSend cs, Tag tag) {
		String param = cs.getData().getName();

		String rosNodeName = RosModelUtils.getROSNodeName(tag);
		if (rosNodeName == null) {
			// unsupported case: Implicit node name derived from task name has to be implemented
			rosNodeName = "UNDEFINED_NODE_NAME";
		}

		return rosNodeName + "_publish_to_" + param + "(" + rosNodeName.toLowerCase() + "Node_" + param + "_msg" + ")";
	}

}
